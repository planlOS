/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/cpu.h"
#include "ke/smp.h"
#include "ke/debug.h"
#include "ke/apic.h"
#include "ke/timer.h"
#include "mm/memory.h"
#include "ke/gdt.h"
#include <string.h>
#include <stdlib.h>

extern uint32_t kernel_directory[1024];

char bootstrap_stack[0x4000] __attribute__ ((aligned (4096)));

volatile uint32_t start_aps = 0;

void keInitPIC2(void);

void keSMPEntry(void)
{
	keInitPIC2();
	
	while (!start_aps)
	{
		char *cpusign = (char*)0xC00B8000 + keAPICGetCPU() * 2;
		cpusign[0] = 'k';
		cpusign[1]++;
	}
	keFlushGDT();
	asm volatile("ltrw %%ax"::"a"(0x28 + keAPICGetCPU() * 8));
	
	char *vidmem = (char*)0xc00B8000;
	
	char *cpusign = vidmem + keAPICGetCPU() * 2;
	cpusign[0] = 'K';
	cpusign[1] = 'A';
	
	keAPICWrite(0x80, 0x20);
	keAPICWrite(0x320, 0x10000);
	keAPICWrite(0x340, 0x10000);
	keAPICWrite(0x350, 0x8700);
	keAPICWrite(0x360, 0x400);
	keAPICWrite(0x370, 0x10000);
	// Activate APIC
	keAPICWrite(0xF0, 0x10F);
	
	keInstallTimer();
	asm("sti");
	while (1) asm("hlt");
}

static FloatingPointerStruct *keFindFloatingPointerStruct(void);

extern uint32_t kernel_stack[];

void keInitSMP(void)
{
	// Find smp info
	FloatingPointerStruct *fps = keFindFloatingPointerStruct();
	if (!fps)
	{
		kePrint("No SMP!\n");
		keAddCPU(keAPICGetCPU());
		return;
	}
	else
	{
		kePrint("SMP configuration table: %X\n", fps->configtable);
		if (fps->configtable == 0)
		{
			kePrint("Error: Default SMP configurations not supported.\n");
			mmMapKernelMemory(0, (uint32_t)fps & ~0xFFF, 0);
			return;
		}
		
		// Load configuration table
		SMPConfigTable *config = (SMPConfigTable*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
			MM_MIN_KERNEL_PAGE, 1, 0x1000);
		mmMapKernelMemory(fps->configtable & ~0xFFF, (uintptr_t)config,
			MM_MAP_READ | MM_MAP_WRITE);
		config = (SMPConfigTable*)((char*)config + (fps->configtable & 0xFFF));
		
		kePrint("Table entries: %d\n", config->entrycount);
		
		// TODO: This still can crash when we step upon the next page!
		uint32_t max_lapic_id = 0;
		uint32_t bootstrap_id = 0;
		uintptr_t *stacks = malloc(sizeof(uintptr_t));
		uint8_t *entry = (uint8_t*)config + sizeof(SMPConfigTable);
		uint32_t i;
		for (i = 0; i < config->entrycount; i++)
		{
			switch (*entry)
			{
				case 0:
				{
					// CPU
					SMPProcessorEntry *processor = (SMPProcessorEntry*)entry;
					if (processor->flags & KE_SMP_PROCESSOR_ENABLED)
					{
						kePrint("Processor (%d).\n", processor->lapicid);
						if (processor->lapicid > max_lapic_id)
						{
							max_lapic_id = processor->lapicid;
							stacks = realloc(stacks, sizeof(uintptr_t) * (max_lapic_id + 1));
						}
						if (processor->flags & KE_SMP_PROCESSOR_BOOTSTRAP)
						{
							// Add bootstrap processor to the list
							keAddCPU(processor->lapicid);
							bootstrap_id = processor->lapicid;
						}
						else
						{
							// Add application processor
							uintptr_t stack = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
								MM_MIN_KERNEL_PAGE, 1, 0x1000);
							uintptr_t paddr = mmAllocPhysicalMemory(0, 0, 0x1000);
							mmMapKernelMemory(paddr, stack, MM_MAP_READ | MM_MAP_WRITE);
							keAddCPU(processor->lapicid);
							stacks[processor->lapicid] = stack + 0x1000;
						}
					}
					entry = entry + 20;
					break;
				}
				default:
					entry = entry + 8;
			}
		}
		
		// Start processors
		extern void *_binary_smpstart_o_start;
		extern void *_binary_smpstart_o_end;
		
		// Load startup code to physical address 0x1000
		uintptr_t smpstart = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
			MM_MIN_KERNEL_PAGE, 1, 0x1000);
		mmMapKernelMemory(0x1000, smpstart,
			MM_MAP_READ | MM_MAP_WRITE);
		
		kePrint("start: %x, end: %x\n", &_binary_smpstart_o_start, &_binary_smpstart_o_end);
		kePrint("Copying %d bytes from %x to %x\n", (uintptr_t)&_binary_smpstart_o_end - (uintptr_t)&_binary_smpstart_o_start, &_binary_smpstart_o_start, 0x2000);
		memcpy((void*)smpstart, &_binary_smpstart_o_start, (uintptr_t)&_binary_smpstart_o_end - (uintptr_t)&_binary_smpstart_o_start);
		kePrint("Copied memory.\n");
		
		*((volatile void**)(smpstart + 2)) = keSMPEntry;
		*((volatile void**)(smpstart + 6)) = (uintptr_t*)((uintptr_t)kernel_directory - MM_KERNEL_BASE);
		*((volatile void**)(smpstart + 10)) = stacks;
		
		mmMapKernelMemory(0x1000, 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
		
		// Send start signal to processors
		keAPICWriteInit();
		// TODO: 20 ms delay
		for (i = 0; i < 100000; i++)
		{
			keAPICRead(0x380);
		}
		keAPICWriteStartup(0x1000);
		// TODO: Send second startup IPI after some time
		//keAPICWriteStartup(0x1000);
		
		// Clean up
		mmMapKernelMemory(0, smpstart, 0);
		mmMapKernelMemory(0, (uint32_t)config & ~0xFFF, 0);
		mmMapKernelMemory(0, (uint32_t)fps & ~0xFFF, 0);
		// TODO: Wait until all procesors are running and are set up correctly
		//mmMapKernelMemory(0, 0x1000, 0);
		//free(stacks);
	}
}

void keStartAPs(void)
{
	start_aps = 1;
}

static uintptr_t keFindFloatingPointerStructRange(uintptr_t addr, uintptr_t size)
{
	// Map memory
	uintptr_t vaddr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, (size + 4095) & ~0xFFF);
	uint32_t i;
	for (i = 0; i < (size + 4095) / 0x1000; ++i)
	{
		mmMapKernelMemory((addr & ~0xFFF) + i * 0x1000, vaddr + i * 0x1000, MM_MAP_READ | MM_MAP_WRITE);
	}
	vaddr += addr & 0xFFF;
	// Loop through possible addresses
	uintptr_t currentaddr = vaddr;
	currentaddr = ((currentaddr + 15) / 16) * 16;
	uintptr_t fps_phys = 0;
	while (currentaddr <= (vaddr + size - 16))
	{
		FloatingPointerStruct *currentfps = (FloatingPointerStruct*)currentaddr;
		// Signature
		if (!memcmp(currentfps->signature, "_MP_", 4))
		{
			// Checksum
			uint32_t i;
			uint8_t checksum = 0;
			for (i = 0; i < 16; i++)
			{
				checksum += ((char*)currentaddr)[i];
			}
			if (checksum == 0)
			{
				fps_phys = (uintptr_t)currentfps - vaddr + addr;
				break;
			}
		}
		currentaddr += 16;
	}
	
	// Unmap memory
	for (i = 0; i < (size + 4095) / 0x1000; ++i)
	{
		mmMapKernelMemory(0, vaddr + i * 0x1000, 0);
	}
	
	return fps_phys;
}

static FloatingPointerStruct *keFindFloatingPointerStruct(void)
{
	// Find EBDA address
	uintptr_t vaddr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	mmMapKernelMemory(0, vaddr, MM_MAP_READ | MM_MAP_WRITE);
	
	/*halMapPage(kernel_directory, 0x1000, 0x0, 0x3);*/
	uint16_t ebdasegment = *(uint16_t*)(vaddr + 0x400 + 0x0E);
	mmMapKernelMemory(0, vaddr, 0);
	kePrint("EDBA: 0x%X\n", ((uint32_t)ebdasegment) << 4);
	
	uintptr_t fps_phys = 0;
	if (ebdasegment)
	{
		// Search in EDBA
		fps_phys = keFindFloatingPointerStructRange(((uint32_t)ebdasegment) << 4, 1024);
	}
	else
	{
		// Search in last kB of main memory
		fps_phys = keFindFloatingPointerStructRange(0x9FC00, 1024);
	}
	if (!fps_phys)
	{
		// Search in BIOS area
		fps_phys = keFindFloatingPointerStructRange(0xE0000, 0x20000);
	}
	if (!fps_phys) return 0;
	
	/*FloatingPointerStruct *fps = (FloatingPointerStruct*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, (uint32_t)fps, fps_phys & ~0xFFF, 0x3);
	fps = (FloatingPointerStruct*)((char*)fps + (fps_phys & 0xFFF));*/
	FloatingPointerStruct *fps = (void*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	mmMapKernelMemory(fps_phys & ~0xFFF, (uintptr_t)fps,
		MM_MAP_READ | MM_MAP_WRITE);
	fps = (FloatingPointerStruct*)((char*)fps + (fps_phys & 0xFFF));
	return fps;
}

