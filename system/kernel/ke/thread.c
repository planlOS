/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/thread.h"
#include "mm/memory.h"
#include "ke/apic.h"
#include "ke/level.h"
#include "ke/cpu.h"
#include "ke/timer.h"
#include "ke/debug.h"
#include "ke/interrupts.h"
#include <stdlib.h>
#include <string.h>

static KeSpinlock threadlock;
static KeThread **threads = 0;
static uint32_t threadcount = 0;
static uint32_t lastthread = 0;

static void keIdleThread(void)
{
	char *threadsign = (char*)0xC00B8000 + 158 - keAPICGetCPU() * 2;
	while (1)
	{
		threadsign[0] = 'i';
		threadsign[1]++;
		asm volatile("hlt");
	}
}

KeThread *keCreateThread(KeProcess *process, uintptr_t entry, uint32_t paramcount, ...)
{
	uint32_t *params = &paramcount + 1;
	KeThread *thread = malloc(sizeof(KeThread));
	memset(thread, 0, sizeof(KeThread));
	thread->process = process;
	keInitSpinlock(&thread->active);
	
	// Setup user stack
	uintptr_t userstack_virt = mmFindFreePages(&process->memory, MM_MAX_USER_PAGE,
		MM_MIN_USER_PAGE, 1, 0x1000);
	uintptr_t userstack_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapMemory(&process->memory, userstack_phys, userstack_virt, MM_MAP_READ | MM_MAP_WRITE);
	
	// Push params onto user stack
	uint32_t *stack = (uint32_t*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	mmMapKernelMemory(userstack_phys, (uintptr_t)stack, MM_MAP_READ | MM_MAP_WRITE);
	uint32_t i;
	for (i = 0; i < paramcount; i++)
	{
		stack[1024 - paramcount + i] = params[i];
	}
	stack[1024 - paramcount - 1] = 0xDEADC0DE;
	//halMapPage(kernel_directory, (uintptr_t)stack, 0, 0x0);
	mmMapKernelMemory(0, (uintptr_t)stack, 0);
	
	thread->userstack = userstack_virt;
	// Setup kernel stack
	uintptr_t kernelstack_virt = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	uintptr_t kernelstack_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapKernelMemory(kernelstack_phys, kernelstack_virt, MM_MAP_READ | MM_MAP_WRITE);
	thread->kernelstack_bottom = kernelstack_virt;
	stack = (uint32_t*)kernelstack_virt;
	stack += 1024;
	*(--stack) = 0x23;
	*(--stack) = thread->userstack + 0x1000 - paramcount * 4 - 4;
	*(--stack) = 0x202;
	*(--stack) = 0x1b;
	*(--stack) = entry;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	*(--stack) = 0x23;
	thread->kernelstack = (uintptr_t)stack;
	thread->userframe = thread->kernelstack;
	
	// Add thread to thread list
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&threadlock);
	threads = realloc(threads, (threadcount + 1) * sizeof(KeThread*));
	threads[threadcount] = thread;
	threadcount++;
	keUnlockSpinlock(&threadlock);
	keSetExecutionLevel(oldlevel);
	// Add thread to process
	keLockSpinlock(&process->lock);
	process->threads = realloc(process->threads, (process->threadcount + 1) * sizeof(KeThread*));
	process->threads[process->threadcount] = thread;
	process->threadcount++;
	keUnlockSpinlock(&process->lock);
	
	return thread;
}
int keDestroyThread(KeThread *thread)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	// Stop thread on current CPU
	thread->status = KE_THREAD_KILLED;
	KeCPU *cpu = keGetCurrentCPU();
	if (cpu && cpu->currentthread == thread)
	{
		cpu->currentthread = 0;
		//keUnlockSpinlock(&thread->active);
	}
	else
	{
		// Wait for other CPUs to schedule out of the thread
		keLockSpinlock(&thread->active);
	}
	// Delete thread from list
	keLockSpinlock(&threadlock);
	uint32_t i;
	for (i = 0; i < threadcount; i++)
	{
		if (threads[i] == thread)
		{
			memmove(&threads[i], &threads[i + 1],
				(threadcount - i - 1) * sizeof(KeThread*));
			threadcount--;
			threads = realloc(threads, threadcount * sizeof(KeThread*));
			break;
		}
	}
	keUnlockSpinlock(&threadlock);
	// Delete thread from process
	KeProcess *process = thread->process;
	if (process)
	{
		keLockSpinlock(&process->lock);
		for (i = 0; i < process->threadcount; i++)
		{
			if (process->threads[i] == thread)
			{
				memmove(&process->threads[i], &process->threads[i + 1],
					(process->threadcount - i - 1) * sizeof(KeThread*));
				process->threadcount--;
				process->threads = realloc(process->threads, process->threadcount * sizeof(KeThread*));
				break;
			}
		}
		keUnlockSpinlock(&process->lock);
	}
	keSetExecutionLevel(oldlevel);
	// Delete stacks
	// TODO: Larger stack
	// FIXME: Delete kernel stack later (it's probably currently in use)
	/*uintptr_t kernel_phys = mmKernelGetPhysAddress(thread->kernelstack_bottom);
	mmMapKernelMemory(0, thread->kernelstack_bottom, 0);
	mmFreePhysicalMemory(kernel_phys, 0x1000);*/
	// TODO: User stack
	return 0;
}

KeThread *keCloneThread(KeProcess *process, KeThread *thread, uintptr_t stack)
{
	// Create thread struct
	KeThread *newthread = malloc(sizeof(KeThread));
	memset(newthread, 0, sizeof(KeThread));
	newthread->process = process;
	keInitSpinlock(&newthread->active);
	newthread->userstack = thread->userstack;
	newthread->userstack_size = thread->userstack_size;
	// Setup kernel stack
	uintptr_t kernelstack_virt = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	uintptr_t kernelstack_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapKernelMemory(kernelstack_phys, kernelstack_virt, MM_MAP_READ | MM_MAP_WRITE);
	newthread->kernelstack_bottom = kernelstack_virt;
	memcpy((void*)kernelstack_virt, (void*)thread->kernelstack_bottom, 0x1000);
	newthread->kernelstack = kernelstack_virt + (stack - thread->kernelstack_bottom);
	newthread->userframe = newthread->kernelstack_bottom + thread->userframe - thread->kernelstack_bottom;
	// Add thread to thread list
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&threadlock);
	threads = realloc(threads, (threadcount + 1) * sizeof(KeThread*));
	threads[threadcount] = newthread;
	threadcount++;
	keUnlockSpinlock(&threadlock);
	keSetExecutionLevel(oldlevel);
	// Add thread to process
	keLockSpinlock(&process->lock);
	process->threads = realloc(process->threads, (process->threadcount + 1) * sizeof(KeThread*));
	process->threads[process->threadcount] = newthread;
	process->threadcount++;
	keUnlockSpinlock(&process->lock);
	
	return thread;

}

KeThread *keCreateKernelThread(uintptr_t entry, uint32_t paramcount, ...)
{
	uint32_t *params = &paramcount + 1;
	KeThread *thread = malloc(sizeof(KeThread));
	memset(thread, 0, sizeof(KeThread));
	keInitSpinlock(&thread->active);
	
	// Setup kernel stack
	uintptr_t kernelstack_virt = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	uintptr_t kernelstack_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapKernelMemory(kernelstack_phys, kernelstack_virt, MM_MAP_READ | MM_MAP_WRITE);
	thread->kernelstack_bottom = kernelstack_virt;
	uint32_t *stack = (uint32_t*)kernelstack_virt;
	stack += 1024;
	uint32_t i;
	for (i = 0; i < paramcount; i++)
	{
		*(--stack) = params[paramcount - i - 1];
	}
	*(--stack) = 0x0BADC0DE;
	*(--stack) = 0x202;
	*(--stack) = 0x08;
	*(--stack) = (uint32_t)entry;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	thread->kernelstack = (uintptr_t)stack;
	
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&threadlock);
	threads = realloc(threads, (threadcount + 1) * sizeof(KeThread*));
	threads[threadcount] = thread;
	threadcount++;
	keUnlockSpinlock(&threadlock);
	keSetExecutionLevel(oldlevel);
	
	return thread;
}
KeThread *keCreateIdleThread(void)
{
	KeThread *thread = malloc(sizeof(KeThread));
	memset(thread, 0, sizeof(KeThread));
	keInitSpinlock(&thread->active);
	
	// Setup kernel stack
	uintptr_t kernelstack_virt = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x2000);
	kernelstack_virt += 0x1000;
	uintptr_t kernelstack_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapKernelMemory(kernelstack_phys, kernelstack_virt - 0x1000, MM_MAP_READ | MM_MAP_WRITE);
	kernelstack_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapKernelMemory(kernelstack_phys, kernelstack_virt, MM_MAP_READ | MM_MAP_WRITE);
	thread->kernelstack_bottom = kernelstack_virt;
	uint32_t *stack = (uint32_t*)kernelstack_virt;
	stack += 1024;
	*(--stack) = 0x202;
	*(--stack) = 0x08;
	*(--stack) = (uint32_t)keIdleThread;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x0;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	*(--stack) = 0x10;
	thread->kernelstack = (uintptr_t)stack;
	
	return thread;
}

void keThreadSetParam(KeThread *thread, uint8_t index, uint32_t value)
{
	IntStackFrame *isf = (void*)thread->kernelstack;
	switch (index)
	{
		case 0:
			isf->eax = value;
			break;
		case 1:
			isf->ebx = value;
			break;
		case 2:
			isf->ecx = value;
			break;
		case 3:
			isf->edx = value;
			break;
		case 4:
			isf->esi = value;
			break;
		case 5:
			isf->edi = value;
			break;
		default:
			break;
	}
}
uint32_t keThreadGetParam(KeThread *thread, uint8_t index)
{
	IntStackFrame *isf = (void*)thread->kernelstack;
	switch (index)
	{
		case 0:
			return isf->eax;
		case 1:
			return isf->ebx;
		case 2:
			return isf->ecx;
		case 3:
			return isf->edx;
		case 4:
			return isf->esi;
		case 5:
			return isf->edi;
		default:
			return 0;
	}
}

KeThread *keGetSchedulableThread(int oldthread)
{
	KeThread *old = keGetCurrentCPU()->currentthread;
	keLockSpinlock(&threadlock);
	if (threadcount == 0) return 0;
	uint32_t startthread = lastthread;
	startthread %= threadcount;
	uint32_t i = startthread;
	do
	{
		i++;
		i %= threadcount;
		if ((threads[i]->status == KE_THREAD_SLEEP) && (threads[i]->timeout <= keGetTime()))
		{
			threads[i]->status = KE_THREAD_RUNNING;
			threads[i]->timeout = 0;
		}
		if ((threads[i]->status == KE_THREAD_RUNNING) && !keTryLockSpinlock(&threads[i]->active))
		{
			lastthread = i;
			keUnlockSpinlock(&threadlock);
			return threads[i];
		}
	}
	while (i != startthread);
	keUnlockSpinlock(&threadlock);
	if (oldthread)
		return old;
	return 0;
}

void keSleep(uint32_t ms)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("keSleep called at KE_LEVEL_HIGH!\n");
		return;
	}
	KeCPU *cpu = keGetCurrentCPU();
	if (!cpu) return;
	cpu->currentthread->timeout = keGetTime() + ms + keGetTimePerTick();
	cpu->currentthread->status = KE_THREAD_SLEEP;
	// Schedule out of the thread
	asm volatile("int $0x32");
}
void keExitThread(void)
{
	asm volatile("int $0x33");
}

