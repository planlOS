/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/elf.h"
#include "ke/errors.h"
#include <string.h>

int keElfMapProgram(MmAddressSpace *addrspace, char *program, uint32_t size)
{
	ElfHeader *header = (ElfHeader*)program;
	ElfProgramHeader *pheaders = (ElfProgramHeader*)(program + header->e_phoff);
	uint32_t i;
	for (i = 0; i < header->e_phnum; i++)
	{
		if (pheaders[i].p_type == PT_LOAD)
		{
			uintptr_t secstart = pheaders[i].p_vaddr & ~0xFFF;
			uintptr_t secend = (pheaders[i].p_vaddr + pheaders[i].p_memsz + 4095) & ~0xFFF;
			uintptr_t secpagecount = (secend - secstart) / 4096;
			uint32_t offset = pheaders[i].p_vaddr & 0xFFF;
			uintptr_t kerneladdr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
				MM_MIN_KERNEL_PAGE, 1, secpagecount * 0x1000);
			uint32_t j;
			for (j = 0; j < secpagecount; j++)
			{
				uintptr_t paddr = mmAllocPhysicalMemory(0, 0, 0x1000);
				mmMapKernelMemory(paddr, kerneladdr + j * 0x1000,
					MM_MAP_READ | MM_MAP_WRITE);
				mmMapMemory(addrspace, paddr, secstart + j * 0x1000,
					MM_MAP_READ | MM_MAP_WRITE | MM_MAP_EXECUTE);
			}
			memset((void*)kerneladdr, 0, j * 0x1000);
			memcpy((char*)kerneladdr + offset, program + pheaders[i].p_offset,
				pheaders[i].p_filesz);
			for (j = 0; j < secpagecount; j++)
			{
				mmMapKernelMemory(0, kerneladdr + j * 0x1000, 0);
			}
		}
	}

	return KE_ERROR_UNKNOWN;
}

