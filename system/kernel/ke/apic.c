/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/apic.h"
#include "ke/cpu.h"
#include "ke/debug.h"
#include "mm/memory.h"

static uintptr_t apic_base_phys = 0;
static uintptr_t apic_base = 0;

int keAPICInit(void)
{
	// TODO: Check whether APIC is available
	// Get physical address
	//keWriteMSR(0x1B, 0xFEE00000);
	apic_base_phys = keReadMSR(0x1B) & ~0xFFF;
	kePrint("APIC: Address: %X\n", apic_base_phys);
	
	// Map mmio space into virtual address space
	/*apic_base = halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
	halMapPage(kernel_directory, apic_base, apic_base_phys & ~0xFFF, 0x3);
	apic_base = apic_base + (apic_base_phys & 0xFFF);*/
	
	apic_base = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	mmMapKernelMemory(apic_base_phys, apic_base,
		MM_MAP_READ | MM_MAP_WRITE | MM_MAP_UNCACHEABLE);
	
	keAPICWrite(0x80, 0x20);
	keAPICWrite(0x320, 0x10000);
	keAPICWrite(0x340, 0x10000);
	keAPICWrite(0x350, 0x8700);
	keAPICWrite(0x360, 0x400);
	keAPICWrite(0x370, 0x10000);
	// Activate APIC
	keAPICWrite(0xF0, 0x10F);
	
	// Timer
	/*keAPICWrite(0x3e0, 0xb);
	keAPICWrite(0x320, 0x20040);
	keAPICWrite(0x380, 0x100);
	
	int i;
	for (i = 0; i < 100000; i++)
	{
		keAPICRead(0x380);
	}
	kePrint("APIC Timer: 0x%X\n", keAPICRead(0x390));*/
	
	
	return 1;
}

uint32_t keAPICGetCPU(void)
{
	uint32_t cpuinfo[4];
	keCPUID(1, cpuinfo);
	return cpuinfo[1] >> 24;
}

void keAPICWrite(uint16_t offset, uint32_t value)
{
	*((volatile uint32_t*)(apic_base + offset)) = value;
}
uint32_t keAPICRead(uint16_t offset)
{
	return *((volatile uint32_t*)(apic_base + offset));
}

void keAPICWriteInit(void)
{
	if (!apic_base) return;
	keAPICWrite(0x310, 0);
	keAPICWrite(0x300, 0xC4500);
}
void keAPICWriteStartup(uintptr_t entry)
{
	if (!apic_base) return;
	keAPICWrite(0x310, 0);
	keAPICWrite(0x300, 0xC4600 | ((entry / 0x1000) && 0xFF));
}
void keAPICBroadcast(uint8_t intno, int self)
{
	if (!apic_base) return;
	keAPICWrite(0x310, 0);
	if (self)
		keAPICWrite(0x300, intno | 0x4000 | (2 << 18));
	else
		keAPICWrite(0x300, intno | 0x4000 | (3 << 18));
}

