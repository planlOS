/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/gdt.h"
#include "ke/cpu.h"
#include "ke/apic.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

TSS *tss = 0;

static GDTEntry gdt[32];
GDTPtr gp;


static void keSetGDTEntry(unsigned int index, unsigned int base, unsigned int limit,
	int bytegran, int rw, int code, unsigned char privlevel, int tss)
{
	gdt[index].limit_low = limit & 0xFFFF;
	gdt[index].base_low = base & 0xFFFF;
	gdt[index].base_middle = (base & 0xFF0000) >> 16;
	gdt[index].base_high = (base & 0xFF000000) >> 24;
	gdt[index].granularity = ((limit & 0xF0000) >> 16) | 0x40;
	gdt[index].access = 0x90; // Present + 00010000b
	
	gdt[index].access |= (privlevel & 0x3) << 5;
	if (code) gdt[index].access |= 0x8;
	if (rw) gdt[index].access |= 0x2;
	if (!bytegran) gdt[index].granularity |= 0x80;
	if (tss) {
		gdt[index].access |= 0x9;
		gdt[index].access &= ~0x10;
	}
}

void keInstallStartupGDT(void)
{
	// Pointer to the GDT
	gp.limit = sizeof(GDTEntry) * 5;
	gp.base = (unsigned int)&gdt;
	
	// Null selector
	memset(gdt, 0, sizeof(GDTEntry));
	// Kernel selectors
	keSetGDTEntry(1, 0, 0xFFFFF, 0, 0, 1, 0, 0);
	keSetGDTEntry(2, 0, 0xFFFFF, 0, 1, 0, 0, 0);
	// User selectors
	keSetGDTEntry(3, 0, 0xFFFFF, 0, 0, 1, 3, 0);
	keSetGDTEntry(4, 0, 0xFFFFF, 0, 1, 0, 3, 0);
	keFlushGDT();
}

void keInstallGDT(void)
{
	// We need one TSS per CPU
	uint8_t cpucount = keGetCPUCount();
	
	// Pointer to the GDT
	gp.limit = sizeof(GDTEntry) * (5 + cpucount);
	gp.base = (unsigned int)&gdt;
	
	// Null selector
	memset(gdt, 0, sizeof(GDTEntry));
	// Kernel selectors
	keSetGDTEntry(1, 0, 0xFFFFF, 0, 0, 1, 0, 0);
	keSetGDTEntry(2, 0, 0xFFFFF, 0, 1, 0, 0, 0);
	// User selectors
	keSetGDTEntry(3, 0, 0xFFFFF, 0, 0, 1, 3, 0);
	keSetGDTEntry(4, 0, 0xFFFFF, 0, 1, 0, 3, 0);
	// TSS
	tss = malloc(sizeof(TSS) * cpucount);
	memset(tss, 0, sizeof(TSS) * cpucount);
	uint32_t i;
	for (i = 0; i < cpucount; i++)
	{
		tss[i].ss0 = 0x10;
		tss[i].es = 0x23;
		tss[i].cs = 0x1b;
		tss[i].ss = 0x23;
		tss[i].ds = 0x23;
		tss[i].fs = 0x23;
		tss[i].gs = 0x23;
		tss[i].iobitmap[0] = 0xFF;
		keSetGDTEntry(5 + i, (unsigned int)&tss[i], sizeof(TSS) - 1, 1, 0, 0, 3, 1);
	}
	
	keFlushGDT();
	asm volatile("ltrw %%ax"::"a"(0x28 + keAPICGetCPU() * 8));
}

void keFlushGDT(void)
{
	asm volatile(
	"lgdt (gp)\n"
	"mov $0x10, %ax\n"
	"mov %ax, %ds\n"
	"mov %ax, %es\n"
	"mov %ax, %fs\n"
	"mov %ax, %gs\n"
	"mov %ax, %ss\n"
	"ljmpl *(target_addr)\n"
	"target_addr:\n"
	".long keFlushGDT2\n"
	".word 0x08, 0\n"
	"keFlushGDT2:\n");
}

