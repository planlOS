/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/timer.h"
#include "ke/ports.h"
#include "ke/apic.h"
#include "ke/interrupts.h"
#include "ke/debug.h"
#include "ke/errors.h"
#include <stdlib.h>
#include <string.h>

// TODO: Make this work without APIC

static volatile uint32_t interrupt_count = 0;
static uint32_t ticks_per_usec = 0;
static volatile uint32_t acpi_start = 0;

static uint32_t us_per_tick = 0;

static void keTimerCalibrationHandler(KeCPU *cpu, uint8_t intno)
{
	if (interrupt_count == 0)
	{
		// Initialize APIC timer
		keAPICWrite(0x3e0, 0xb);
		keAPICWrite(0x320, 0x20020);
		keAPICWrite(0x380, 0xFFFFFFFF);
	}
	else if (interrupt_count == 1)
	{
		acpi_start = keAPICRead(0x390);
	}
	else if (interrupt_count == 2)
	{
		// Get APIC timer speed
		ticks_per_usec = (acpi_start - keAPICRead(0x390)) / 10000;
	}
	interrupt_count++;
}

static void keTimerHandler(KeCPU *cpu, uint8_t intno)
{
	// Next timer
	KeTimer *oldtimer = cpu->next_timer;
	cpu->next_timer = cpu->next_timer->next;
	oldtimer->active = 0;
	oldtimer->prev = 0;
	oldtimer->next = 0;
	oldtimer->queued = 0;
	// Increase current time and restart periodic timer
	if (oldtimer->periodic)
	{
		cpu->time += oldtimer->time;
		keSetTimer(oldtimer, oldtimer->time, 1);
	}
	else
	{
		cpu->time = oldtimer->time;
	}
	if (cpu->next_timer != oldtimer)
	{
		// Start next timer if it wasn't already started
		if (cpu->time > cpu->next_timer->targettime)
		{
			cpu->next_timer->targettime = cpu->time + 30;
		}
		keAPICWrite(0x380, ticks_per_usec * (cpu->next_timer->targettime - cpu->time));
		cpu->next_timer->active = 1;
	}
	if (oldtimer->callback) oldtimer->callback(oldtimer);
	
	if (oldtimer == cpu->sched_timer)
	{
		char *cpusign = (char*)0xC00B8000 + keAPICGetCPU() * 2;
		cpusign[0] = '*';
		cpusign[1]++;
	}
	// Schedule to another thread here
	KeThread *newthread = keGetSchedulableThread(1);
	if (newthread)
	{
		cpu->currentthread = newthread;
	}
	else
	{
		keLockSpinlock(&cpu->idlethread->active);
		cpu->currentthread = cpu->idlethread;
	}
	// EOI
	keAPICWrite(0xb0, 0);
}

void keInitTimer(uint32_t frequency)
{
	// Calibrate APIC timer
	keRegisterIRQ(0, keTimerCalibrationHandler);
	// Set PIT to 10ms-interval
	uint16_t divisor = (uint16_t)(1193180 / 100);
	outb(0x43, 0x34);
	outb(0x40, divisor & 0xFF);
	outb(0x40, divisor >> 8);
	asm ("sti");
	while (interrupt_count < 3)
	{
		char *cpusign = (char*)0xC00B8000 + keAPICGetCPU() * 2;
		cpusign[0] = 'k';
		cpusign[1]++;
		asm("hlt");
	}
	// Stop APIC timer
	keAPICWrite(0x320, 0x10000);
	asm("cli");
	keRegisterIRQ(0, 0);
	keRegisterIRQ(0x10, keTimerHandler);
	us_per_tick = 1000000 / frequency;
}

void keInstallTimer(void)
{
	keAPICWrite(0x3e0, 0xb);
	keAPICWrite(0x320, 0x20030);
	KeCPU *cpu = keGetCurrentCPU();
	cpu->sched_timer = keCreateTimer();
	cpu->starttime = keGetUnixTime();
	keSetTimer(cpu->sched_timer, us_per_tick, 1);
	// keRegisterIRQ(0, keTimerHandler);
}

KeTimer *keCreateTimer(void)
{
	KeTimer *timer = malloc(sizeof(KeTimer));
	memset(timer, 0, sizeof(KeTimer));
	return timer;
}
int keDestroyTimer(KeTimer *timer)
{
	free(timer);
	return 0;
}
int keSetTimer(KeTimer *timer, uint64_t time, int periodic)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	KeCPU *cpu = keGetCurrentCPU();
	if (timer->queued) keCancelTimer(timer);
	// Store data in the struct
	timer->time = time;
	uint32_t currenttime = keGetTime();
	if (periodic)
	{
		timer->targettime = currenttime + time;
	}
	else
	{
		timer->targettime = time;
	}
	timer->periodic = periodic;
	// Add timer to timer queue
	if (!cpu->next_timer)
	{
		keAPICWrite(0x380, ticks_per_usec * (timer->targettime - currenttime));
		timer->active = 1;
		cpu->next_timer = timer;
	}
	else
	{
		if (cpu->next_timer->targettime > timer->targettime)
		{
			// First timer to expire
			if (currenttime >= timer->targettime)
			{
				timer->targettime = currenttime + 1;
			}
			keAPICWrite(0x380, ticks_per_usec * (timer->targettime - currenttime));
			timer->next = cpu->next_timer;
			cpu->next_timer = timer;
			timer->active = 1;
		}
		else
		{
			// Add later in the queue
			KeTimer *prev_timer = cpu->next_timer;
			while (prev_timer->next && (prev_timer->next->targettime < timer->targettime))
			{
				prev_timer = prev_timer->next;
			}
			timer->next = prev_timer->next;
			prev_timer->next = timer;
		}
	}
	timer->queued = 1;
	keSetExecutionLevel(oldlevel);
	return 0;
}
int keCancelTimer(KeTimer *timer)
{
	// TODO
	return KE_ERROR_UNKNOWN;
}
int keAdjustTimer(KeTimer *timer, uint64_t time)
{
	// TODO: Currently only works for timers on the current CPU!
	if (!timer->queued) return KE_ERROR_UNKNOWN;
	if (time == 0) time = 1;
	// Set target time
	uint32_t currenttime = keGetTime();
	timer->targettime = currenttime + time;
	// TODO: Move back in the queue if needed
	if (timer->active) keAPICWrite(0x380, ticks_per_usec * (timer->targettime - currenttime));
	return 0;
}

uint64_t keGetTime(void)
{
	KeCPU *cpu = keGetCurrentCPU();
	if (!cpu->next_timer || !cpu->next_timer->active) return cpu->time;
	uint32_t tickcount = keAPICRead(0x390);
	return cpu->next_timer->targettime - tickcount / ticks_per_usec;
}
uint32_t keGetTimePerTick(void)
{
	return us_per_tick;
}

static uint8_t keReadCMOS(uint8_t index)
{
	uint8_t indexbyte = inb(0x70);
	indexbyte = (indexbyte & 0x80) | (index & 0x7F);
	outb(0x70, indexbyte);
	
	return inb(0x71);
}

static unsigned int days[12] = {
31,
31 + 28,
31 + 28 + 31,
31 + 28 + 31 + 30,
31 + 28 + 31 + 30 + 31,
31 + 28 + 31 + 30 + 31 + 30,
31 + 28 + 31 + 30 + 31 + 30 + 31,
31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30,
31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31
};

static int keIsLeapYear(unsigned int year)
{
	if (year % 400 == 0) return 1;
	if (year % 100 == 0) return 0;
	if (year % 4 == 0) return 1;
	return 0;
}

uint32_t keGetUnixTime(void)
{
	// Read in data
	uint32_t seconds = keReadCMOS(0x0);
	uint32_t minutes = keReadCMOS(0x2);
	uint32_t hours = keReadCMOS(0x4);
	uint32_t day = keReadCMOS(0x7);
	uint32_t month = keReadCMOS(0x8);
	uint32_t year = keReadCMOS(0x9);
	uint32_t century = keReadCMOS(0x32);
	// BCD
	seconds = (seconds & 0xF) + (seconds >> 4) * 10;
	minutes = (minutes & 0xF) + (minutes >> 4) * 10;
	hours = (hours & 0xF) + (hours >> 4) * 10;
	day = (day & 0xF) + (day >> 4) * 10;
	month = (month & 0xF) + (month >> 4) * 10;
	year = (year & 0xF) + (year >> 4) * 10;
	century = (century & 0xF) + (century >> 4) * 10;
	year += century * 100;
	day--;
	// Add data
	if (month > 1)
		day += days[month - 2];
	if ((month > 2) && keIsLeapYear(year))
		day++;
	year -= 1900;
	int time = seconds + minutes * 60 + hours * 3600 + day * 86400
		+ (year - 70) * 31536000 + ((year - 69) / 4) * 86400
		- ((year - 1) / 100) * 86400 + ((year + 299) / 400) * 86400;
	return time;
}

