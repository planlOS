/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/process.h"
#include "ke/thread.h"
#include "ke/level.h"
#include "ke/cpu.h"
#include "ke/interrupts.h"
#include "ke/debug.h"
#include "sys/syscall.h"
#include <stdlib.h>
#include <string.h>
#include <planlos/signals.h>

static uint32_t lastpid = 0;

KeSpinlock process_lock;
KeProcess **processes = 0;
uint32_t processcount = 0;

int keInitProcessManager(void)
{
	// Create signal entry
	uintptr_t paddr = mmAllocPhysicalMemory(0, 0, 0x1000);
	mmMapKernelMemory(paddr, 0xFFFFF000,
		MM_MAP_READ | MM_MAP_WRITE | MM_MAP_USER);
	extern void *_binary_signal_o_start;
	extern void *_binary_signal_o_end;
	memcpy((void*)0xFFFFF000, &_binary_signal_o_start,
		(uintptr_t)&_binary_signal_o_end - (uintptr_t)&_binary_signal_o_start);

	// Create process list
	processes = 0;
	processcount = 0;
	keInitSpinlock(&process_lock);
	return 0;
}

KeProcess *keCreateProcess(void)
{
	// Create empty process
	KeProcess *newprocess = malloc(sizeof(KeProcess));
	memset(newprocess, 0, sizeof(KeProcess));
	newprocess->fd = malloc(sizeof(FsFileHandle*) * 3);
	memset(newprocess->fd, 0, sizeof(FsFileHandle*) * 3);
	newprocess->fdcount = 3;
	newprocess->cwd = strdup("/");
	int status = mmCreateAddressSpace(&newprocess->memory);
	if (status)
	{
		free(newprocess);
		return 0;
	}
	keInitSpinlock(&newprocess->lock);
	newprocess->pid = ++lastpid;
	// Add process to process list
	keLockSpinlock(&process_lock);
	processes = realloc(processes, (processcount + 1) * sizeof(KeProcess*));
	processes[processcount] = newprocess;
	processcount++;
	// Lock process so that caller can use it safely
	//keLockSpinlock(&newprocess->lock);
	keUnlockSpinlock(&process_lock);
	return newprocess;
}
int keTerminateProcess(KeProcess *process)
{
	// Close files
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_NORMAL);
	uint32_t i;
	for (i = 0; i < process->fdcount; i++)
	{
		if (process->fd[i]) fsClose(process->fd[i]);
	}
	keSetExecutionLevel(oldlevel);
	if (process->waitthread)
	{
		// Destroy process
		process->waitthread->status = KE_THREAD_RUNNING;
		return keDestroyProcess(process);
	}
	else
	{
		// Kill threads
		while (process->threadcount > 0)
		{
			keDestroyThread(process->threads[0]);
		}
		process->zombie = 1;
		return 0;
	}
}
int keDestroyProcess(KeProcess *process)
{
	// Kill threads
	while (process->threadcount > 0)
	{
		keDestroyThread(process->threads[0]);
	}
	// Remove process from list
	keLockSpinlock(&process_lock);
	uint32_t i;
	for (i = 0; i < processcount; i++)
	{
		if (processes[i] == process)
		{
			memmove(&processes[i], &processes[i + 1],
				(processcount - i - 1) * sizeof(KeProcess*));
			processcount--;
			processes = realloc(processes, processcount * sizeof(KeProcess*));
			break;
		}
	}
	// Destroy address space
	mmDestroyAddressSpace(&process->memory);
	// Delete process
	free(process->cwd);
	free(process);
	keUnlockSpinlock(&process_lock);
	return 0;
}

int keClearProcess(KeProcess *process, KeThread *current)
{
	// Kill threads
	while (process->threadcount > 1)
	{
		if (process->threads[0] == current)
			keDestroyThread(process->threads[1]);
		else
			keDestroyThread(process->threads[0]);
	}
	// Clear address space
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	mmClearAddressSpace(&process->memory);
	keUnlockSpinlock(mmGetMemoryLock());
	keSetExecutionLevel(oldlevel);
	return 0;
}

KeProcess *keGetProcess(uint32_t pid)
{
	keLockSpinlock(&process_lock);
	uint32_t i;
	for (i = 0; i < processcount; i++)
	{
		if (processes[i]->pid == pid)
		{
			KeProcess *process = processes[i];
			keUnlockSpinlock(&process_lock);
			return process;
		}
	}
	keUnlockSpinlock(&process_lock);
	return 0;
}

void keSendSignal(KeProcess *process, KeThread *thread, int signal)
{
	KeCPU *cpu = keGetCurrentCPU();
	if (signal == SIGKILL)
	{
		// Kill process
		KeThread *currentthread = cpu->currentthread;
		keTerminateProcess(thread->process);
		if (currentthread->process == process)
			asm volatile("int $0x32");
		return;
	}
	// TODO: SIGSTOP
	// Find and lock target thread
	if (!thread)
	{
		if (process->threadcount == 0) return;
		thread = process->threads[0];
	}
	if (thread)
	{
		if (thread != cpu->currentthread)
		{
			if (keTryLockSpinlock(&thread->active))
			{
				// Someone uses the thread right now
				thread->status = KE_THREAD_INUSE;
				keLockSpinlock(&thread->active);
				if (thread->status == KE_THREAD_INUSE)
				{
					thread->status = KE_THREAD_RUNNING;
				}
			}
		}
	}
	// Send signal
	uintptr_t handler = process->signal_handlers[signal];
	if (thread && handler && thread->userframe)
	{
		// Reset signal handler in case the handler is broken
		if (signal == SIGSEGV)
			process->signal_handlers[signal] = 0;
		// Get user stack
		IntStackFrame *userframe = (IntStackFrame*)thread->userframe;
		uint32_t *stack = sysMapUserMemory(thread, userframe->esp - 16, 16, 1);
		if (!stack)
		{
			// TODO
			kePrint("User stack broken.\n");
			if (thread != cpu->currentthread)
			{
				keUnlockSpinlock(&thread->active);
			}
			keTerminateProcess(thread->process);
			if (cpu->currentthread->process == process)
				asm volatile("int $0x32");
			return;
		}
		else
		{
			// Push eip, ebp, signal and handler onto the stack
			stack[3] = userframe->eip;
			stack[2] = userframe->ebp;
			userframe->ebp = userframe->esp - 8;
			stack[1] = signal;
			stack[0] = handler;
			userframe->esp = userframe->esp - 16;
			userframe->eip = 0xFFFFF000;
			thread->status = KE_THREAD_RUNNING;
		}
	}
	else
	{
		// TODO: Default actions
		switch (signal)
		{
			case SIGINT:
				{
					// Unlock thread again
					if (thread != cpu->currentthread)
					{
						keUnlockSpinlock(&thread->active);
					}
					// Kill process
					KeThread *currentthread = cpu->currentthread;
					keTerminateProcess(thread->process);
					if (currentthread->process == process)
						asm volatile("int $0x32");
					return;
				}
		}
	}
	// Unlock thread again
	if (thread != cpu->currentthread)
	{
		keUnlockSpinlock(&thread->active);
	}
}

