/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/module.h"
#include "ke/debug.h"
#include "ke/errors.h"
#include "mm/memory.h"
#include "ke/elf.h"
#include <string.h>
#include <stdlib.h>

typedef struct
{
	char *name;
	uintptr_t addr;
	uintptr_t size;
} KeKernelSymbol;

typedef struct
{
	char *name;
	uintptr_t start;
	uintptr_t size;
} KeKernelModule;

KeKernelSymbol *symbols = 0;
uint32_t symbolcount = 0;
KeKernelSymbol *private_symbols = 0;
uint32_t private_symbolcount = 0;

KeKernelModule *modules = 0;
uint32_t modulecount = 0;

static void keAddSymbol(char *name, uintptr_t addr, uintptr_t size)
{
	symbols = realloc(symbols, sizeof(KeKernelSymbol) * (symbolcount + 1));
	symbols[symbolcount].name = name;
	symbols[symbolcount].addr = addr;
	symbols[symbolcount].size = size;
	symbolcount++;
}
static void keAddPrivateSymbol(char *name, uintptr_t addr, uintptr_t size)
{
	private_symbols = realloc(private_symbols, sizeof(KeKernelSymbol) * (private_symbolcount + 1));
	private_symbols[private_symbolcount].name = name;
	private_symbols[private_symbolcount].addr = addr;
	private_symbols[private_symbolcount].size = size;
	private_symbolcount++;
}
int keInitKernelSymbols(MultibootInfo *info)
{
	// Load kernel ELF sections
	uintptr_t sec_addr_phys = info->u.elfsections.addr;
	uintptr_t sec_size = info->u.elfsections.size;
	uint32_t sec_count = info->u.elfsections.num;
	// FIXME: Execution level
	uintptr_t sec_addr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, (sec_size + 0xFFF) & ~0xFFF);
	uint32_t i;
	for (i = 0; i < (sec_size + 0xFFF) / 0x1000; i++)
	{
		mmMapKernelMemory(sec_addr_phys + i * 0x1000, sec_addr + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	
	ElfSectionHeader *sheaders = (ElfSectionHeader*)sec_addr;
	ElfSectionHeader *namesection = &sheaders[info->u.elfsections.shindex];
	char *nametable = (char*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, (namesection->sh_size + 0xFFF) & ~0xFFF);
	for (i = 0; i < (namesection->sh_size + 0xFFF) / 0x1000; i++)
	{
		mmMapKernelMemory((namesection->sh_addr & ~0xFFF) + i * 0x1000, (uintptr_t)nametable + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	nametable += namesection->sh_addr & 0xFFF;
	ElfSectionHeader *stringsection = 0;
	ElfSectionHeader *symbolsection = 0;
	for (i = 1; i < sec_count; i++)
	{
		if (!strcmp(nametable + sheaders[i].sh_name, ".strtab"))
		{
			stringsection = &sheaders[i];
		}
		else if (!strcmp(nametable + sheaders[i].sh_name, ".symtab"))
		{
			symbolsection = &sheaders[i];
		}
	}
	if (!stringsection || !symbolsection)
	{
		// TODO: Panic
		kePrint("Error: No symbol table.\n");
		while (1);
	}
	// Map symbol table
	char *strtab = (char*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, ((stringsection->sh_addr & 0xFFF) + stringsection->sh_size + 0xFFF) & ~0xFFF);
	for (i = 0; i < ((stringsection->sh_addr & 0xFFF) + stringsection->sh_size + 0xFFF) / 0x1000; i++)
	{
		mmMapKernelMemory((stringsection->sh_addr & ~0xFFF) + i * 0x1000, (uintptr_t)strtab + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	strtab += stringsection->sh_addr & 0xFFF;
	ElfSymbol *symbols = (ElfSymbol*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, ((symbolsection->sh_addr & 0xFFF) + symbolsection->sh_size + 0xFFF) & ~0xFFF);
	for (i = 0; i < ((symbolsection->sh_addr & 0xFFF) + symbolsection->sh_size + 0xFFF) / 0x1000; i++)
	{
		mmMapKernelMemory((symbolsection->sh_addr & ~0xFFF) + i * 0x1000, (uintptr_t)symbols + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	symbols = (ElfSymbol*)((char*)symbols + (symbolsection->sh_addr & 0xFFF));
	// Load kernel symbols
	for (i = 0; i < symbolsection->sh_size / sizeof(ElfSymbol); i++)
	{
		if (ELF32_ST_BIND(symbols[i].st_info) == STB_GLOBAL)
		{
			keAddSymbol(strtab + symbols[i].st_name, symbols[i].st_value, symbols[i].st_size);
		}
		else if (ELF32_ST_TYPE(symbols[i].st_info) == STT_FUNCTION)
		{
			keAddPrivateSymbol(strtab + symbols[i].st_name, symbols[i].st_value, symbols[i].st_size);
		}
	}
	return 0;
}
static int keLoadModule(char *data, uint32_t size, char *name)
{
	ElfHeader *header = (ElfHeader*)data;
	ElfProgramHeader *pheaders = (ElfProgramHeader*)(data + header->e_phoff);
	// Get module size
	uint32_t modulesize = 0;
	uint32_t i;
	for (i = 0; i < header->e_phnum; i++)
	{
		if (pheaders[i].p_vaddr + pheaders[i].p_memsz > modulesize)
		{
			modulesize = pheaders[i].p_vaddr + pheaders[i].p_memsz;
		}
	}
	// Allocate memory
	char *module = (char*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, (modulesize + 4095) & ~0xFFF);
	for (i = 0; i < (modulesize + 4095) / 0x1000; i++)
	{
		uintptr_t paddr = mmAllocPhysicalMemory(0, 0, 0x1000);
		mmMapKernelMemory(paddr, (uintptr_t)module + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE | MM_MAP_EXECUTE);
	}
	memset(module, 0, modulesize);
	kePrint("Module: %x-%x\n", module, module + size);
	// Copy data
	for (i = 0; i < header->e_phnum; i++)
	{
		if (pheaders[i].p_type == PT_LOAD)
		{
			memcpy(module + pheaders[i].p_vaddr, data + pheaders[i].p_offset,
				pheaders[i].p_filesz);
		}
	}
	ElfSectionHeader *sheaders = (ElfSectionHeader*)(data + header->e_shoff);
	char *sectionnames = data + sheaders[header->e_shstrndx].sh_offset;
	// Find needed sections
	char *dynstr = 0;
	ElfSymbol *dynsym = 0;
	uint32_t symbolcount = 0;
	for (i = 0; i < header->e_shnum; i++)
	{
		if (!strcmp(sectionnames + sheaders[i].sh_name, ".dynstr"))
		{
			dynstr = data + sheaders[i].sh_offset;
		}
		else if (!strcmp(sectionnames + sheaders[i].sh_name, ".dynsym"))
		{
			dynsym = (ElfSymbol*)(data + sheaders[i].sh_offset);
			symbolcount = sheaders[i].sh_size / sizeof(ElfSymbol);
		}
	}
	if (!dynstr || !dynsym)
	{
		kePrint("Needed sections not found.\n");
		return KE_ERROR_UNKNOWN;
	}
	// Resolve symbols
	void (*modLoad)(void);
	void (*modUnload)(void);
	int everything_resolved = 1;
	for (i = 1; i < symbolcount; i++)
	{
		if (dynsym[i].st_shndx == SHN_UNDEF)
		{
			uintptr_t symvalue = keResolveSymbol(dynstr + dynsym[i].st_name);
			if (!symvalue)
			{
				kePrint("Undefined symbol: %s\n", dynstr + dynsym[i].st_name);
				everything_resolved = 0;
			}
			else
			{
				dynsym[i].st_value = symvalue;
			}
		}
		else
		{
			dynsym[i].st_value += (uintptr_t)module;
			if (!strcmp(dynstr + dynsym[i].st_name, "modLoad"))
			{
				modLoad = (void*)dynsym[i].st_value;
			}
			else if (!strcmp(dynstr + dynsym[i].st_name, "modUnload"))
			{
				modUnload = (void*)dynsym[i].st_value;
			}
		}
	}
	if (!everything_resolved)
	{
		return KE_ERROR_UNKNOWN;
	}
	
	// Relocate module
	for (i = 0; i < header->e_shnum; i++)
	{
		if (sheaders[i].sh_type == SHT_REL)
		{
			ElfRel *reltable = (ElfRel*)(module + sheaders[i].sh_offset);
			uint32_t relcount = sheaders[i].sh_size / sizeof(ElfRel);
			uint32_t j;
			for (j = 0; j < relcount; j++)
			{
				switch (ELF32_R_TYPE(reltable[j].r_info))
				{
					case R_386_32:
					case R_386_GLOB_DAT:
					case R_386_JUMP_SLOT:
						*((uintptr_t*)(module + reltable[j].r_offset)) = dynsym[ELF32_R_SYM(reltable[j].r_info)].st_value;
						break;
					case R_386_RELATIVE:
						*((char**)(module + reltable[j].r_offset)) = data + *((uintptr_t*)(module + reltable[j].r_offset));
						break;
					default:
						kePrint("Unsupported relocation type: %d\n", ELF32_R_TYPE(reltable[j].r_info));
						return KE_ERROR_NOTIMPLEMENTED;
				}
			}
		}
		else if (sheaders[i].sh_type == SHT_RELA)
		{
			kePrint("SHT_RELA not yet supported!\n");
			return KE_ERROR_NOTIMPLEMENTED;
		}
	}
	// Add module to module list
	modules = realloc(modules, (modulecount + 1) * sizeof(KeKernelModule));
	modules[modulecount].name = name;
	modules[modulecount].start = (uintptr_t)module;
	modules[modulecount].size = modulesize;
	modulecount++;
	// Call module init function
	if (!modLoad || !modUnload)
	{
		kePrint("Entry functions not found!\n");
		return KE_ERROR_UNKNOWN;
	}
	modLoad();
	
	return 0;
}
int keInitModules(MultibootInfo *info)
{
	// Load module list
	uint32_t pagecount = ((info->modaddr & 0xFFF) + info->modcount * sizeof(MbModule) + 4095) / 0x1000;
	MbModule *mod = (MbModule*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, pagecount * 0x1000);
	uint32_t i;
	for (i = 0; i < pagecount; i++)
	{
		mmMapKernelMemory((info->modaddr & ~0xFFF) + i * 0x1000, (uintptr_t)mod + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	mod = (MbModule*)((uintptr_t)mod + (info->modaddr & 0xFFF));
	// Loop through modules
	for (i = 0; i < info->modcount; i++)
	{
		// Map module data
		uint32_t modsize = mod[i].end - mod[i].start;
		char *moddata = (char*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
			MM_MIN_KERNEL_PAGE, 1, (modsize + 4095) & ~0xFFF);
		uint32_t j;
		for (j = 0; j < (modsize + 4095) / 0x1000; j++)
		{
			mmMapKernelMemory(mod[i].start + j * 0x1000, (uintptr_t)moddata + j * 0x1000,
				MM_MAP_READ | MM_MAP_WRITE);
		}
		char *modname = (char*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
			MM_MIN_KERNEL_PAGE, 1, 0x1000);
		mmMapKernelMemory(mod[i].string & ~0xFFF, (uintptr_t)modname,
			MM_MAP_READ | MM_MAP_WRITE);
		char *name = strdup(modname + (mod[i].string & 0xFFF));
		mmMapKernelMemory(0, (uintptr_t)modname, 0);
		// Load module
		keLoadModule(moddata, modsize, name);
	}
	return KE_ERROR_NOTIMPLEMENTED;
}

int keResolveAddress(uintptr_t addr, char **name, uintptr_t *offset)
{
	uint32_t i;
	for (i = 0; i < symbolcount; i++)
	{
		
		if ((addr >= symbols[i].addr) && (addr < symbols[i].addr + symbols[i].size))
		{
			if (name) *name = symbols[i].name;
			if (offset) *offset = addr - symbols[i].addr;
			return 0;
		}
	}
	for (i = 0; i < private_symbolcount; i++)
	{
		if ((addr >= private_symbols[i].addr) && (addr < private_symbols[i].addr + private_symbols[i].size))
		{
			if (name) *name = private_symbols[i].name;
			if (offset) *offset = addr - private_symbols[i].addr;
			return 0;
		}
	}
	return KE_ERROR_UNKNOWN;
}
uintptr_t keResolveSymbol(char *name)
{
	uint32_t i;
	for (i = 0; i < symbolcount; i++)
	{
		if (!strcmp(symbols[i].name, name))
		{
			return symbols[i].addr;
		}
	}
	return 0;
}
int keResolveModuleAddress(uintptr_t addr, char **module, char **name, uintptr_t *offset)
{
	uint32_t i;
	for (i = 0; i < modulecount; i++)
	{
		if ((addr >= modules[i].start) && (addr < modules[i].start + modules[i].size))
		{
			if (module)
			{
				*module = modules[i].name;
			}
			if (name)
			{
				*name = "";
			}
			if (offset)
			{
				*offset = addr - modules[i].start;
			}
			return 0;
		}
	}
	return KE_ERROR_UNKNOWN;
}

