/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/spinlock.h"
#include "ke/errors.h"
#include "ke/panic.h"

int keDestroySpinlock(KeSpinlock *spinlock)
{
	if (!spinlock) return KE_ERROR_INVALID_ARG;
	if (keTryLockSpinlock(spinlock)) return KE_ERROR_BUSY;
	return 0;
}
int keInitSpinlock(KeSpinlock *spinlock)
{
	if (!spinlock) return KE_ERROR_INVALID_ARG;
	spinlock->lock = 0;
	return 0;
}
int keLockSpinlock(KeSpinlock *spinlock)
{
	if (!spinlock) return KE_ERROR_INVALID_ARG;
	int tries = 0;
	while (keTryLockSpinlock(spinlock) == KE_ERROR_BUSY)
	{
		tries++;
		if (tries == 5000000) kePanic(0, 3);
	}
	return 0;
}
int keTryLockSpinlock(KeSpinlock *spinlock)
{
	if (!spinlock) return KE_ERROR_INVALID_ARG;
	int value = 1;
	asm volatile("lock xchgl %%eax, (%%edx)" : "=a"(value) : "a"(value), "d"(&spinlock->lock));
	if (value) return KE_ERROR_BUSY;
	return 0;
}
int keUnlockSpinlock(KeSpinlock *spinlock)
{
	if (!spinlock) return KE_ERROR_INVALID_ARG;
	if (!spinlock->lock) return KE_ERROR_UNKNOWN;
	spinlock->lock = 0;
	return 0;
}

