/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/panic.h"
#include "ke/apic.h"
#include "ke/module.h"
#include "mm/memory.h"
#include "sys/terminal.h"
#include <string.h>

static int first_try = 1;

extern uint32_t kernel_directory[1024];

static int width;
static int height;
static char *screen;

static void kePrintString(int x, int y, char *s)
{
	int length = strlen(s);
	if (x + length > width) length = width - x;
	int i;
	for (i = 0; i < length; i++)
	{
		screen[y * width * 2 + (x + i) * 2] = s[i];
	}
}
static char *digits = "0123456789abcdef";
static void kePrintHex(int x, int y, uint32_t number)
{
	int i = 0;
	if (x > width - 8) i = 8 - (width - x);
	for (i = 0; i < 8; i++)
	{
		screen[y * width * 2 + (x + 7 - i) * 2] = digits[(number >> (i * 4)) & 0xF];
	}
}
static int kePrintDec(int x, int y, uint32_t number)
{
	int written = 0;
	if ((number >= 10) && (width - x > 0))
	{
		written = kePrintDec(x, y, number / 10);
		x += written;
	}
	written++;
	screen[y * width * 2 + x * 2] = '0' + (number % 10);
	return written;
}

static int keIsAccessible(uintptr_t addr)
{
	if (0x1000 - (addr & 0xFFF) < sizeof(uintptr_t))
		return !mmKernelPageIsFree(addr & ~0xFFF) && !mmKernelPageIsFree((addr + 0x1000) & ~0xFFF);
	else
		return !mmKernelPageIsFree(addr & ~0xFFF);
}

static void kePrintStackFrameEIP(int y, uintptr_t eip)
{
	kePrintHex(29, y, eip);
	// Print function
	char *function = 0;
	uint32_t offset = 0;
	int status = keResolveAddress(eip, &function, &offset);
	if (!status)
	{
		kePrintString(38, y, function);
		kePrintString(38 + strlen(function) + 1, y, "+");
		kePrintHex(38 + strlen(function) + 3, y, offset);
	}
	else
	{
		char *module = 0;
		status = keResolveModuleAddress(eip, &module, &function, &offset);
		if (!status)
		{
			if (strcmp(function, ""))
			{
				kePrintString(38, y, module);
				kePrintString(38 + strlen(module) + 1, y, "+");
				kePrintHex(38 + strlen(module) + 3, y, offset);
			}
			else
			{
				kePrintString(38, y, module);
				kePrintString(38 + strlen(module) + 1, y, "+");
				kePrintHex(38 + strlen(module) + 3, y, offset);
			}
		}
		else
		{
			kePrintString(38, y, "<unknown>");
		}
	}
}

static uintptr_t kePrintStackFrame(int y, uintptr_t ebp, uintptr_t eip)
{
	if (eip)
	{
		// Add one line with the current function
		kePrintStackFrameEIP(y, eip);
		y++;
	}
	kePrintHex(20, y, ebp);
	if (ebp < 0xC0000000) return 0;
	if (keIsAccessible(ebp))
	{
		uintptr_t nextebp = *((uintptr_t*)ebp);
		// Get calling code
		if (keIsAccessible(ebp + 4))
		{
			eip = *((uintptr_t*)ebp + 1);
			kePrintStackFrameEIP(y, eip);
		}
		return nextebp;
	}
	return 0;
}

void kePanic(IntStackFrame *isf, int error)
{
	if (!first_try)
	{
		// The crash handler crashed.
		asm("cli");
		asm("hlt");
	}
	first_try = 0;
	// Stop other CPUs
	keAPICBroadcast(0x31, 0);
	// Switch to kernel address space
	asm volatile("mov %%eax, %%cr3" :: "a" ((uintptr_t)kernel_directory - MM_KERNEL_BASE));
	// Get screen info
	screen = sysTerminalPreparePanic(&width, &height);
	memset(screen, 0, width * height * 2);
	int i;
	for (i = 0; i < width * height; i++)
	{
		screen[i * 2 + 1] = 0x17;
	}
	
	// Draw info
	kePrintString(1, 1, "Panic: ");
	
	if (error)
	{
		kePrintString(8, 1, "Error ");
		kePrintDec(14, 1, error);
	}
	else
	{
		// Registers
		kePrintString(8, 1, "Exception ");
		kePrintDec(19, 1, isf->intno);
		kePrintString(8, 2, "Error code");
		kePrintHex(19, 2, isf->error);
		if (isf->intno == 14)
		{
			uint32_t cr2;
			asm("mov %%cr2, %0":"=r"(cr2));
			kePrintString(8, 3, "Address");
			kePrintHex(19, 3, cr2);
		}
		kePrintString(1, 5, "cs");
		kePrintHex(8, 5, isf->cs);
		kePrintString(1, 6, "eip");
		kePrintHex(8, 6, isf->eip);
		kePrintString(1, 7, "ss");
		kePrintHex(8, 7, isf->ss);
		kePrintString(1, 8, "esp");
		kePrintHex(8, 8, isf->esp);
		kePrintString(1, 9, "ebp");
		kePrintHex(8, 9, isf->ebp);
		kePrintString(1, 10, "eax");
		kePrintHex(8, 10, isf->eax);
		kePrintString(1, 11, "ebx");
		kePrintHex(8, 11, isf->ebx);
		kePrintString(1, 12, "ecx");
		kePrintHex(8, 12, isf->ecx);
		kePrintString(1, 13, "edx");
		kePrintHex(8, 13, isf->edx);
		kePrintString(1, 14, "esi");
		kePrintHex(8, 14, isf->esi);
		kePrintString(1, 15, "edi");
		kePrintHex(8, 15, isf->edi);
		kePrintString(1, 16, "ds");
		kePrintHex(8, 16, isf->ds);
		kePrintString(1, 17, "es");
		kePrintHex(8, 17, isf->es);
		kePrintString(1, 18, "fs");
		kePrintHex(8, 18, isf->fs);
		kePrintString(1, 19, "gs");
		kePrintHex(8, 19, isf->gs);
		kePrintString(1, 20, "eflags");
		kePrintHex(8, 20, isf->eflags);
	}
	
	// Stack trace
	kePrintString(20, 5, "Stack:");
	uintptr_t ebp = 0;
	if (isf)
	{
		ebp = isf->ebp;
	}
	else
	{
		asm volatile("mov %%ebp, %0" : "=r"(ebp));
	}
	int y = 6;
	if (isf)
	{
		kePrintStackFrame(y, isf->ebp, isf->eip);
		y++;
	}
	while ((ebp = kePrintStackFrame(y, ebp, 0)) != 0)
	{
		y++;
		if (y == height - 1) break;
	}
	
	// Stop current CPU
	asm("cli");
	asm("hlt");
}

