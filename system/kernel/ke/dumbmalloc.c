
#include "mm/memory.h"
#include "ke/debug.h"
#include "ke/level.h"
#include "ke/spinlock.h"
#include <string.h>

void *malloc(size_t size)
{
	uint32_t memsize = size + 4;
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	
	uintptr_t vaddr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, (memsize + 0xFFF) & ~0xFFF);
	if (!vaddr)
	{
		kePrint("Out of kernel space.\n");
		return 0;
	}
	uint32_t i;
	for (i = 0; i < (memsize + 0xFFF) / 0x1000; ++i)
	{
		uintptr_t paddr = mmAllocPhysicalMemory(0, 0, 0x1000);
		if (!paddr)
		{
			kePrint("Out of kernel space.\n");
			return 0;
		}
		mmMapKernelMemory(paddr, vaddr + i * 0x1000, MM_MAP_READ | MM_MAP_WRITE);
	}
	
	keUnlockSpinlock(mmGetMemoryLock());
	keSetExecutionLevel(oldlevel);
	
	*(uint32_t*)vaddr = size;
	
	memset((void*)(vaddr + 4), 0xF0, size);
	
	return (void*)(vaddr + 4);
}

void *calloc(size_t nmemb, size_t size)
{
	void *data = malloc(nmemb * size);
	memset(data, 0, nmemb * size);
	return data;
}

void free(void *ptr)
{
	if (!ptr) return;
	uintptr_t addr = (uintptr_t)ptr - 4;
	uint32_t memsize = *(uint32_t*)addr + 4;
	
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	
	uint32_t i;
	for (i = 0; i < (memsize + 0xFFF) / 0x1000; ++i)
	{
		uintptr_t vaddr = (uintptr_t)addr + i * 0x1000;
		uintptr_t paddr = mmKernelGetPhysAddress(vaddr);
		if (paddr != 0)
			mmFreePhysicalMemory(paddr, 0x1000);
		mmMapKernelMemory(0, vaddr, 0);
	}
	
	keUnlockSpinlock(mmGetMemoryLock());
	keSetExecutionLevel(oldlevel);
}

void *realloc(void *ptr, size_t size)
{
	uint32_t srcsize = 0;
	if (ptr) srcsize = *(uint32_t*)((uintptr_t)ptr - 4);
	if (size > srcsize)
	{
		void *newdata = malloc(size);
		memcpy(newdata, ptr, srcsize);
		free(ptr);
		return newdata;
	}
	else
		return ptr;
}

