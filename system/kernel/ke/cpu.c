/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/cpu.h"
#include "ke/apic.h"
#include "ke/thread.h"
#include <stdlib.h>
#include <string.h>

extern uint32_t kernel_stack[];

int keCPUID(uint32_t code, uint32_t *output)
{
	asm volatile("cpuid" : "=a"(output[0]), "=b"(output[1]), "=c"(output[2]),
		"=d"(output[3]) : "a"(code));
	return 0;
}

void keWriteMSR(uint32_t index, uint64_t value)
{
	uint32_t low = value;
	uint32_t high = value >> 32;
	asm volatile("wrmsr" : : "d"(high), "a"(low), "c"(index));
}
uint64_t keReadMSR(uint32_t index)
{
	uint32_t low;
	uint32_t high;
	asm volatile("rdmsr" : "=d"(high), "=a"(low) : "c"(index));
	return ((uint64_t)high << 32) + low;
}

static KeCPU **cpus = 0;
static uint32_t cpucount = 0;

KeCPU *keAddCPU(uint32_t id)
{
	// Create CPU info
	KeCPU *newcpu = malloc(sizeof(KeCPU));
	memset(newcpu, 0, sizeof(KeCPU));
	newcpu->id = id;
	newcpu->idlethread = keCreateIdleThread();
	newcpu->level = KE_LEVEL_HIGH;
	
	// Add CPU to list
	KeCPU **cpus_new = malloc((cpucount + 1) * sizeof(KeCPU*));
	// We cannot just use realloc here as realloc calls keGetCurrentCPU which
	// would lead to a triple fault.
	memcpy(cpus_new, cpus, cpucount * sizeof(KeCPU*));
	cpus_new[cpucount] = newcpu;
	KeCPU **cpus_old = cpus;
	cpus = cpus_new;
	free(cpus_old);
	cpucount++;
	return newcpu;
}
KeCPU *keGetCurrentCPU(void)
{
	uint32_t id = keAPICGetCPU();
	uint32_t i;
	for (i = 0; i < cpucount; i++)
	{
		if (cpus[i]->id == id) return cpus[i];
	}
	return 0;
}
uint32_t keGetCPUCount(void)
{
	return cpucount;
}

