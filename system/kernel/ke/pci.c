/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/pci.h"
#include "ke/errors.h"
#include "ke/ports.h"
#include "ke/debug.h"
#include <stdlib.h>
#include <string.h>

#define KE_PCI_CONFIG_ADDRESS 0xCF8
#define KE_PCI_CONFIG_DATA 0xCFC

#define KE_PCI_DEVICE_VENDOR 0x0
#define KE_PCI_DEVICE_CLASS 0x8
#define KE_PCI_DEVICE_HEADERTYPE 0x0C
#define KE_PCI_DEVICE_BAR0 0x10
#define KE_PCI_DEVICE_IRQ 0x3C

static KePCIDevice *devices = 0;
static uint32_t devicecount = 0;

static uint32_t kePCIRead(uint8_t bus, uint8_t slot, uint8_t func, uint8_t reg)
{
	uint32_t addr = 0x80000000 | (bus << 16) | ((slot & 0x1F) << 11) | ((func & 0x7) << 8) | reg;
	outl(KE_PCI_CONFIG_ADDRESS, addr);
	return inl(KE_PCI_CONFIG_DATA);
}
static void kePCIWrite(uint8_t bus, uint8_t slot, uint8_t func, uint8_t reg, uint32_t data)
{
	uint32_t addr = 0x80000000 | (bus << 16) | ((slot & 0xF) << 12) | ((func & 0xF) << 8) | reg;
	outl(KE_PCI_CONFIG_ADDRESS, addr);
	outl(KE_PCI_CONFIG_DATA, data);
}

int keInitPCI(void)
{
	devicecount = 0;
	uint32_t bus;
	uint32_t slot;
	uint32_t func;
	// Count devices
	for (bus = 0; bus < 8; bus++)
	{
		for (slot = 0; slot < 32; slot++)
		{
			for (func = 0; func < 8; func++)
			{
				uint32_t device = kePCIRead(bus, slot, func, KE_PCI_DEVICE_VENDOR);
				if (device != 0xFFFFFFFF)
				{
					devicecount++;
				}
			}
		}
	}
	// Allocate memory
	devices = malloc(sizeof(KePCIDevice) * devicecount);
	// Read device info
	uint32_t currentdevice = 0;
	for (bus = 0; bus < 8; bus++)
	{
		for (slot = 0; slot < 32; slot++)
		{
			for (func = 0; func < 8; func++)
			{
				uint32_t device = kePCIRead(bus, slot, func, KE_PCI_DEVICE_VENDOR);
				if (((device >> 16) != 0xFFFF) && ((device >> 16) != 0x0))
				{
					keGetPCIDevice(&devices[currentdevice], bus, slot, func);
					currentdevice++;
				}
			}
		}
	}
	return 0;
}

int keGetPCIDevice(KePCIDevice *device, uint8_t bus, uint8_t slot, uint8_t func)
{
	if (!device)
	{
		return KE_ERROR_UNKNOWN;
	}
	memset(device, 0, sizeof(KePCIDevice));
	// Vendor/device ID
	uint32_t devinfo = kePCIRead(bus, slot, func, KE_PCI_DEVICE_VENDOR);
	if (devinfo == 0xFFFF) return KE_ERROR_UNKNOWN;
	device->device = devinfo >> 16;
	device->vendor = devinfo & 0xFFFF;
	// Class
	uint32_t class = kePCIRead(bus, slot, func, KE_PCI_DEVICE_CLASS);
	device->devclass = class >> 24;
	device->subclass = class >> 16;
	device->revision = class;
	device->irq = kePCIRead(bus, slot, func, KE_PCI_DEVICE_IRQ);
	kePrint("PCI device at %x:%x:%x: %x/%x, class: %x:%x\n", bus, slot, func, device->vendor, device->device, device->devclass, device->subclass);
	// Memory/ports
	uint8_t headertype = kePCIRead(bus, slot, func, KE_PCI_DEVICE_HEADERTYPE) >> 16;
	if (!headertype)
	{
		// Not a bridge
		uint32_t i;
		for (i = 0; i < 6; i++)
		{
			uint32_t addr = kePCIRead(bus, slot, func, KE_PCI_DEVICE_BAR0 + i * 4);
			if (!addr) break;
			device->rescount++;
			device->res[i].addr = addr & ~0xF;
			device->res[i].type = addr & 0x1;
			// Get size
			kePCIWrite(bus, slot, func, KE_PCI_DEVICE_BAR0 + i * 4, 0xFFFFFFFF);
			uint32_t size = kePCIRead(bus, slot, func, KE_PCI_DEVICE_BAR0 + i * 4);
			device->res[i].size = ~(size & ~0xF) + 1;
			kePCIWrite(bus, slot, func, KE_PCI_DEVICE_BAR0 + i * 4, addr);
			kePrint("BAR: %x: %x\n", device->res[i].addr, device->res[i].type);
		}
	}
	
	return 0;
}
int keGetPCIDeviceCount(void)
{
	return devicecount;
}
KePCIDevice *keGetPCIDevices(void)
{
	return devices;
}


