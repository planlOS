/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/debug.h"
#include "ke/ports.h"
#include "ke/level.h"
#include "ke/spinlock.h"
#include <string.h>

#define SERIAL 0x3f8

static char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";
static char digits_cap[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

static char *vidmem = (char*)0xC00B8000;
static int cursorx = 0;
static int cursory = 0;

static KeSpinlock printlock;

static int use_vga = 1;

void keTerminalInitialized(void)
{
	//use_vga = 0;
}

void kePrintInit(void)
{
	outb(SERIAL + 1, 0); // Disable interrupts
	outb(SERIAL + 3, 0x80); // Set baud rate
	outb(SERIAL, 0x3); // High byte, 38400 baud
	outb(SERIAL + 1, 0); // Low byte
	outb(SERIAL + 3, 0x3); // 8 bits, no parity bit, one stop bit
	outb(SERIAL + 2, 0xC7); // Enable FIFO
	outb(SERIAL + 4, 0x0B); // Enable interrupts
	keInitSpinlock(&printlock);
}

static void keScroll(int lines)
{
	memmove(vidmem, vidmem + lines * 160, (25 - lines) * 160);
	memset(vidmem + (25 - lines) * 160, 0, lines * 160);
	cursory -= lines;
}

static void kePrintChar(char c)
{
	// Serial output
	while ((inb(SERIAL + 5) & 0x20) == 0);
	outb(SERIAL, c);
	
	if (use_vga)
	{
		// VGA output
		switch (c)
		{
			case '\n':
				cursorx = 0;
				cursory++;
				if (cursory == 25) keScroll(1);
				break;
			case '\r':
				cursorx = 0;
				break;
			case '\t':
				kePrintChar(' ');
				while (cursorx & 0x7) kePrintChar(' ');
				break;
			default:
				vidmem[cursory * 160 + cursorx * 2] = c;
				vidmem[cursory * 160 + cursorx * 2 + 1] = 0x07;
				cursorx++;
				if (cursorx == 80)
				{
					cursorx = 0;
					cursory++;
					if (cursory == 25) keScroll(1);
				}
				break;
		}
	}
	else
	{
		// TODO
	}
}
static void kePrintString(const char *s)
{
	while (*s)
	{
		kePrintChar(*s);
		s++;
	}
}

static void kePrintInt(int32_t value, uint32_t base, int capital_letters)
{
	if (value < 0) {
		kePrintChar('-');
		value = -value;
	}
	if (value >= (int32_t)base) {
		kePrintInt(value / base, base, capital_letters);
	}
	int digit = value % base;
	if (capital_letters) {
		kePrintChar(digits_cap[digit]);
	} else {
		kePrintChar(digits[digit]);
	}
}
static void kePrintUInt(uint32_t value, uint32_t base, int capital_letters)
{
	if (value >= base) {
		kePrintInt(value / base, base, capital_letters);
	}
	int digit = value % base;
	if (capital_letters) {
		kePrintChar(digits_cap[digit]);
	} else {
		kePrintChar(digits[digit]);
	}
}

void kePrint(const char *fmt, ...)
{
	int *args = ((int*)&fmt) + 1;
	kePrintList(fmt, &args);
}
void kePrintList(const char *fmt, int **args)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&printlock);
	while (*fmt) {
		switch (*fmt)
		{
			case '%':
				switch(*(fmt+1))
				{
					case '%':
						kePrintChar('%');
						break;
					case 'c':
						kePrintChar(*((char*)(*args)));
						(*args)++;
						break;
					case 'd':
					case 'i':
						kePrintInt(*((int32_t*)(*args)), 10, 0);
						(*args)++;
						break;
					case 'u':
						kePrintUInt(*((uint32_t*)(*args)), 10, 0);
						(*args)++;
						break;
					case 'x':
						kePrintUInt(*((uint32_t*)(*args)), 16, 0);
						(*args)++;
						break;
					case 'X':
						kePrintUInt(*((uint32_t*)(*args)), 16, 1);
						(*args)++;
						break;
					case 's':
						kePrintString(*((char**)(*args)));
						(*args)++;
						break;
				}
				fmt++;
				break;
			case '\n':
				kePrintChar('\n');
				break;
			case '\t':
				kePrintChar('\t');
				break;
			default:
				kePrintChar(*fmt);
				break;
		}
		fmt++;
	}
	keUnlockSpinlock(&printlock);
	keSetExecutionLevel(oldlevel);
}

