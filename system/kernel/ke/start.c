/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/debug.h"
#include "ke/gdt.h"
#include "ke/apic.h"
#include "ke/multiboot.h"
#include "ke/interrupts.h"
#include "ke/smp.h"
#include "ke/timer.h"
#include "ke/pci.h"
#include "ke/process.h"
#include "mm/memory.h"
#include "ke/module.h"
#include "fs/fs.h"
#include "fs/devfs.h"
#include "ke/elf.h"
#include "sys/pipe.h"
#include "sys/terminal.h"
#include "sys/serial.h"
#include "net/net.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

static MultibootInfo *mbinfo;

void kernel_phys_start();
void kernel_phys_end();

static void keInit2(void)
{
	keInitKernelSymbols(mbinfo);
	keInitPCI();
	fsInit();
	fsInitDevFS();
	sysInitPipe();
	netInit();
	FsFileSystemDriver *devfs = fsGetDriver("devfs");
	if (!devfs)
	{
		// TODO: Panic
		kePrint("devfs not available.\n");
		while (1);
	}
	fsMount(devfs, "/dev/", 0, 0);
	sysInitSerial();
	sysInitTerminals();
	keInitModules(mbinfo);
	
	// Map CDROM
	FsFileSystemDriver *iso9660 = fsGetDriver("iso9660");
	if (!iso9660)
	{
		// TODO: Panic
		kePrint("iso9660 not available.\n");
		while (1);
	}
	fsMount(iso9660, "/", "/dev/cdrom0", 0);
	FsFileHandle *testfile = fsOpen("/boot/grub/menu.lst", FS_OPEN_READ);
	if (!testfile)
	{
		kePrint("Could not open test file.\n");
	}
	else
	{
		char *test = malloc(20);
		int read = fsRead(testfile, test, 19, 1);
		kePrint("Read %d bytes.\n", read);
		test[read] = 0;
		kePrint("Read: \"%s\"\n", test);
		fsClose(testfile);
		free(test);
	}
	
	// Map /tmp
	FsFileSystemDriver *ramdisk = fsGetDriver("ramdisk");
	if (!ramdisk)
	{
		// TODO: Panic
		kePrint("ramdisk not available.\n");
		while (1);
	}
	fsMount(ramdisk, "/tmp/", 0, 0);
	
	kePrint("Done.\n");
	
	// Load initial process
	FsFileHandle *initfile = fsOpen("/bin/shell", FS_OPEN_READ);
	if (!initfile)
	{
		kePrint("Could not open initial program.\n");
		while (1);
	}
	int size = fsSeek(initfile, 0, 2);
	kePrint("File size: %d bytes.\n", size);
	fsSeek(initfile, 0, 0);
	char *program = malloc(size);
	if (fsRead(initfile, program, size, 1) != size)
	{
		kePrint("Could not read whole program.\n");
		fsClose(initfile);
		while (1);
	}
	
	// Start initial processes
	int i;
	char terminal[11];
	for (i = 0; i < 8; i++)
	{
		snprintf(terminal, 11, "/dev/tty%d", i);
		KeProcess *process = keCreateProcess();
		FsFileHandle *stdin = fsProcessOpen(process, terminal, FS_OPEN_READ);
		process->fd[0] = stdin;
		FsFileHandle *stdout = fsProcessOpen(process, terminal, FS_OPEN_WRITE);
		process->fd[1] = stdout;
		FsFileHandle *stderr = fsProcessOpen(process, terminal, FS_OPEN_WRITE);
		process->fd[2] = stderr;
		keElfMapProgram(&process->memory, program, size);
		uintptr_t entry = ((ElfHeader*)program)->e_entry;
		keCreateThread(process, entry, 4, 0, 0, 0, 0);
	}
	
	free(program);
	fsClose(initfile);
	
	keExitThread();
}

void keInit(int magic, MultibootInfo *info)
{
	char *vidmem = (char*)0xC00B8000;
	memset(vidmem, 0, 160 * 25);
	
	keInstallStartupGDT();
	kePrintInit();
	keInitPIC();
	if (!CHECK_FLAG(info->flags, 5))
	{
		kePrint("Error: no symbols available.\n");
		while (1);
	}
	
	uintptr_t kernel_begin = ((uintptr_t)kernel_phys_start & ~0xFFF) + 0xC0000000;
	uintptr_t kernel_end = (((uintptr_t)kernel_phys_end + 4095) & ~0xFFF) + 0xC0000000;
	mmInitMemoryManager(info, kernel_begin, kernel_end);
	// Remap multiboot struct
	uintptr_t info_addr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	mmMapKernelMemory((uintptr_t)info & ~0xFFF, info_addr,
		MM_MAP_READ | MM_MAP_WRITE);
	info_addr += (uintptr_t)info & 0xFFF;
	info = (void*)info_addr;
	mbinfo = info;
	
	keAPICInit();
	keInitSMP();
	keInstallGDT();
	keInitTimer(100);
	keInitProcessManager();
	
	// Initial kernel thread
	keCreateKernelThread((uintptr_t)keInit2, 0);
	
	// Start kernel
	kePrint("Installing timer.\n");
	keInstallTimer();
	kePrint("Starting APs.\n");
	keStartAPs();
	kePrint("Enabling interrupts.\n");
	asm("sti");
	
	while (1) asm("hlt");
}

