/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/interrupts.h"
#include "ke/debug.h"
#include "ke/ports.h"
#include "ke/gdt.h"
#include "ke/apic.h"
#include "ke/module.h"
#include "ke/panic.h"
#include "sys/syscall.h"
#include "mm/memory.h"

#include <string.h>

static IDTEntry idt[256];
extern uint32_t kernel_directory[1024];

static void keSetIDTEntry(int intno, void (*handler)(void), int dpl)
{
	IDTEntry entry;
	entry.zero = 0;
	entry.offset_low = (uint32_t)handler & 0x0000FFFF;
	entry.offset_high = (uint32_t)handler >> 16;
	entry.selector = 0x8;
	entry.type = 0x8e + ((dpl & 0x3) << 5);
	idt[intno] = entry;
}

static void keStopCPU(KeCPU *cpu, uint8_t intno)
{
	asm("hlt");
}
static void keDummy(KeCPU *cpu, uint8_t intno);
static void keStopThread(KeCPU *cpu, uint8_t intno)
{
	if (!cpu->currentthread) return;
	keDestroyThread(cpu->currentthread);
	keDummy(cpu, intno);
}
static void keDummy(KeCPU *cpu, uint8_t intno)
{
	KeThread *newthread = keGetSchedulableThread(0);
	if (newthread)
	{
		cpu->currentthread = newthread;
	}
	else
	{
		keLockSpinlock(&cpu->idlethread->active);
		cpu->currentthread = cpu->idlethread;
	}
}
static void keReloadPagedir(KeCPU *cpu, uint8_t intno)
{
	kePrint("[1]");
	if (cpu->currentthread)
	{
		if (!cpu->currentthread->process)
		{
			asm volatile("mov %%eax, %%cr3" :: "a" ((uintptr_t)kernel_directory - MM_KERNEL_BASE));
		}
	}
	kePrint("[2]");
}

void keInitPIC(void)
{
	memset(idt, 0, sizeof(IDTEntry) * 256);
	
	// Remap PIC
	outb(0x20, 0x11);
	outb(0xa0, 0x11);
	outb(0x21, 0x20);
	outb(0xa1, 0x28);
	outb(0x21, 0x04);
	outb(0xa1, 0x02);
	outb(0x21, 0x01);
	outb(0xa1, 0x01);
	outb(0x21, 0xfb);
	outb(0xa1, 0xff);
	
	// Fill IDT
	keSetIDTEntry(0x00, keInt00, 0);
	keSetIDTEntry(0x01, keInt01, 0);
	keSetIDTEntry(0x02, keInt02, 0);
	keSetIDTEntry(0x03, keInt03, 0);
	keSetIDTEntry(0x04, keInt04, 0);
	keSetIDTEntry(0x05, keInt05, 0);
	keSetIDTEntry(0x06, keInt06, 0);
	keSetIDTEntry(0x07, keInt07, 0);
	keSetIDTEntry(0x08, keInt08, 0);
	keSetIDTEntry(0x09, keInt09, 0);
	keSetIDTEntry(0x0a, keInt0a, 0);
	keSetIDTEntry(0x0b, keInt0b, 0);
	keSetIDTEntry(0x0c, keInt0c, 0);
	keSetIDTEntry(0x0d, keInt0d, 0);
	keSetIDTEntry(0x0e, keInt0e, 0);
	keSetIDTEntry(0x0f, keInt0f, 0);
	keSetIDTEntry(0x10, keInt10, 0);
	keSetIDTEntry(0x11, keInt11, 0);
	keSetIDTEntry(0x12, keInt12, 0);
	keSetIDTEntry(0x13, keInt13, 0);
	keSetIDTEntry(0x14, keInt14, 0);
	keSetIDTEntry(0x15, keInt15, 0);
	keSetIDTEntry(0x16, keInt16, 0);
	keSetIDTEntry(0x17, keInt17, 0);
	keSetIDTEntry(0x18, keInt18, 0);
	keSetIDTEntry(0x19, keInt19, 0);
	keSetIDTEntry(0x1a, keInt1a, 0);
	keSetIDTEntry(0x1b, keInt1b, 0);
	keSetIDTEntry(0x1c, keInt1c, 0);
	keSetIDTEntry(0x1d, keInt1d, 0);
	keSetIDTEntry(0x1e, keInt1e, 0);
	keSetIDTEntry(0x1f, keInt1f, 0);

	keSetIDTEntry(0x20, keInt20, 0);
	keSetIDTEntry(0x21, keInt21, 0);
	keSetIDTEntry(0x22, keInt22, 0);
	keSetIDTEntry(0x23, keInt23, 0);
	keSetIDTEntry(0x24, keInt24, 0);
	keSetIDTEntry(0x25, keInt25, 0);
	keSetIDTEntry(0x26, keInt26, 0);
	keSetIDTEntry(0x27, keInt27, 0);
	keSetIDTEntry(0x28, keInt28, 0);
	keSetIDTEntry(0x29, keInt29, 0);
	keSetIDTEntry(0x2a, keInt2a, 0);
	keSetIDTEntry(0x2b, keInt2b, 0);
	keSetIDTEntry(0x2c, keInt2c, 0);
	keSetIDTEntry(0x2d, keInt2d, 0);
	keSetIDTEntry(0x2e, keInt2e, 0);
	keSetIDTEntry(0x2f, keInt2f, 0);

	keSetIDTEntry(0x30, keInt30, 0);
	keSetIDTEntry(0x31, keInt31, 0);
	keSetIDTEntry(0x32, keInt32, 0);
	keSetIDTEntry(0x33, keInt33, 0);
	keSetIDTEntry(0x34, keInt34, 0);
	keSetIDTEntry(0x35, keInt35, 0);
	keSetIDTEntry(0x36, keInt36, 0);
	keSetIDTEntry(0x37, keInt37, 0);

	keSetIDTEntry(0x80, keInt80, 3);
	
	// Enable IDT
	IDTDescriptor desc;
	desc.size = 256 * sizeof(IDTEntry) - 1;
	desc.base = (uint32_t)idt;
	
	asm("lidt %0"::"m"(desc));
	
	// Install standard handlers
	// 0x31 = Crash-IPI
	keRegisterIRQ(0x11, keStopCPU);
	// 0x32 = Dummy interrupt (used to schedule out of a thread)
	keRegisterIRQ(0x12, keDummy);
	// 0x33 = Stop current thread
	keRegisterIRQ(0x13, keStopThread);
	// 0x34 = Reload page directory
	keRegisterIRQ(0x14, keReloadPagedir);
}

void keInitPIC2(void)
{
	// Enable IDT
	IDTDescriptor desc;
	desc.size = 256 * sizeof(IDTEntry) - 1;
	desc.base = (uint32_t)idt;
	
	asm("lidt %0"::"m"(desc));
}

void keExceptionHandler(IntStackFrame *isf)
{
	//kePrint("\nStack frame: %x\n", isf);
	/*if (isf->cs == 0x8)
	{*/
		/*// TODO: Don't kill kernel on non-critical exceptions
		// Print register info
		if (isf->intno == 14) {
			uint32_t cr2;
			asm("mov %%cr2, %0":"=r"(cr2));
			kePrint("\nPanic:\nCPU %d\nPage fault at %x, error code: %x\n\n", keAPICGetCPU(), cr2, isf->error);
			char *function = 0;
			uint32_t offset = 0;
			int status = keResolveAddress(isf->eip, &function, &offset);
			if (!status)
			{
				kePrint("in function %s+%x\n", function, offset);
			}
			else
			{
				kePrint("Unknown function.\n");
			}
		} else {
			kePrint("\n\nPanic:\nException %d, error code: %x\n\n", isf->intno, isf->error);
		}
		kePrint("eax: %x\tebx: %x\tecx: %x\tedx: %x\n", isf->eax, isf->ebx, isf->ecx, isf->edx);
		kePrint("ds: %x\tes: %x\tfs: %x\tgs: %x\n", isf->ds, isf->es, isf->fs, isf->gs);
		kePrint("ss: %x\tesp: %x\tcs: %x\teip: %x\n", isf->ss, isf->esp, isf->cs, isf->eip);
		kePrint("eflags: %x\n", isf->eflags);
		asm("cli");
		asm("hlt");
		while(1);*/
		kePanic(isf, 0);
	/*}
	else
	{
		// Print register info
		if (isf->intno == 14) {
			uint32_t cr2;
			asm("mov %%cr2, %0":"=r"(cr2));
			if (current_thread && (cr2 < current_thread->userstack + 0x400000) && (cr2 > current_thread->userstack))
			{
				// Just grow stack
				uint32_t *page_dir = (uint32_t*)halFindContinuousPages(kernel_directory, 1, 0xE0000000, 0xFFFFFFFF);
				halMapPage(kernel_directory, (uint32_t)page_dir, ((X86Task*)current_thread->thread.task)->pagedir, 0x3);
				uintptr_t userstack_phys = halAllocPhysPage();
				halMapPage(page_dir, cr2 & ~0xFFF, userstack_phys, 0x7);
				halMapPage(kernel_directory, (uint32_t)page_dir, 0, 0x0);
				return;
			}
			kePrint("\nProcess killed:\nPage fault at %x, error code: %x\n\n", cr2, isf->error);
			if (current_thread && current_thread->thread.task)
			{
				kePrint("Task: %d, thread: %d\n\n", current_thread->thread.task->id, current_thread->thread.id);
			}
		} else {
			kePrint("\n\nProcess killed:\nException %d, error code: %x\n\n", isf->intno, isf->error);
		}
		kePrint("eax: %x\tebx: %x\tecx: %x\tedx: %x\n", isf->eax, isf->ebx, isf->ecx, isf->edx);
		kePrint("ds: %x\tes: %x\tfs: %x\tgs: %x\n", isf->ds, isf->es, isf->fs, isf->gs);
		kePrint("ss: %x\tesp: %x\tcs: %x\teip: %x\n", isf->ss, isf->esp, isf->cs, isf->eip);
		kePrint("eflags: %x\n", isf->eflags);
		// Kill task
		Task *task = current_thread->thread.task;
		while (task->threadcount > 0)
		{
			Thread *thread0 = task->threads[0];
			sysRemoveThread(thread0);
			halDestroyThread(thread0);
		}
		halDestroyTask(task);
	}*/	
}

/*inline void keSyscall(void)
{
	IntStackFrame *isf = (IntStackFrame*)current_thread->kernelstack;
	sysSyscall(&current_thread->thread, isf->eax, &isf->ebx, &isf->ecx, &isf->edx, &isf->esi, &isf->edi);
}*/

void (*irq_handler[128])(KeCPU*, uint8_t);

void *keIntHandler(IntStackFrame *isf)
{
	KeCPU *cpu = keGetCurrentCPU();
	KeThread *oldthread = 0;
	if (cpu)
	{
		cpu->level = KE_LEVEL_HIGH;
		cpu->interrupt = 1;
		oldthread = cpu->currentthread;
	}
	if (oldthread)
	{
		oldthread->kernelstack = (uintptr_t)isf;
		if (isf->cs == 0x1b)
		{
			oldthread->userframe = oldthread->kernelstack;
		}
	}
		
	if (isf->intno < 0x20) {
		// Exception
		keExceptionHandler(isf);
	/*} else if (isf->intno >= 0x80) {
		if (isf->intno == 0x80) {
			if (cpu && (isf->cs != 0x8)) {
				sysSyscall(cpu->currentthread, isf->eax, &isf->ebx, &isf->ecx, &isf->edx, &isf->esi, &isf->edi);
			} else {
				// TODO: Panic
			}
		}*/
	} else {
		// Call handler
		if (isf->intno == 0x80)
		{
			if (cpu && (isf->cs != 0x8))
			{
				sysSyscall(cpu->currentthread, isf->eax, &isf->ebx, &isf->ecx, &isf->edx, &isf->esi, &isf->edi);
			}
			else
			{
				// TODO: Panic
			}
		}
		else if (irq_handler[isf->intno - 0x20])
		{
			irq_handler[isf->intno - 0x20](cpu, isf->intno - 0x20);
		}
		else
		{
			if (isf->intno != 0x27)
				kePrint("Interrupt (%d) without handler (cpu: %d)!\n", isf->intno - 0x20, keAPICGetCPU());
		}
		
		if (isf->intno < 0x30) {
			// Send EOI
			if (isf->intno >= 0x28) {
				outb(0xA0, 0x20);
			}
			outb(0x20, 0x20);
		}
		if (oldthread)
		{
			// Kernel stack variable might have been overwritten by now
			oldthread->kernelstack = (uintptr_t)isf;
		}
		if (cpu && cpu->currentthread)
		{
			if (cpu->currentthread->process)
			{
				uintptr_t pagedir = cpu->currentthread->process->memory.phys_addr;
				asm volatile("mov %%eax, %%cr3\n" :: "a" (pagedir));
			}
			//kePrint("CPU %d\n", cpu->id);
			isf = (void*)cpu->currentthread->kernelstack;
			if (isf->cs == 0x8)
				tss[keAPICGetCPU()].esp0 = cpu->currentthread->kernelstack + 17 * 4;
			else
				tss[keAPICGetCPU()].esp0 = cpu->currentthread->kernelstack + 19 * 4;
			if (cpu)
			{
				cpu->level = KE_LEVEL_NORMAL;
				cpu->interrupt = 0;
				if (oldthread && (oldthread != cpu->currentthread))
				{
					keUnlockSpinlock(&oldthread->active);
				}
			}
			return (void*)cpu->currentthread->kernelstack;
		}
	}
	if (cpu)
	{
		cpu->level = KE_LEVEL_NORMAL;
		cpu->interrupt = 0;
		if (oldthread && (oldthread != cpu->currentthread))
		{
			keUnlockSpinlock(&oldthread->active);
		}
	}
	return isf;
}

void keRegisterIRQ(uint32_t irqno, void (*handler)(KeCPU*, uint8_t))
{
	irq_handler[irqno] = handler;
	if (irqno >= 0x10) return;
	if (handler)
	{
		if (irqno < 8)
		{
			outb(0x21, inb(0x21) & ~(0x1 << irqno));
		}
		else
		{
			outb(0xa1, inb(0xa1) & ~(0x1 << (irqno - 8)));
		}
	}
	else
	{
		if (irqno < 8)
		{
			outb(0x21, inb(0x21) | (0x1 << irqno));
		}
		else
		{
			outb(0xa1, inb(0xa1) | (0x1 << irqno));
		}
	}
}

