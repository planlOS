/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/terminal.h"
#include "ke/debug.h"
#include "ke/level.h"
#include "fs/request.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

static char *termnames[12] =
{
	"tty0",
	"tty1",
	"tty2",
	"tty3",
	"tty4",
	"tty5",
	"tty6",
	"tty7",
	"gfx0",
	"gfx1",
	"gfx2",
	"gfx3",
};
static SysTextTerminal textterms[8];
static SysGfxTerminal gfxterms[4];

static int currentterm = 0;

static int screenwidth = 80;
static int screenheight = 25;

static char *vidmem = (char*)0xC00B8000;

static KeSpinlock terminallock;

void keTerminalInitialized(void);

static int control_pressed = 0;

static void sysTerminalScroll(SysTextTerminal *terminal, int lines, int screen)
{
	if (screen)
	{
		memmove(vidmem, vidmem + lines * 160, (25 - lines) * 160);
		memset(vidmem + (25 - lines) * 160, 0, lines * 160);
	}
	memmove(terminal->screendata, terminal->screendata + lines * 160, (25 - lines) * 160);
	memset(terminal->screendata + (25 - lines) * 160, 0, lines * 160);
	terminal->y -= lines;
}

static void sysTerminalWriteChar(SysTextTerminal *terminal, char c, int screen)
{
	if (terminal->x >= 80)
	{
		terminal->x = 0;
		terminal->y++;
		if (terminal->y == 25)
			sysTerminalScroll(terminal, 1, screen);
	}
	int position = (screenwidth * terminal->y + terminal->x) * 2;
	switch (c)
	{
		case '\n':
			terminal->y++;
			terminal->x = 0;
			if (terminal->y == 25)
				sysTerminalScroll(terminal, 1, screen);
			break;
		case '\r':
			terminal->x = 0;
			break;
		case '\t':
			sysTerminalWriteChar(terminal, ' ', screen);
			while (terminal->x % 7)
				sysTerminalWriteChar(terminal, ' ', screen);
			break;
		default:
			terminal->screendata[position] = c;
			terminal->screendata[position + 1] = (terminal->brightfg << 3) + (terminal->bgcolor << 4) + terminal->fgcolor;
			if (screen)
			{
				vidmem[position] = c;
				vidmem[position + 1] = (terminal->brightfg << 3) + (terminal->bgcolor << 4) + terminal->fgcolor;
			}
			terminal->x++;
			break;
	}
}

static int sysTerminalWriteEscape(SysTextTerminal *terminal, char *data, int size, int screen)
{
	// Escape character
	int written = 1;
	data++;
	size--;
	if (!size) return written;
	// [
	switch (*data)
	{
		case '[':
			size--;
			data++;
			written++;
			if (!size) return written;
			break;
		default:
			return written;
	}
	// Parameters
	int paramcount = 0;
	int parameters[10] = {0};
	while (isdigit(*data))
	{
		parameters[paramcount] = *data - '0';
		data++;
		size--;
		written++;
		if (!size) return written;
		if (!size) return written;
		// Read in number
		while (isdigit(*data))
		{
			parameters[paramcount] = parameters[paramcount] * 10 + *data - '0';
			data++;
			size--;
			written++;
			if (!size) return written;
		}
		paramcount++;
		if (paramcount == 10) return written;
		if (*data == ';')
		{
			data++;
			size--;
			written++;
			if (!size) return written;
			continue;
		}
	}
	// Command
	int i;
	switch (*data)
	{
		case 'J':
			if ((paramcount >= 1) && (parameters[0] == 2))
			{
				// Clear screen
				memset(terminal->screendata, 0, screenwidth * screenheight * 2);
				if (screen)
				{
					memset(vidmem, 0, screenwidth * screenheight * 2);
				}
			}
			break;
		case 'm':
			// Set parameters
			for (i = 0; i < 10; i++)
			{
				if (i == paramcount) break;
				switch (parameters[i])
				{
					case 1: // Bright
						terminal->brightfg = 1;
						break;
					case 2: // Dim
						terminal->brightfg = 0;
						break;
					case 30: // Black
						terminal->fgcolor = 0;
						break;
					case 31: // Red
						terminal->fgcolor = 4;
						break;
					case 32: // Green
						terminal->fgcolor = 2;
						break;
					case 33: // Yellow
						terminal->fgcolor = 6;
						break;
					case 34: // Blue
						terminal->fgcolor = 1;
						break;
					case 35: // Magenta
						terminal->fgcolor = 5;
						break;
					case 36: // Cyan
						terminal->fgcolor = 3;
						break;
					case 37: // White
						terminal->fgcolor = 7;
						break;
					case 40: // Black
						terminal->bgcolor = 0;
						break;
					case 41: // Red
						terminal->bgcolor = 4;
						break;
					case 42: // Green
						terminal->bgcolor = 2;
						break;
					case 43: // Yellow
						terminal->bgcolor = 6;
						break;
					case 44: // Blue
						terminal->bgcolor = 1;
						break;
					case 45: // Magenta
						terminal->bgcolor = 5;
						break;
					case 46: // Cyan
						terminal->bgcolor = 3;
						break;
					case 47: // White
						terminal->bgcolor = 7;
						break;
				}
			}
			break;
		case 'H':
			// Set cursor position
			if (paramcount == 2)
			{
				terminal->x = parameters[1] - 1;
				if (terminal->x < 0) terminal->x = 0;
				if (terminal->x >= screenwidth) terminal->x = screenwidth - 1;
				terminal->y = parameters[0] - 1;
				if (terminal->y < 0) terminal->y = 0;
				if (terminal->y >= screenheight) terminal->y = screenheight - 1;
			}
			break;
	}
	return written + 1;
}

static void sysTerminalWrite(SysTextTerminal *terminal, char *data, int size, int screen)
{
	int i;
	for (i = 0; i < size; i++)
	{
		if (data[i] == 033)
		{
			i += sysTerminalWriteEscape(terminal, data + i, size - i, screen);
			i--;
		}
		else
		{
			sysTerminalWriteChar(terminal, data[i], screen);
		}
	}
}

static int sysTerminalRequest(struct FsDeviceFile *file, FsRequest *request)
{
	SysTextTerminal *terminal = (SysTextTerminal*)file;
	int index = ((uintptr_t)terminal - (uintptr_t)textterms) / sizeof(SysTextTerminal);
	
	switch (request->type)
	{
		case FS_REQUEST_OPEN:
			{
				KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
				keLockSpinlock(&terminallock);
				if (!terminal->owner && !terminal->opencount)
				{
					terminal->owner = request->process;
				}
				if (terminal->owner == request->process)
				{
					terminal->opencount++;
				}
				keUnlockSpinlock(&terminallock);
				keSetExecutionLevel(oldlevel);
				fsFinishRequest(request);
			}
			break;
		case FS_REQUEST_CLOSE:
			{
				KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
				keLockSpinlock(&terminallock);
				if (terminal->owner == request->process)
				{
					terminal->opencount--;
					if (!terminal->opencount) terminal->owner = 0;
				}
				keUnlockSpinlock(&terminallock);
				keSetExecutionLevel(oldlevel);
				fsFinishRequest(request);
			}
			break;
		case FS_REQUEST_READ:
			{
				KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
				keLockSpinlock(&terminallock);
				if (!terminal->requestcount && (terminal->inputbuffer_pos >= (int)request->bufferlength))
				{
					// Directly copy from the buffer
					memcpy(request->buffer, terminal->inputbuffer, request->bufferlength);
					memmove(terminal->inputbuffer, terminal->inputbuffer + request->bufferlength,
						terminal->inputbuffer_pos - request->bufferlength);
					terminal->inputbuffer_pos -= request->bufferlength;
					request->return_value = request->bufferlength;
					fsFinishRequest(request);
				}
				else
				{
					// Put the request into the queue
					terminal->requests = realloc(terminal->requests, sizeof(FsRequest*) * (terminal->requestcount + 1));
					terminal->requests[terminal->requestcount] = request;
					terminal->requestcount++;
				}
				keUnlockSpinlock(&terminallock);
				keSetExecutionLevel(oldlevel);
			}
			break;
		case FS_REQUEST_WRITE:
			{
				KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
				keLockSpinlock(&terminallock);
				sysTerminalWrite(terminal, request->buffer, request->bufferlength,
					index == currentterm);
				keUnlockSpinlock(&terminallock);
				keSetExecutionLevel(oldlevel);
				request->return_value = request->bufferlength;
				fsFinishRequest(request);
			}
			break;
		case FS_REQUEST_IOCTLSIZE:
			if (request->offset == 0x10)
			{
				request->return_value = 8;
				fsFinishRequest(request);
				return 0;
			}
			return -1;
		case FS_REQUEST_IOCTL:
			if (request->offset == 0x10)
			{
				uint16_t *data = request->buffer;
				if (currentterm < 8)
				{
					data[0] = 25;
					data[1] = 80;
					data[2] = 0;
					data[3] = 0;
				}
				else
				{
					memset(data, 0, 8);
				}
				request->return_value = 0;
				fsFinishRequest(request);
				return 0;
			}
			request->return_value = -1;
			fsFinishRequest(request);
			break;
		default:
			fsFinishRequest(request);
	}
	return 0;
}
static int sysGfxRequest(struct FsDeviceFile *file, FsRequest *request)
{
	switch (request->type)
	{
		case FS_REQUEST_READ:
			fsFinishRequest(request);
			break;
		case FS_REQUEST_WRITE:
			fsFinishRequest(request);
			break;
		case FS_REQUEST_IOCTL:
			fsFinishRequest(request);
			break;
		default:
			fsFinishRequest(request);
	}
	return 0;
}

int sysInitTerminals(void)
{
	// Create text terminals
	memset(textterms, 0, sizeof(SysTextTerminal) * 8);
	int i;
	for (i = 0; i < 8; i++)
	{
		textterms[i].file.path = termnames[i];
		textterms[i].file.query_request = sysTerminalRequest;
		textterms[i].inputbuffer_size = 256;
		textterms[i].inputbuffer = malloc(256);
		textterms[i].screendata = malloc(screenwidth * screenheight * 2);
		memset(textterms[i].screendata, 0, screenwidth * screenheight * 2);
		textterms[i].fgcolor = 0x7;
		textterms[i].bgcolor = 0x0;
		textterms[i].echo = 1;
		fsCreateDeviceFile(&textterms[i].file);
	}
	// Create graphical terminals
	memset(gfxterms, 0, sizeof(SysGfxTerminal) * 4);
	for (i = 0; i < 4; i++)
	{
		gfxterms[i].file.path = termnames[i + 8];
		gfxterms[i].file.query_request = sysGfxRequest;
		fsCreateDeviceFile(&gfxterms[i].file);
	}
	// Clear screen
	keTerminalInitialized();
	memset(vidmem, 0, 80 * 25 * 2);
	return 0;
}

void sysSetCurrentTerminal(int terminal)
{
	keLockSpinlock(&terminallock);
	if (terminal != currentterm)
	{
		if (terminal < 8)
		{
			memcpy(vidmem, textterms[terminal].screendata, screenwidth * screenheight * 2);
		}
		else
		{
			memset(vidmem, 0, screenwidth * screenheight * 2);
		}
		currentterm = terminal;
	}
	keUnlockSpinlock(&terminallock);
}
int sysGetCurrentTerminal(void)
{
	return currentterm;
}

int sysTerminalInjectKey(int key, int modifiers, int down)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&terminallock);
	if (currentterm < 8)
	{
		SysTextTerminal *terminal = &textterms[currentterm];
		// SIGINT
		if (control_pressed && (key == 'c') && down)
		{
			if (terminal->owner)
			{
				keSendSignal(terminal->owner, 0, SIGINT);
				keUnlockSpinlock(&terminallock);
				keSetExecutionLevel(oldlevel);
				return 0;
			}
		}
		if ((key < 256) && down)
		{
			if (terminal->echo)
			{
				// Draw char
				sysTerminalWriteChar(terminal, key, 1);
			}
			// Add key to buffer
			if (terminal->inputbuffer_pos < terminal->inputbuffer_size)
			{
				terminal->inputbuffer[terminal->inputbuffer_pos] = key;
				terminal->inputbuffer_pos++;
			}
			while (terminal->requestcount && (terminal->inputbuffer_pos >= (int)terminal->requests[0]->bufferlength))
			{
				// Answer request
				FsRequest *request = terminal->requests[0];
				memcpy(request->buffer, terminal->inputbuffer, request->bufferlength);
				memmove(terminal->inputbuffer, terminal->inputbuffer + request->bufferlength,
					terminal->inputbuffer_pos - request->bufferlength);
				terminal->inputbuffer_pos -= request->bufferlength;
				request->return_value = request->bufferlength;
				fsFinishRequest(request);
				// Delete request from queue
				memmove(terminal->requests, terminal->requests + 1, (terminal->requestcount - 1) * sizeof(FsRequest*));
				terminal->requestcount--;
				terminal->requests = realloc(terminal->requests, sizeof(FsRequest*) * terminal->requestcount);
			}
		}
		else
		{
			if (key == SYS_KEY_LCONTROL)
			{
				control_pressed = down;
			}
			// Changing terminals
			if (control_pressed)
			{
				if ((key >= SYS_KEY_F1) && (key <= SYS_KEY_F12))
				{
					keUnlockSpinlock(&terminallock);
					sysSetCurrentTerminal(key - SYS_KEY_F1);
					keLockSpinlock(&terminallock);
				}
			}
		}
	}
	else
	{
		// TODO
	}
	keUnlockSpinlock(&terminallock);
	keSetExecutionLevel(oldlevel);
	return 0;
}

char *sysTerminalPreparePanic(int *width, int *height)
{
	*width = 80;
	*height = 25;
	return (char*)0xC00B8000;
}

