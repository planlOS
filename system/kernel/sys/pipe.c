/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/pipe.h"
#include "ke/errors.h"
#include "fs/request.h"
#include "ke/debug.h"
#include <string.h>
#include <stdlib.h>

typedef struct
{
	FsFileSystem fs;
} SysPipeFS;

typedef struct SysPipeFile
{
	FsFile file;
	struct SysPipe *pipe;
} SysPipeFile;

typedef struct SysPipe
{
	SysPipeFile in;
	SysPipeFile out;
	char buffer[4096];
	int buffered;
	KeSpinlock lock;
	KeSpinlock writelock;
	KeSpinlock readlock;
} SysPipe;


static SysPipeFS fs;

static int sysPipeClose(SysPipeFile *file)
{
	SysPipe *pipe = file->pipe;
	keLockSpinlock(&pipe->lock);
	// Only close pipe when both ends are closed
	if (((file == &pipe->in) && (pipe->out.file.refcount != 0))
		|| ((file == &pipe->out) && (pipe->in.file.refcount != 0)))
	{
		keUnlockSpinlock(&pipe->lock);
		return 0;
	}
	free(pipe);
	return 0;
}
static int sysPipeRead(SysPipeFile *file, char *data, int length)
{
	int origlength = length;
	SysPipe *pipe = file->pipe;
	// Only one thread is allowed to read from the pipe at a time
	keLockSpinlock(&pipe->readlock);
	// TODO: Do this in another thread to allow non-blocking writing?
	while (length > 0)
	{
		keLockSpinlock(&pipe->lock);
		int max = pipe->buffered;
		if (max >= length)
		{
			memcpy(data, pipe->buffer + pipe->buffered - length, length);
			pipe->buffered -= length;
			keUnlockSpinlock(&pipe->lock);
			break;
		}
		else
		{
			memcpy(data, pipe->buffer, max);
			data += max;
			length -= max;
			pipe->buffered -= max;
		}
		keUnlockSpinlock(&pipe->lock);
		asm volatile("int $0x32");
	}
	keUnlockSpinlock(&pipe->readlock);
	return origlength;
}
static int sysPipeWrite(SysPipeFile *file, char *data, int length)
{
	int origlength = length;
	SysPipe *pipe = file->pipe;
	// Only one thread is allowed to write to the pipe at a time
	keLockSpinlock(&pipe->writelock);
	// Continue writing until everything was written
	// TODO: Do this in another thread to allow non-blocking writing?
	while (length > 0)
	{
		keLockSpinlock(&pipe->lock);
		int max = 4096 - pipe->buffered;
		if (max >= length)
		{
			memcpy(pipe->buffer + pipe->buffered, data, length);
			pipe->buffered += length;
			keUnlockSpinlock(&pipe->lock);
			break;
		}
		else
		{
			memcpy(pipe->buffer + pipe->buffered, data, max);
			data += max;
			length -= max;
			pipe->buffered += max;
		}
		keUnlockSpinlock(&pipe->lock);
		asm volatile("int $0x32");
	}
	keUnlockSpinlock(&pipe->writelock);
	return origlength;
}

static int sysPipeRequest(struct FsFileSystem *fs, struct FsRequest *request)
{
	if (!request->file) return KE_ERROR_UNKNOWN;
	SysPipeFile *file = (SysPipeFile*)request->file;
	switch (request->type)
	{
		case FS_REQUEST_CLOSE:
			request->return_value = sysPipeClose(file);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_READ:
			request->return_value = sysPipeRead(file, request->buffer, request->bufferlength);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_WRITE:
			request->return_value = sysPipeWrite(file, request->buffer, request->bufferlength);
			fsFinishRequest(request);
			return 0;
		default:
			return KE_ERROR_UNKNOWN;
	}
}

void sysInitPipe(void)
{
	memset(&fs, 0, sizeof(fs));
	fs.fs.query_request = sysPipeRequest;
}

int sysOpenPipe(FsFileHandle *in, FsFileHandle *out)
{
	// Create pipe
	SysPipe *pipe = malloc(sizeof(SysPipe));
	memset(pipe, 0, sizeof(SysPipe));
	pipe->in.pipe = pipe;
	pipe->in.file.fs = &fs.fs;
	pipe->in.file.refcount = 1;
	pipe->out.pipe = pipe;
	pipe->out.file.fs = &fs.fs;
	pipe->out.file.refcount = 1;
	// Create file handles
	in->file = &pipe->in.file;
	out->file = &pipe->out.file;
	return 0;
}

