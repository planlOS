/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/serial.h"
#include "ke/ports.h"
#include "fs/devfs.h"
#include "fs/request.h"

#define SERIAL 0x3f8

static void sysSerialPrintChar(char c)
{
	// Serial output
	while ((inb(SERIAL + 5) & 0x20) == 0);
	outb(SERIAL, c);
}

static int sysTerminalRequest(struct FsDeviceFile *file, FsRequest *request)
{
	switch (request->type)
	{
		case FS_REQUEST_READ:
			request->return_value = 0;
			fsFinishRequest(request);
			break;
		case FS_REQUEST_WRITE:
			{
				uint32_t i;
				for (i = 0; i < request->bufferlength; i++)
				{
					sysSerialPrintChar(((char*)request->buffer)[i]);
				}
				request->return_value = request->bufferlength;
				fsFinishRequest(request);
			}
			break;
		case FS_REQUEST_IOCTL:
			request->return_value = -1;
			fsFinishRequest(request);
			break;
		default:
			request->return_value = -1;
			fsFinishRequest(request);
	}
	return 0;
}

static FsDeviceFile serialfile;

void sysInitSerial(void)
{
	serialfile.path = "serial";
	serialfile.query_request = sysTerminalRequest;
	fsCreateDeviceFile(&serialfile);
}

