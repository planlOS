/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sys/syscall.h"
#include "sys/pipe.h"
#include "ke/debug.h"
#include "ke/level.h"
#include "ke/elf.h"
#include "ke/timer.h"
#include "ke/cpu.h"
#include "net/socket.h"
#include <string.h>
#include <stdlib.h>

void *sysMapUserMemory(KeThread *thread, uintptr_t addr, uint32_t size, uint32_t writeable)
{
	KeProcess *process = thread->process;
	uint32_t pageoffset = addr & 0xFFF;
	uint32_t pagecount = (size + pageoffset + 0xFFF) / 0x1000;
	uint32_t i;
	// Validate pages
	for (i = 0; i < pagecount; i++)
	{
		// TODO: Check flags
		if (!mmGetPhysAddress(&process->memory, (addr & ~0xFFF) + i * 0x1000))
		{
			kePrint("No memory at %x\n", (addr & ~0xFFF) + i * 0x1000);
			return 0;
		}
	}
	// Map pages
	uintptr_t vaddr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE,
		1, pagecount * 0x1000);
	if (!vaddr)
	{
		kePrint("Could not allocate %d bytes.\n", pagecount * 0x1000);
		return 0;
	}
	for (i = 0; i < pagecount; i++)
	{
		uintptr_t paddr = mmGetPhysAddress(&process->memory, (addr & ~0xFFF) + i * 0x1000);
		mmMapKernelMemory(paddr, vaddr + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	return (void*)(vaddr + pageoffset);
}

void sysUnmapUserMemory(void *data, uint32_t size)
{
	uintptr_t addr = (uintptr_t)data;
	uint32_t pageoffset = addr & 0xFFF;
	uint32_t pagecount = (size + pageoffset + 0xFFF) / 0x1000;
	uint32_t i;
	for (i = 0; i < pagecount; i++)
	{
		mmMapKernelMemory(0, addr + i * 0x1000, 0);
	}
}

int sysCreateFileDescriptor(KeProcess *process, FsFileHandle *fh)
{
	// Search for empty fds
	uint32_t i;
	int fd = -1;
	for (i = 3; i < process->fdcount; i++)
	{
		if (process->fd[i] == 0)
		{
			fd = i;
		}
	}
	if (fd == -1)
	{
		// Increase fd array
		process->fd = realloc(process->fd, (process->fdcount + 1) * sizeof(FsFileHandle*));
		fd = process->fdcount;
		process->fdcount++;
	}
	process->fd[fd] = fh;
	return fd;
}

void sysOpen(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	KeProcess *process = thread->process;
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	// TODO: Can be crashed easily
	char *filename = strdup((char*)*param1);
	if (strlen(filename) == 0)
	{
		*param1 = -1;
		keSetExecutionLevel(KE_LEVEL_HIGH);
		return;
	}
	if (filename[0] != '/')
	{
		// Relative path
		char *tmp = malloc(strlen(filename) + 1 + strlen(process->cwd));
		strcpy(tmp, process->cwd);
		strcpy(tmp + strlen(process->cwd), filename);
		free(filename);
		filename = tmp;
	}
	kePrint("Opening %s\n", filename);
	uint32_t mode = 0;
	if (*param2 & 1) mode |= FS_OPEN_CREATE;
	if (*param2 & 0x200) mode |= FS_OPEN_READ;
	if (*param2 & 0x400) mode |= FS_OPEN_RW;
	if (*param2 & 0x800) mode |= FS_OPEN_WRITE;
	FsFileHandle *file = fsOpen(filename, mode);
	if (!file)
	{
		*param1 = -1;
		keSetExecutionLevel(KE_LEVEL_HIGH);
	}
	else
	{
		keSetExecutionLevel(KE_LEVEL_HIGH);
		keLockSpinlock(&process->lock);
		uint32_t i;
		int fd = -1;
		for (i = 3; i < process->fdcount; i++)
		{
			if (process->fd[i] == 0)
			{
				fd = i;
			}
		}
		if (fd == -1)
		{
			process->fd = realloc(process->fd, (process->fdcount + 1) * sizeof(FsFileHandle*));
			fd = process->fdcount;
			process->fdcount++;
		}
		process->fd[fd] = file;
		*param1 = fd;
		keUnlockSpinlock(&process->lock);
		kePrint("Opened %s as %d\n", filename, fd);
	}
	free(filename);
}

void sysFork(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	uintptr_t current_stack = thread->kernelstack;
	KeProcess *process = thread->process;
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	// Create new process
	KeProcess *newprocess = keCreateProcess();
	keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	if (mmCloneAddressSpace(&newprocess->memory, &process->memory))
	{
		keUnlockSpinlock(mmGetMemoryLock());
		keSetExecutionLevel(KE_LEVEL_NORMAL);
		keDestroyProcess(newprocess);
		keSetExecutionLevel(KE_LEVEL_HIGH);
		*param1 = -1;
	}
	else
	{
		keUnlockSpinlock(mmGetMemoryLock());
		// Clone file handles
		keLockSpinlock(&newprocess->lock);
		newprocess->fd = realloc(newprocess->fd, process->fdcount * sizeof(FsFileHandle*));
		newprocess->fdcount = process->fdcount;
		memcpy(newprocess->fd, process->fd, process->fdcount * sizeof(FsFileHandle*));
		uint32_t i;
		for (i = 0; i < process->fdcount; i++)
		{
			if (process->fd[i])
			{
				process->fd[i]->file->refcount++;
				newprocess->fd[i] = malloc(sizeof(FsFileHandle));
				memcpy(newprocess->fd[i], process->fd[i], sizeof(FsFileHandle));
			}
		}
		
		free(newprocess->cwd);
		newprocess->cwd = strdup(process->cwd);
		keUnlockSpinlock(&newprocess->lock);
		// Create thread
		KeThread *newthread = keCloneThread(newprocess, thread, current_stack);
		keThreadSetParam(newthread, 1, 0);
		
		*param1 = newprocess->pid;
	}
}
void sysExec(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	KeProcess *process = thread->process;
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	// Load executable file
	// FIXME: Can be crashed by users
	char *filename = strdup((char*)*param1);
	FsFileHandle *file = fsOpen(filename, FS_OPEN_READ);
	if (!file)
	{
		kePrint("exec: Could not open \"%s\".\n", filename);
		free(filename);
		keSetExecutionLevel(KE_LEVEL_HIGH);
		*param1 = -1;
		return;
	}
	// Read program
	int size = fsSeek(file, 0, 2);
	fsSeek(file, 0, 0);
	keSetExecutionLevel(KE_LEVEL_HIGH);
	char *program = malloc(size);
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	if (!program)
	{
		kePrint("exec: Out of memory.\n");
		free(filename);
		fsClose(file);
		keSetExecutionLevel(KE_LEVEL_HIGH);
		*param1 = -1;
		return;
	}
	if (fsRead(file, program, size + 10, 1) != size)
	{
		kePrint("exec: Could not read \"%s\".\n", filename);
		free(filename);
		fsClose(file);
		keSetExecutionLevel(KE_LEVEL_HIGH);
		*param1 = -1;
		return;
	}
	uintptr_t entry = ((ElfHeader*)program)->e_entry;
	free(filename);
	fsClose(file);
	if ((program[0] != 0x7F) || (program[1] != 'E')
		|| (program[2] != 'L') || (program[3] != 'F'))
	{
		kePrint("Not a valid ELF file: %x%x\n", *((uint32_t*)program + 1), *((uint32_t*)program));
		keSetExecutionLevel(KE_LEVEL_HIGH);
		free(program);
		*param1 = -1;
		return;
	}
	// Map arguments/environment variables
	keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	uint32_t argcount = *param2;
	uintptr_t *args = 0;
	if (argcount != 0)
	{
		args = sysMapUserMemory(thread, *param3, argcount * sizeof(char*), 0);
		if (!args)
		{
			keUnlockSpinlock(mmGetMemoryLock());
			free(program);
			*param1 = -1;
			return;
		}
	}
	uint32_t envcount = *param4;
	uintptr_t *env = 0;
	if (envcount != 0)
	{
		env = sysMapUserMemory(thread, *param5, envcount * sizeof(char*), 0);
		if (!env)
		{
			if (args)
				sysUnmapUserMemory(args, argcount * sizeof(char*));
			keUnlockSpinlock(mmGetMemoryLock());
			free(program);
			*param1 = -1;
			return;
		}
	}
	// Copy data to kernel space
	uint32_t pagecount = ((argcount + envcount + 2) * sizeof(char*) + 4095) / 0x1000;
	if (pagecount > 1)
	{
		keSetExecutionLevel(KE_LEVEL_HIGH);
		free(program);
		*param1 = -1;
		return;
	}
	uintptr_t page = mmAllocPhysicalMemory(0, 0, 0x1000);
	uintptr_t argdata = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, 0x1000);
	mmMapKernelMemory(page, argdata, MM_MAP_READ | MM_MAP_WRITE);
	uint32_t stringsize = 0;
	uint32_t i;
	for (i = 0; i < argcount; i++)
	{
		stringsize += strlen((char*)args[i]) + 1;
	}
	for (i = 0; i < envcount; i++)
	{
		stringsize += strlen((char*)env[i]) + 1;
	}
	memset((void*)argdata, 0xab, 0x1000);
	((uintptr_t*)argdata)[argcount] = 0;
	((uintptr_t*)argdata)[argcount + 1 + envcount] = 0;
	uintptr_t datapage = mmAllocPhysicalMemory(0, 0, (stringsize + 0xFFF) & ~0xFFF);
	uintptr_t datavirt = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE,
		MM_MIN_KERNEL_PAGE, 1, (stringsize + 0xFFF) & ~0xFFF);
	for (i = 0; i < (stringsize + 0xFFF) / 0x1000; i++)
	{
		mmMapKernelMemory(datapage + i * 0x1000, datavirt + i * 0x1000,
			MM_MAP_READ | MM_MAP_WRITE);
	}
	char *s = (char*)datavirt;
	for (i = 0; i < argcount; i++)
	{
		((uintptr_t*)argdata)[i] = (uintptr_t)s - datavirt;
		strcpy(s, (char*)args[i]);
		s += strlen((char*)args[i]) + 1;
	}
	for (i = 0; i < envcount; i++)
	{
		((uintptr_t*)argdata)[argcount + 1 + i] = (uintptr_t)s - datavirt;
		strcpy(s, (char*)env[i]);
		s += strlen((char*)env[i]) + 1;
	}
	// Unmap arguments again
	if (args)
		sysUnmapUserMemory(args, argcount * sizeof(char*));
	if (env)
		sysUnmapUserMemory(args, argcount * sizeof(char*));
	keUnlockSpinlock(mmGetMemoryLock());
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	// Clear process
	keClearProcess(process, thread);
	keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	keElfMapProgram(&process->memory, program, size);
	uintptr_t targetaddr = mmFindFreePages(&process->memory, MM_MAX_USER_PAGE,
		MM_MIN_USER_PAGE, 0, 0x1000);
	mmMapMemory(&process->memory, page, targetaddr, MM_MAP_READ | MM_MAP_WRITE);
	uintptr_t dataaddr = mmFindFreePages(&process->memory, MM_MAX_USER_PAGE,
		MM_MIN_USER_PAGE, 0, (stringsize + 0xFFF) & ~0xFFF);
	for (i = 0; i < argcount; i++)
	{
		((uintptr_t*)argdata)[i] += dataaddr;
	}
	for (i = 0; i < envcount; i++)
	{
		((uintptr_t*)argdata)[argcount + 1 + i] += dataaddr;
	}
	mmMapKernelMemory(0, argdata, 0);
	for (i = 0; i < (stringsize + 0xFFF) / 0x1000; i++)
	{
		mmMapMemory(&process->memory, datapage + i * 0x1000,
			dataaddr + i * 0x1000, MM_MAP_READ | MM_MAP_WRITE);
		mmMapKernelMemory(0, datavirt + i * 0x1000, 0);
	}
	keUnlockSpinlock(mmGetMemoryLock());
	free(program);
	keDestroyThread(thread);
	keCreateThread(process, entry, 4, envcount,
		targetaddr + (argcount + 1) * sizeof(char*), argcount, targetaddr);
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	// Schedule into other thread
	asm volatile("int $0x32");
}
void sysPipe(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	KeProcess *process = thread->process;
	// Create pipe
	FsFileHandle *in = malloc(sizeof(FsFileHandle));
	memset(in, 0, sizeof(FsFileHandle));
	FsFileHandle *out = malloc(sizeof(FsFileHandle));
	memset(out, 0, sizeof(FsFileHandle));
	if (sysOpenPipe(in, out))
	{
		free(in);
		free(out);
		*param1 = -1;
	}
	// Save file descriptors
	keLockSpinlock(&process->lock);
	*param1 = sysCreateFileDescriptor(process, in);
	*param2 = sysCreateFileDescriptor(process, out);
	keUnlockSpinlock(&process->lock);
}
void sysSocket(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	KeProcess *process = thread->process;
	// Create socket
	FsFileHandle *fh = malloc(sizeof(FsFileHandle));
	memset(fh, 0, sizeof(FsFileHandle));
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	int status = netOpenSocket(fh, NET_SOCKET_STREAM);
	keSetExecutionLevel(KE_LEVEL_HIGH);
	if (status)
	{
		*param1 = -1;
		return;
	}
	// Save file descriptors
	keLockSpinlock(&process->lock);
	*param1 = sysCreateFileDescriptor(process, fh);
	keUnlockSpinlock(&process->lock);
}
void sysIoctl(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	KeProcess *process = thread->process;
	// Check maximum fd
	if ((*param1 >= process->fdcount) || !process->fd[*param1])
	{
		kePrint("ioctl: invalid fd.\n");
		*param1 = -1;
		return;
	}
	// Get ioctl size
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	int datasize = fsGetIOCTLSize(process->fd[*param1], *param2);
	keSetExecutionLevel(KE_LEVEL_HIGH);
	if (datasize == -1)
	{
		*param1 = -1;
		return;
	}
	// Map memory to kernel space
	uintptr_t data;
	if (datasize)
	{
		keLockSpinlock(mmGetMemoryLock());
		void *buffer = sysMapUserMemory(thread, *param3, datasize, 0);
		keUnlockSpinlock(mmGetMemoryLock());
		if (!buffer)
		{
			kePrint("ioctl: sysMapUserMemory failed (%x: %d).\n", (uint32_t)*param3, datasize);
			*param1 = -1;
			return;
		}
		data = (uintptr_t)buffer;
	}
	else
	{
		data = *param3;
	}
	// Send ioctl
	keSetExecutionLevel(KE_LEVEL_NORMAL);
	*param1 = fsIOCTL(process->fd[*param1], *param2, data);
	keSetExecutionLevel(KE_LEVEL_HIGH);
	// Unmap memory
	if (datasize)
	{
		keLockSpinlock(mmGetMemoryLock());
		sysUnmapUserMemory((void*)data, datasize);
		keUnlockSpinlock(mmGetMemoryLock());
	}
}
void sysFcntl(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	KeProcess *process = thread->process;
	// Check file descriptor
	if ((*param1 >= process->fdcount) || !process->fd[*param1])
	{
		kePrint("fcntl: invalid fd.\n");
		*param1 = -1;
		return;
	}
	// Execute action
	int fcntl = *param2;
	switch (fcntl)
	{
		case SYS_FCNTL_DUP:
			{
				int newfd = *param3;
				if (!newfd)
				{
					// dup()
					process->fd[*param1]->file->refcount++;
					FsFileHandle *newfh = malloc(sizeof(FsFileHandle));
					memcpy(newfh, process->fd[*param1], sizeof(FsFileHandle));
					*param1 = sysCreateFileDescriptor(process, newfh);
				}
				else
				{
					// dup2()
					if (newfd > 4096)
					{
						*param1 = -1;
						return;
					}
					process->fd[*param1]->file->refcount++;
					FsFileHandle *newfh = malloc(sizeof(FsFileHandle));
					memcpy(newfh, process->fd[*param1], sizeof(FsFileHandle));
					
					while ((newfd < (int)process->fdcount) && process->fd[newfd])
					{
						newfd++;
					}
					if ((int)process->fdcount <= newfd)
					{
						process->fd = realloc(process->fd, (newfd + 1) * sizeof(FsFileHandle*));
						memset(process->fd + process->fdcount, 0, (newfd + 1 - process->fdcount) * sizeof(FsFileHandle*));
						process->fdcount = newfd + 1;
					}
					process->fd[newfd] = newfh;
					*param1 = newfd;
				}
				break;
			}
		default:
			*param1 = -1;
			break;
	}
}
void sysSignal(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	int signal = *param1;
	uintptr_t handler = *param2;
	if (signal >= NSIG)
	{
		*param1 = -1;
	}
	thread->process->signal_handlers[signal] = handler;
	*param1 = 0;
}
void sysRaise(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	int signal = *param1;
	if (signal >= NSIG)
	{
		*param1 = -1;
		return;
	}
	*param1 = 0;
	keSendSignal(thread->process, 0, signal);
}
void sysKill(KeThread *thread, uintptr_t *param1, uintptr_t *param2,
	uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	int pid = *param1;
	int signal = *param2;
	if (signal >= NSIG)
	{
		*param1 = -1;
		return;
	}
	KeProcess *target = keGetProcess(pid);
	if (!target)
	{
		*param1 = -1;
		return;
	}
	*param1 = 0;
	keSendSignal(target, 0, signal);
}

void sysSyscall(KeThread *thread, uintptr_t index, uintptr_t *param1,
	uintptr_t *param2, uintptr_t *param3, uintptr_t *param4, uintptr_t *param5)
{
	if (!thread->process)
	{
		// TODO: Panic
		return;
	}
	
	switch (index)
	{
		case SYS_EXIT:
			keTerminateProcess(thread->process);
			asm volatile("int $0x32");
			break;
		case SYS_OPEN:
			sysOpen(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_CLOSE:
			{
				KeProcess *process = thread->process;
				// Check maximum fd
				if ((*param1 >= process->fdcount) || !process->fd[*param1])
				{
					*param1 = -1;
					return;
				}
				kePrint("Closing %d.\n", *param1);
				FsFileHandle *file = process->fd[*param1];
				process->fd[*param1] = 0;
				// Close file
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				fsClose(file);
				keSetExecutionLevel(KE_LEVEL_HIGH);
				*param1 = 0;
			}
			break;
		case SYS_READ:
			{
				KeProcess *process = thread->process;
				// Check maximum fd
				if ((*param1 >= process->fdcount) || !process->fd[*param1])
				{
					kePrint("read: invalid fd.\n");
					*param1 = -1;
					return;
				}
				
				// Map buffer
				keLockSpinlock(mmGetMemoryLock());
				void *buffer = sysMapUserMemory(thread, *param2, *param3, 1);
				keUnlockSpinlock(mmGetMemoryLock());
				if (!buffer)
				{
					kePrint("read: sysMapUserMemory failed (%x: %d).\n", (uint32_t)*param2, (int)*param3);
					// TODO: Stop program here
					*param1 = -1;
					return;
				}
				// Read data
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				FsFileHandle *file = process->fd[*param1];
				//kePrint("Reading %d bytes from %d.\n", (int)*param3, (int)*param1);
				int read = fsRead(file, buffer, *param3, 1);
				keSetExecutionLevel(KE_LEVEL_HIGH);
				
				// Unmap buffer
				keLockSpinlock(mmGetMemoryLock());
				sysUnmapUserMemory(buffer, *param3);
				keUnlockSpinlock(mmGetMemoryLock());
				
				*param1 = read;
			}
			break;
		case SYS_WRITE:
			{
				KeProcess *process = thread->process;
				// Check maximum fd
				if ((*param1 >= process->fdcount) || !process->fd[*param1])
				{
					kePrint("write: invalid fd.\n");
					*param1 = -1;
					return;
				}
				
				// Map buffer
				keLockSpinlock(mmGetMemoryLock());
				void *buffer = sysMapUserMemory(thread, *param2, *param3, 0);
				keUnlockSpinlock(mmGetMemoryLock());
				if (!buffer)
				{
					kePrint("write: sysMapUserMemory failed (%x: %d).\n", (uint32_t)*param2, (int)*param3);
					// TODO: Stop program here
					*param1 = -1;
					return;
				}
				// Read data
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				FsFileHandle *file = process->fd[*param1];
				int written = fsWrite(file, buffer, *param3);
				keSetExecutionLevel(KE_LEVEL_HIGH);
				
				// Unmap buffer
				keLockSpinlock(mmGetMemoryLock());
				sysUnmapUserMemory(buffer, *param3);
				keUnlockSpinlock(mmGetMemoryLock());
				
				*param1 = written;
			}
			break;
		case SYS_SEEK:
			{
				KeProcess *process = thread->process;
				// Check maximum fd
				if ((*param1 >= process->fdcount) || !process->fd[*param1])
				{
					kePrint("seek: invalid fd.\n");
					*param1 = -1;
					return;
				}
				// Seek in file
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				FsFileHandle *file = process->fd[*param1];
				*param1 = fsSeek(file, *param2, *param3);
				keSetExecutionLevel(KE_LEVEL_HIGH);
			}
			break;
		case SYS_MKNOD:
			{
				// TODO: Can be crashed easily
				char *filename = strdup((char*)*param1);
				uint32_t mode = 0;
				uint32_t type = *param2 & 0xF00;
				if (type == 0x400)
					mode = FS_MKNOD_FILE;
				else if (type == 0x500)
					mode = FS_MKNOD_DIR;
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				*param1 = fsMknod(filename, mode);
				keSetExecutionLevel(KE_LEVEL_HIGH);
			}
			break;
		case SYS_ALLOC:
			{
				KeProcess *process = thread->process;
				uint32_t size = *param1;
				uint32_t pagecount = (size + 0xFFF) / 0x1000;
				keLockSpinlock(mmGetMemoryLock());
				// Look for free memory
				uintptr_t vaddr = mmFindFreePages(&process->memory, MM_MAX_USER_PAGE,
					MM_MIN_USER_PAGE, 0, pagecount * 0x1000);
				// Map memory
				if (vaddr)
				{
					uint32_t i;
					for (i = 0; i < pagecount; i++)
					{
						uintptr_t paddr = mmAllocPhysicalMemory(0, 0, 0x1000);
						// TODO: Error handling
						mmMapMemory(&process->memory, paddr, vaddr + i * 0x1000,
							MM_MAP_READ | MM_MAP_WRITE);
					}
				}
				keUnlockSpinlock(mmGetMemoryLock());
				*param1 = vaddr;
			}
			break;
		case SYS_FREE:
			{
				KeProcess *process = thread->process;
				uint32_t addr = *param1;
				uint32_t size = *param2;
				uint32_t pagecount = (size + 0xFFF) / 0x1000;
				uint32_t i;
				for (i = 0; i < pagecount; i++)
				{
					uintptr_t vaddr = (addr & ~0xFFF) + i * 0x1000;
					// Free memory
					uintptr_t paddr = mmGetPhysAddress(&process->memory, vaddr);
					if (paddr) mmFreePhysicalMemory(paddr, 0x1000);
					// Unmap page
					mmMapMemory(&process->memory, 0, vaddr, 0);
				}
			}
			break;
		case SYS_FORK:
			sysFork(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_EXEC:
			sysExec(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_WAIT:
			{
				// Get process
				int pid = *param1;
				uint32_t options = *param3;
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				KeProcess *process = keGetProcess(pid);
				keSetExecutionLevel(KE_LEVEL_HIGH);
				if (!process)
				{
					*param1 = -1;
					break;
				}
				// If the process was terminated, return at once
				if (process->zombie)
				{
					keSetExecutionLevel(KE_LEVEL_NORMAL);
					keDestroyProcess(process);
					keSetExecutionLevel(KE_LEVEL_HIGH);
					*param1 = 0;
					break;
				}
				if (options & 1)
				{
					// Don't block
					*param1 = -1;
					break;
				}
				else
				{
					if (process->waitthread)
					{
						*param1 = -1;
						break;
					}
					process->waitthread = thread;
					thread->status = KE_THREAD_WAIT;
					asm volatile("int $0x32");
				}
			}
			break;
		case SYS_SLEEP:
			thread->status = KE_THREAD_SLEEP;
			thread->timeout = keGetTime() + *param1;
			asm volatile("int $0x32");
			break;
		case SYS_GETWD:
			{
				KeProcess *process = thread->process;
				int length = strlen(process->cwd) + 1;
				if (length > (int)*param2)
				{
					length = *param2;
				}
				// Map buffer
				keLockSpinlock(mmGetMemoryLock());
				char *buffer = sysMapUserMemory(thread, *param1, length + 1, 0);
				keUnlockSpinlock(mmGetMemoryLock());
				if (!buffer)
				{
					*param1 = -1;
					return;
				}
				// Read data
				strncpy(buffer, process->cwd, length);
				
				// Unmap buffer
				keLockSpinlock(mmGetMemoryLock());
				sysUnmapUserMemory(buffer, length + 1);
				keUnlockSpinlock(mmGetMemoryLock());
				
				*param1 = 0;
			}
			break;
		case SYS_CHDIR:
			{
				// FIXME: Can be crashed by users
				char *filename = strdup((char*)*param1);
				KeProcess *process = thread->process;
				// Validate path
				keSetExecutionLevel(KE_LEVEL_NORMAL);
				if (strlen(filename) == 0)
				{
					*param1 = -1;
					keSetExecutionLevel(KE_LEVEL_HIGH);
					break;
				}
				if (filename[0] != '/')
				{
					// Relative path
					char *tmp = malloc(strlen(filename) + strlen(process->cwd) + 2);
					strcpy(tmp, process->cwd);
					strcpy(tmp + strlen(process->cwd), filename);
					free(filename);
					filename = tmp;
					if (filename[strlen(filename) - 1] != '/')
					{
						filename[strlen(filename) + 1] = 0;
						filename[strlen(filename)] = '/';
					}
				}
				FsFileHandle *file = fsOpen(filename, FS_OPEN_READ);
				if (!file)
				{
					*param1 = -1;
					keSetExecutionLevel(KE_LEVEL_HIGH);
					break;
				}
				fsClose(file);
				keSetExecutionLevel(KE_LEVEL_HIGH);
				// Set working directory
				free(process->cwd);
				process->cwd = filename;
				*param1 = 0;
			}
			break;
		case SYS_PIPE:
			sysPipe(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_SOCKET:
			sysSocket(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_IOCTL:
			sysIoctl(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_FCNTL:
			sysFcntl(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_SIGNAL:
			sysSignal(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_RAISE:
			sysRaise(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_KILL:
			sysKill(thread, param1, param2, param3, param4, param5);
			break;
		case SYS_GETPID:
			*param1 = thread->process->pid;
			break;
		case SYS_TIME:
			{
				uint64_t time = keGetTime();
				uint32_t seconds = keGetCurrentCPU()->starttime + time / 1000000;
				*param1 = seconds;
				*param2 = time % 1000000;
			}
			break;
		default:
			*param1 = -1;
			break;
	}
}

