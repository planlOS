
#include "arch/cc.h"
#include "ke/panic.h"
#include "ke/debug.h"
#include "lwip/sys.h"

KeSpinlock critlock;

void sys_init(void)
{
	keInitSpinlock(&critlock);
}

void LWIP_PLATFORM_ASSERT(char *msg)
{
	kePrint("%s\n", msg);
	kePanic(0, 42);
}


sys_sem_t sys_sem_new(u8_t count)
{
	sys_sem_t sem = malloc(sizeof(KeSpinlock));
	keInitSpinlock(sem);
	if (count) keLockSpinlock(sem);
	return sem;
}
void sys_sem_free(sys_sem_t sem)
{
	free(sem);
}
void sys_sem_signal(sys_sem_t sem)
{
	keUnlockSpinlock(sem);
}
u32_t sys_arch_sem_wait(sys_sem_t sem, u32_t timeout)
{
	if (!timeout)
	{
		uint64_t starttime = keGetTime();
		keLockSpinlock(sem);
		uint64_t needed = keGetTime() - starttime;
		return needed / 1000;
	}
	else
	{
		uint64_t starttime = keGetTime();
		uint64_t currenttime = starttime;
		do
		{
			if (!keTryLockSpinlock(sem))
			{
				return (currenttime - starttime) / 1000;
			}
			currenttime = keGetTime();
		}
		while (currenttime - starttime < timeout * 1000);
		return SYS_ARCH_TIMEOUT;
	}
}

sys_mbox_t sys_mbox_new(int size)
{
	struct sys_mbox *mbox = malloc(sizeof(struct sys_mbox));
	keInitSpinlock(&mbox->lock);
	mbox->firstentry = 0;
	return mbox;
}
void sys_mbox_post(sys_mbox_t mbox, void *msg)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&mbox->lock);
	sys_mbox_entry_t *entry = malloc(sizeof(sys_mbox_entry_t));
	entry->data = msg;
	entry->next = 0;
	if (!mbox->firstentry)
	{
		mbox->firstentry = entry;
	}
	else
	{
		sys_mbox_entry_t *prev = mbox->firstentry;
		while (prev->next) prev = prev->next;
		prev->next = entry;
	}
	keUnlockSpinlock(&mbox->lock);
	keSetExecutionLevel(oldlevel);
}
err_t sys_mbox_trypost(sys_mbox_t mbox, void *msg)
{
	sys_mbox_post(mbox, msg);
	return ERR_OK;
}
u32_t sys_arch_mbox_fetch(sys_mbox_t mbox, void **msg, u32_t timeout)
{
	if (!timeout)
	{
		uint64_t starttime = keGetTime();
		while (sys_arch_mbox_tryfetch(mbox, msg) != 0);
		uint64_t needed = keGetTime() - starttime;
		return needed / 1000;
	}
	else
	{
		uint64_t starttime = keGetTime();
		uint64_t currenttime = starttime;
		do
		{
			if (sys_arch_mbox_tryfetch(mbox, msg) == 0)
			{
				return (currenttime - starttime) / 1000;
			}
			asm volatile("int $0x32");
			currenttime = keGetTime();
		}
		while (currenttime - starttime < timeout * 1000);
		return SYS_ARCH_TIMEOUT;
	}
}
u32_t sys_arch_mbox_tryfetch(sys_mbox_t mbox, void **msg)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&mbox->lock);
	u32_t status = SYS_MBOX_EMPTY;
	if (mbox->firstentry)
	{
		sys_mbox_entry_t *entry = mbox->firstentry;
		mbox->firstentry = entry->next;
		*msg = entry->data;
		free(entry);
		status = 0;
	}
	keUnlockSpinlock(&mbox->lock);
	keSetExecutionLevel(oldlevel);
	return status;
}
void sys_mbox_free(sys_mbox_t mbox)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&mbox->lock);
	if (mbox->firstentry)
	{
		kePrint("Trying to free full mailbox.\n");
	}
	free(mbox);
	keSetExecutionLevel(oldlevel);
}

sys_prot_t sys_arch_protect(void)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	return oldlevel;
}
void sys_arch_unprotect(sys_prot_t p)
{
	keSetExecutionLevel(p);
}

static struct sys_timeouts deftimeouts;

static KeSpinlock threadlock;
static sys_thread_t *threads = 0;
static uint32_t threadcount = 0;

sys_thread_t sys_thread_new(char *name, void (*func)(void *arg), void *arg, int stacksize, int prio)
{
	kePrint("Creeating thread %s\n", name);
	// Create thread struct
	sys_thread_t thread = malloc(sizeof(struct sys_thread));
	memset(thread, 0, sizeof(struct sys_thread));
	thread->name = name;
	// Add to list
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(&threadlock);
	threads = realloc(threads, (threadcount + 1) * sizeof(sys_thread_t));
	threads[threadcount] = thread;
	threadcount++;
	keUnlockSpinlock(&threadlock);
	keSetExecutionLevel(oldlevel);
	// Start thread
	keCreateKernelThread((uintptr_t)func, 1, arg);
	return thread;
}

struct sys_timeouts *sys_arch_timeouts(void)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	KeThread *currentthread = keGetCurrentCPU()->currentthread;
	keLockSpinlock(&threadlock);
	struct sys_timeouts *timeouts = &deftimeouts;
	uint32_t i;
	for (i = 0; i < threadcount; i++)
	{
		if (threads[i]->thread == currentthread)
		{
			timeouts = &threads[i]->timeouts;
		}
	}
	keUnlockSpinlock(&threadlock);
	keSetExecutionLevel(oldlevel);
   return timeouts;
}


