/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <net/net.h>
#include <net/socket.h>
#include <ke/debug.h>
#include <string.h>
#include <stdlib.h>
#include <lwip/init.h>
#include <lwip/tcpip.h>

static NetNetworkDevice **devices = 0;
static uint32_t devicecount = 0;

void lwip_add_device(NetNetworkDevice *device);
void lwip_device_receive(NetNetworkDevice *device, char *data, int length);

uint16_t netConvert16(uint16_t value)
{
	return (value << 8) | (value >> 8);
}
uint32_t netConvert32(uint32_t value)
{
	return (value << 24) | ((value & 0xFF00) << 8) | ((value & 0xFF0000) >> 8) | (value >> 24);
}
uint64_t netConvert64(uint64_t value)
{
	uint32_t low = value;
	uint32_t high = value >> 32;
	low = netConvert32(low);
	high = netConvert32(high);
	return ((uint64_t)low << 32) | high;
}

void netInit(void)
{
	tcpip_init(NULL, NULL);
	netInitSocket();
}

void netRegisterDevice(NetNetworkDevice *device, char *name)
{
	uint8_t ip[] = {172, 20, 0, 2};
	memcpy(device->ip, ip, 4);
	// Add device to the list
	devices = realloc(devices, (devicecount + 1) * sizeof(NetNetworkDevice*));
	devices[devicecount] = device;
	devicecount++;
	
	lwip_add_device(device);
	
	/*// Test
	uint8_t msg[512] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x08, 0x06, 0x00, 0x01, 0x08, 0x00, 0x06, 0x04, 0x00, 0x01,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		172, 20, 0, 2,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		172, 20, 0, 1};
	memcpy(msg + 6, (char*)&device->mac, 6);
	memcpy(msg + 22, (char*)&device->mac, 6);
	device->send(device, msg, 42);*/
}
void netUnregisterDevice(NetNetworkDevice *device)
{
	// Remove device from the list
	uint32_t i;
	for (i = 0; i < devicecount; i++)
	{
		if (devices[i] == device)
		{
			memmove(&devices[i], &devices[i + 1], (devicecount - i - 1) * sizeof(NetNetworkDevice*));
			devicecount--;
			devices = realloc(devices, devicecount * sizeof(NetNetworkDevice*));
			break;
		}
	}
}

void netReceiveData(NetNetworkDevice *device, void *data, int length)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_NORMAL);
	lwip_device_receive(device, data, length);
	keSetExecutionLevel(oldlevel);
}

