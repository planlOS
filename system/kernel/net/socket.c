/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "net/socket.h"
#include "ke/errors.h"
#include "fs/request.h"
#include "ke/debug.h"
#include <string.h>
#include <stdlib.h>
#include "lwip/api.h"
#include "sys/syscall.h"

typedef struct
{
	FsFileSystem fs;
} NetSocketFS;

typedef struct NetSocket
{
	FsFile file;
	struct netconn *conn;
	char *buffer;
	int buffered;
	KeSpinlock lock;
} NetSocket;

static NetSocketFS fs;

static void netSocketCallback(struct netconn *conn, enum netconn_evt event, u16_t length)
{
	switch (event)
	{
		case NETCONN_EVT_SENDPLUS:
			conn->writebuffer -= length;
			break;
		default:
			return;
	}
}

static int netSocketClose(NetSocket *socket)
{
	keLockSpinlock(&socket->lock);
	while (socket->conn->writebuffer > 0);
	netconn_delete(socket->conn);
	free(socket);
	return 0;
}
static int netSocketIoctl(KeProcess *process, NetSocket *socket, int request, void *data)
{
	if (request == NET_SOCKET_IOCTL_BIND)
	{
		struct ip_addr addr;
		memcpy(&addr, data, 4);
		uint16_t port = *((uint16_t*)data + 2);
		keLockSpinlock(&socket->lock);
		if (netconn_bind(socket->conn, &addr, port) != ERR_OK)
		{
			keUnlockSpinlock(&socket->lock);
			kePrint("Could not bind socket\n");
			return -1;
		}
		keUnlockSpinlock(&socket->lock);
		kePrint("Bound socket to %d\n", port);
		return 0;
	}
	else if (request == NET_SOCKET_IOCTL_LISTEN)
	{
		uint32_t backlog = (uint32_t)data;
		keLockSpinlock(&socket->lock);
		if (netconn_listen_with_backlog(socket->conn, backlog) != ERR_OK)
		{
			keUnlockSpinlock(&socket->lock);
			kePrint("netconn_listen_with_backlog failed.\n");
			return -1;
		}
		keUnlockSpinlock(&socket->lock);
		kePrint("Listening on socket.\n");
		return 0;
	}
	else if (request == NET_SOCKET_IOCTL_ACCEPT)
	{
		keLockSpinlock(&socket->lock);
		struct netconn *newconn = netconn_accept(socket->conn);
		keUnlockSpinlock(&socket->lock);
		if (!newconn) return -1;
		// Create socket
		NetSocket *file = malloc(sizeof(NetSocket));
		memset(file, 0, sizeof(NetSocket));
		newconn->callback = netSocketCallback;
		file->conn = newconn;
		file->file.fs = &fs.fs;
		file->file.refcount = 1;
		// Create file handle
		FsFileHandle *fh = malloc(sizeof(FsFileHandle));
		memset(fh, 0, sizeof(FsFileHandle));
		fh->file = &file->file;
		if (!process)
		{
			// Called from the kernel, just return the file handle
			*((void**)data) = fh;
			return 0;
		}
		else
		{
			// Create file descriptor
			KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
			int fd = sysCreateFileDescriptor(process, fh);
			keSetExecutionLevel(oldlevel);
			return fd;
		}
	}
	else
		return -1;
}
static int netSocketRead(NetSocket *socket, char *data, int length)
{
	int read = 0;
	keLockSpinlock(&socket->lock);
	// Read from the buffer
	if (socket->buffered >= length)
	{
		memcpy(data, socket->buffer, length);
		if (socket->buffered != length)
		{
			char *newbuffer = malloc(socket->buffered - length);
			memcpy(newbuffer, socket->buffer + length, socket->buffered - length);
			free(socket->buffer);
			socket->buffer = newbuffer;
			socket->buffered -= length;
		}
		else
		{
			free(socket->buffer);
			socket->buffer = 0;
			socket->buffered = 0;
		}
		keUnlockSpinlock(&socket->lock);
		return length;
	}
	else
	{
		memcpy(data, socket->buffer, socket->buffered);
		read = socket->buffered;
		free(socket->buffer);
		socket->buffer = 0;
		socket->buffered = 0;
	}
	// Read from the socket directly
	while (1)
	{
		struct netbuf *buf = netconn_recv(socket->conn);
		if (!buf) continue;
		void *buffer = 0;
		uint16_t buffersize = 0;
		if (netbuf_data(buf, &buffer, &buffersize) != ERR_OK)
		{
			keUnlockSpinlock(&socket->lock);
			return read;
		}
		if (buffersize < length - read)
		{
			memcpy(data + read, buffer, buffersize);
			read += buffersize;
			netbuf_delete(buf);
		}
		else
		{
			memcpy(data + read, buffer, length - read);
			// Store part which was not yet read in the buffer
			socket->buffer = malloc(buffersize - (length - read));
			memcpy(socket->buffer, buffer + (length - read), buffersize - (length - read));
			socket->buffered = buffersize - (length - read);
			netbuf_delete(buf);
			keUnlockSpinlock(&socket->lock);
			return length;
		}
	}
}
static int netSocketWrite(NetSocket *socket, char *data, int length)
{
	// Send data
	kePrint("Sending %d bytes.\n", length);
	keLockSpinlock(&socket->lock);
	socket->conn->writebuffer += length;
	if (netconn_write(socket->conn, data, length, 0) != ERR_OK)
	{
		keUnlockSpinlock(&socket->lock);
		return -1;
	}
	while (socket->conn->writebuffer > 0);
	kePrint("Data sent.\n");
	keUnlockSpinlock(&socket->lock);
	return length;
}

static int netSocketRequest(struct FsFileSystem *fs, struct FsRequest *request)
{
	if (!request->file) return KE_ERROR_UNKNOWN;
	NetSocket *file = (NetSocket*)request->file;
	switch (request->type)
	{
		case FS_REQUEST_CLOSE:
			request->return_value = netSocketClose(file);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_READ:
			request->return_value = netSocketRead(file, request->buffer, request->bufferlength);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_WRITE:
			request->return_value = netSocketWrite(file, request->buffer, request->bufferlength);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_IOCTLSIZE:
			if (request->offset == NET_SOCKET_IOCTL_BIND)
			{
				request->return_value = 6;
				fsFinishRequest(request);
				return 0;
			}
			else if (request->offset == NET_SOCKET_IOCTL_LISTEN)
			{
				request->return_value = 0;
				fsFinishRequest(request);
				return 0;
			}
			else if (request->offset == NET_SOCKET_IOCTL_ACCEPT)
			{
				request->return_value = 6;
				fsFinishRequest(request);
				return 0;
			}
			else
				return KE_ERROR_UNKNOWN;
			case FS_REQUEST_IOCTL:
				request->return_value = netSocketIoctl(request->process,
					file, request->offset, request->buffer);
				fsFinishRequest(request);
				return 0;
		default:
			return KE_ERROR_UNKNOWN;
	}
}

void netInitSocket(void)
{
	memset(&fs, 0, sizeof(fs));
	fs.fs.query_request = netSocketRequest;
}

int netOpenSocket(FsFileHandle *socket, int type)
{
	// Create connection
	struct netconn *conn = 0;
	if (type == NET_SOCKET_STREAM)
		conn = netconn_new(NETCONN_TCP);
	else if (type == NET_SOCKET_DGRAM)
		conn = netconn_new(NETCONN_UDP);
	if (!conn)
	{
		kePrint("Could not create socket.\n");
		return KE_ERROR_UNKNOWN;
	}
	// Create socket
	NetSocket *file = malloc(sizeof(NetSocket));
	memset(file, 0, sizeof(NetSocket));
	keInitSpinlock(&file->lock);
	conn->callback = netSocketCallback;
	file->conn = conn;
	file->file.fs = &fs.fs;
	file->file.refcount = 1;
	socket->file = &file->file;
	return 0;
}

