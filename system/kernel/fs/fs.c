/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fs/fs.h"
#include "ke/errors.h"
#include "ke/cpu.h"
#include "ke/level.h"
#include "ke/debug.h"
#include "fs/request.h"
#include <stdlib.h>
#include <string.h>

typedef struct FsDriverEntry
{
	FsFileSystemDriver *driver;
	char *name;
} FsDriverEntry;

static FsDriverEntry *drivers = 0;
static uint32_t driver_count = 0;

static FsFileSystem **file_systems = 0;
static uint32_t fs_count = 0;

int fsInit(void)
{
	return KE_ERROR_NOTIMPLEMENTED;
}

int fsRegisterDriver(FsFileSystemDriver *driver, const char *name)
{
	// Add driver to driver list
	drivers = realloc(drivers, (driver_count + 1) * sizeof(FsDriverEntry));
	drivers[driver_count].driver = driver;
	drivers[driver_count].name = strdup(name);
	driver_count++;
	return 0;
}
int fsUnregisterDriver(FsFileSystemDriver *driver)
{
	return KE_ERROR_NOTIMPLEMENTED;
}
FsFileSystemDriver *fsGetDriver(const char *name)
{
	uint32_t i;
	for (i = 0; i < driver_count; i++)
	{
		if (!strcmp(drivers[i].name, name)) return drivers[i].driver;
	}
	return 0;
}
int fsMount(FsFileSystemDriver *driver, const char *path, const char *device,
	uint32_t flags)
{
	if (!driver) return KE_ERROR_UNKNOWN;
	// Call driver
	FsFileSystem *fs = driver->mount(driver, path, device, flags);
	if (!fs) return KE_ERROR_UNKNOWN;
	// Add file system to list
	file_systems = realloc(file_systems, (fs_count + 1) * sizeof(FsFileSystem*));
	file_systems[fs_count] = fs;
	fs_count++;
	return 0;
}
int fsUnmount(const char *path)
{
	return KE_ERROR_NOTIMPLEMENTED;
}

FsFileHandle *fsOpen(const char *filename, uint32_t mode)
{
	/*KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsOpen called at KE_LEVEL_HIGH!\n");
		return 0;
	}
	// Get file system from path
	uint32_t i = 0;
	FsFileSystem *fs = 0;
	uint32_t matched = 0;
	for (i = 0; i < fs_count; i++)
	{
		if (strlen(file_systems[i]->path) <= matched) continue;
		if (!strncmp(file_systems[i]->path, filename, strlen(file_systems[i]->path)))
		{
			matched = strlen(file_systems[i]->path);
			fs = file_systems[i];
		}
	}
	if (!fs) return 0;
	filename += matched;
	// Send open request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_OPEN;
	request.fs = fs;
	request.bufferlength = strlen(filename) + 1;
	request.buffer = (char*)filename;
	request.flags = mode;
	int status = fsCreateRequest(&request, 1);
	if (status) return 0;
	if (request.status || request.return_value) return 0;
	// Create file handle
	FsFileHandle *fh = malloc(sizeof(FsFileHandle));
	fh->file = request.file;
	fh->file->refcount = 1;
	fh->position = 0;
	fh->flags = mode;
	return fh;*/
	return fsProcessOpen(0, filename, mode);
}
FsFileHandle *fsProcessOpen(struct KeProcess *process, const char *filename, uint32_t mode)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsProcessOpen called at KE_LEVEL_HIGH!\n");
		return 0;
	}
	// Get file system from path
	uint32_t i = 0;
	FsFileSystem *fs = 0;
	uint32_t matched = 0;
	for (i = 0; i < fs_count; i++)
	{
		if (strlen(file_systems[i]->path) <= matched) continue;
		if (!strncmp(file_systems[i]->path, filename, strlen(file_systems[i]->path)))
		{
			matched = strlen(file_systems[i]->path);
			fs = file_systems[i];
		}
	}
	if (!fs) return 0;
	filename += matched;
	// Send open request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_OPEN;
	request.fs = fs;
	request.bufferlength = strlen(filename) + 1;
	request.buffer = (char*)filename;
	request.flags = mode;
	int status = fsCreateRequest(&request, 1, process);
	if (status) return 0;
	if (request.status || request.return_value) return 0;
	// Create file handle
	FsFileHandle *fh = malloc(sizeof(FsFileHandle));
	fh->file = request.file;
	fh->file->refcount = 1;
	fh->position = 0;
	fh->flags = mode;
	return fh;
}
int fsClose(FsFileHandle *file)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsClose called at KE_LEVEL_HIGH!\n");
		return KE_ERROR_UNKNOWN;
	}
	if (!file) return KE_ERROR_UNKNOWN;
	// Send close request
	file->file->refcount--;
	if (!file->file->refcount)
	{
		FsRequest request;
		memset(&request, 0, sizeof(request));
		request.type = FS_REQUEST_CLOSE;
		request.fs = file->file->fs;
		request.file = file->file;
		int status = fsCreateRequest(&request, 1, 0);
		if (status) return KE_ERROR_UNKNOWN;
		if (request.status || request.return_value) return KE_ERROR_UNKNOWN;
	}
	free(file);
	return 0;
}
int fsMknod(const char *filename, uint32_t mode)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsMknod called at KE_LEVEL_HIGH!\n");
		return -1;
	}
	// Get file system from path
	uint32_t i = 0;
	FsFileSystem *fs = 0;
	uint32_t matched = 0;
	for (i = 0; i < fs_count; i++)
	{
		if (strlen(file_systems[i]->path) <= matched) continue;
		if (!strncmp(file_systems[i]->path, filename, strlen(file_systems[i]->path)))
		{
			matched = strlen(file_systems[i]->path);
			fs = file_systems[i];
		}
	}
	if (!fs) return 0;
	filename += matched;
	// Send mknod request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_MKNOD;
	request.fs = fs;
	request.bufferlength = strlen(filename) + 1;
	request.buffer = (char*)filename;
	request.flags = mode;
	int status = fsCreateRequest(&request, 1, 0);
	if (status) return -1;
	if (request.status) return -1;
	if (request.return_value) return request.return_value;
	return 0;
	
}

int fsWrite(FsFileHandle *file, void *buffer, int size)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsWrite called at KE_LEVEL_HIGH!\n");
		return KE_ERROR_UNKNOWN;
	}
	if (!file) return -1;
	// Send close request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_WRITE;
	request.fs = file->file->fs;
	request.file = file->file;
	request.bufferlength = size;
	request.buffer = buffer;
	request.offset = file->position;
	int status = fsCreateRequest(&request, 1, 0);
	if (status) return -1;
	if (request.status) return -1;
	if (request.return_value > 0) file->position += request.return_value;
	return request.return_value;
}
int fsRead(FsFileHandle *file, void *buffer, int size, int blocking)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsRead called at KE_LEVEL_HIGH!\n");
		return KE_ERROR_UNKNOWN;
	}
	if (!file) return -1;
	// Send close request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_READ;
	request.fs = file->file->fs;
	request.file = file->file;
	request.bufferlength = size;
	request.buffer = buffer;
	request.offset = file->position;
	int status = fsCreateRequest(&request, 1, 0);
	if (status) return -1;
	if (request.status) return -1;
	if (request.return_value > 0) file->position += request.return_value;
	return request.return_value;
}
uint64_t fsSeek(FsFileHandle *file, int64_t offset, int whence)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsSeek called at KE_LEVEL_HIGH!\n");
		return 0;
	}
	if (!file) return -1;
	// Send seek request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_SEEK;
	request.fs = file->file->fs;
	request.file = file->file;
	if (whence == 1)
		request.offset = file->position + offset;
	else
		request.offset = offset;
	request.whence = whence;
	int status = fsCreateRequest(&request, 1, 0);
	if (status) return -1;
	if (request.status) return -1;
	file->position = request.offset;
	return request.offset;
}
int fsIOCTL(FsFileHandle *file, int requestno, ...)
{
	uintptr_t param = *(uintptr_t*)(&requestno + 1);
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsIOCTL called at KE_LEVEL_HIGH!\n");
		return -1;
	}
	// Send ioctl request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_IOCTL;
	request.fs = file->file->fs;
	request.file = file->file;
	request.offset = requestno;
	request.buffer = (void*)param;
	int status = fsCreateRequest(&request, 1, 0);
	if (status) return -1;
	if (request.status) return -1;
	return request.return_value;
}
int fsGetIOCTLSize(FsFileHandle *file, int requestno)
{
	KeExecLevel level = keGetExecutionLevel();
	if (level == KE_LEVEL_HIGH)
	{
		kePrint("fsGetIOCTLSize called at KE_LEVEL_HIGH!\n");
		return -1;
	}
	// Send ioctl size request
	FsRequest request;
	memset(&request, 0, sizeof(request));
	request.type = FS_REQUEST_IOCTLSIZE;
	request.fs = file->file->fs;
	request.file = file->file;
	request.offset = requestno;
	int status = fsCreateRequest(&request, 1, 0);
	if (status) return -1;
	if (request.status) return -1;
	return request.return_value;
}

