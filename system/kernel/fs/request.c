/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fs/request.h"
#include "ke/cpu.h"
#include "ke/errors.h"
#include "ke/debug.h"

int fsCreateRequest(FsRequest *request, int wait, KeProcess *process)
{
	if (!request->fs->query_request)
	{
		kePrint("Error: Filesystem %x does not support requests.\n", request->fs);
		return KE_ERROR_UNKNOWN;
	}
	if (process)
	{
		request->process = process;
	}
	else
	{
		if (keGetCurrentCPU()->currentthread)
			request->process = keGetCurrentCPU()->currentthread->process;
		else
			request->process = 0;
	}
	int status = request->fs->query_request(request->fs, request);
	if (status) return status;
	if (wait) return fsWaitRequest(request);
	return 0;
}
int fsWaitRequest(FsRequest *request)
{
	// TODO: Thread safety
	if (request->finished) return 0;
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	request->caller = keGetCurrentCPU()->currentthread;
	request->caller->status = KE_THREAD_FSREQUEST;
	// Schedule out of the thread
	asm volatile("int $0x32");
	keSetExecutionLevel(oldlevel);
	return 0;
}
int fsFinishRequest(FsRequest *request)
{
	request->finished = 1;
	if (request->caller)
	{
		request->caller->status = KE_THREAD_RUNNING;
	}
	return 0;
}

