/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fs/devfs.h"
#include "fs/request.h"
#include "ke/errors.h"
#include "ke/debug.h"
#include <string.h>
#include <stdlib.h>

typedef struct
{
	FsFile file;
	uint32_t index;
} KeDevFSFile;

static FsFileSystemDriver devfs;
static FsFileSystem fs;

static FsDeviceFile **devfsfiles = 0;
static FsFile **files = 0;
static uint32_t devfsfile_count = 0;

static FsFile *fsDevFSOpen(FsFileSystem *fs, const char *path)
{
	// Search for file
	uint32_t fileindex = 0xFFFFFFFF;
	uint32_t i;
	for (i = 0; i < devfsfile_count; i++)
	{
		if (!strcmp(devfsfiles[i]->path, path))
		{
			fileindex = i;
			break;
		}
	}
	if (fileindex == 0xFFFFFFFF) return 0;
	// Open existing file
	if (files[fileindex]) return files[fileindex];
	// Create file structure
	KeDevFSFile *file = malloc(sizeof(KeDevFSFile));
	files[fileindex] = &file->file;
	memset(file, 0, sizeof(FsFile));
	file->file.fs = fs;
	file->index = fileindex;
	return files[fileindex];
}

static FsFileSystem *fsDevFSMount(struct FsFileSystemDriver *driver,
	const char *path, const char *device, uint32_t flags)
{
	if (fs.path) return 0;
	fs.path = strdup(path);
	return &fs;
}
static int fsDevFSRequest(struct FsFileSystem *fs, struct FsRequest *request)
{
	switch (request->type)
	{
		// Open/close files at once
		case FS_REQUEST_OPEN:
			{
				request->file = fsDevFSOpen(fs, request->buffer);
				if (!request->file)
				{
					request->status = KE_ERROR_UNKNOWN;
					request->return_value = -1;
				}
				else
				{
					uint32_t fileindex = ((KeDevFSFile*)request->file)->index;
					devfsfiles[fileindex]->query_request(devfsfiles[fileindex], request);
				}
				fsFinishRequest(request);
			}
			return 0;
		case FS_REQUEST_CLOSE:
			{
				uint32_t fileindex = ((KeDevFSFile*)request->file)->index;
				devfsfiles[fileindex]->query_request(devfsfiles[fileindex], request);
				fsFinishRequest(request);
			}
			return 0;
			// Pass other requests to the driver
		default:
			{
				uint32_t fileindex = ((KeDevFSFile*)request->file)->index;
				if (!devfsfiles[fileindex]->query_request)
				{
					kePrint("devfs: No request handler.\n");
					return KE_ERROR_UNKNOWN;
				}
				return devfsfiles[fileindex]->query_request(devfsfiles[fileindex], request);
			}
	}
}

static int fsDevFSUnmount(FsFileSystem *fs)
{
	free(fs->path);
	fs->path = 0;
	return 0;
}

int fsInitDevFS(void)
{
	memset(&fs, 0, sizeof(fs));
	fs.driver = &devfs;
	fs.unmount = fsDevFSUnmount;
	fs.query_request = fsDevFSRequest;
	
	// Register file system driver
	devfs.flags = FS_DRIVER_SINGLE | FS_DRIVER_NOMOUNT | FS_DRIVER_NODATA;
	devfs.mount = fsDevFSMount;
	fsRegisterDriver(&devfs, "devfs");
	return 0;
}

int fsCreateDeviceFile(FsDeviceFile *file)
{
	// Add file to file list
	devfsfiles = realloc(devfsfiles, (devfsfile_count + 1) * sizeof(FsDeviceFile*));
	files = realloc(files, (devfsfile_count + 1) * sizeof(FsFile*));
	devfsfiles[devfsfile_count] = file;
	files[devfsfile_count] = 0;
	devfsfile_count++;
	kePrint("Created device: %s\n", file->path);
	return 0;
}
int fsDestroyDeviceFile(FsDeviceFile *file)
{
	// Search for file
	uint32_t fileindex = 0xFFFFFFFF;
	uint32_t i;
	for (i = 0; i < devfsfile_count; i++)
	{
		if (devfsfiles[i] == file)
		{
			fileindex = i;
			break;
		}
	}
	if (fileindex == 0xFFFFFFFF) return KE_ERROR_UNKNOWN;
	// Close open file handles
	// Remove from file list
	memmove(devfsfiles + fileindex, devfsfiles + fileindex + 1,
		(devfsfile_count - fileindex - 1) * sizeof(FsDeviceFile*));
	devfsfiles = realloc(devfsfiles, (devfsfile_count - 1) * sizeof(FsDeviceFile*));
	memmove(files + fileindex, files + i + 1,
		(devfsfile_count - fileindex - 1) * sizeof(FsFile*));
	files = realloc(files, (devfsfile_count - 1) * sizeof(FsFile*));
	devfsfile_count--;
	for (i = fileindex; i < devfsfile_count; i++)
	{
		((KeDevFSFile*)files[i])->index = i;
	}
	return 0;
}

