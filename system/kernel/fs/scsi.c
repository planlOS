/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fs/scsi.h"
#include "ke/errors.h"
#include <stdlib.h>

static FsSCSIDevice **devices = 0;
static uint32_t device_count = 0;

int fsRegisterSCSIDevice(FsSCSIDevice *device)
{
	devices = realloc(devices, sizeof(FsSCSIDevice*) * (device_count + 1));
	devices[device_count] = device;
	device_count++;
	return 0;
}
int fsRemoveSCSIDevice(FsSCSIDevice *device)
{
	return KE_ERROR_NOTIMPLEMENTED;
}

uint32_t fsGetSCSIDeviceCount(void)
{
	return device_count;
}
FsSCSIDevice *fsOpenSCSIDevice(uint32_t index)
{
	if (index >= device_count) return 0;
	if (keTryLockSpinlock(&devices[index]->opened)) return 0;
	return devices[index];
}
void fsCloseSCSIDevice(FsSCSIDevice *device)
{
	int index = -1;
	uint32_t i;
	for (i = 0; i < device_count; i++)
	{
		if (devices[i] == device)
		{
			index = i;
			break;
		}
	}
	if (index == -1) return;
	keUnlockSpinlock(&devices[index]->opened);
}

