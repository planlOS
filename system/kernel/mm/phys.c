/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "mm/memory.h"
#include "ke/errors.h"
#include "ke/debug.h"
#include "ke/elf.h"

// 0 = available, 1 = used

static uint32_t phys_mem[0x8000];
static uint32_t phys_mem_avail[0x8000];

static uint32_t free_pages = 0;

static void mmMarkPhysPage(uint32_t page, int used)
{
	// Abort if page is a special page
	uint32_t tmp = phys_mem_avail[page / 32];
	if (tmp & (1 << (31 - (page % 32)))) return;
	// Mark single bit in bitmap
	tmp = phys_mem[page / 32];
	if (used)
	{
		tmp |= 1 << (31 - (page % 32));
		free_pages--;
	}
	else
	{
		tmp &= ~((uint32_t)(1 << (31 - (page % 32))));
		free_pages++;
	}
	phys_mem[page / 32] = tmp;
}
static void mmMarkPhysPageRange(uint32_t start, uint32_t end, int used)
{
	uint32_t count = (end - start + 4095) / 0x1000;
	uint32_t i;
	for (i = 0; i < count; ++i)
	{
		mmMarkPhysPage(start / 0x1000 + i, used);
	}
}

static void mmMarkPhysPageAvailable(uintptr_t addr, uint32_t size)
{
	uint32_t i;
	for (i = 0; i < size; i++)
	{
		uint32_t tmp = phys_mem_avail[(addr + i) / 32];
		tmp &= ~((uint32_t)(1 << (31 - ((addr + i) % 32))));
		phys_mem_avail[(addr + i) / 32] = tmp;
	}
	free_pages += size;
}

int mmInitPhysicalMemory(MultibootInfo *mbinfo, uintptr_t kernel_begin,
	uintptr_t kernel_end)
{
	// Mark everything as not available
	uint32_t i;
	for (i = 0; i < 0x8000; i++)
		phys_mem_avail[i] = 0xFFFFFFFF;
	
	// Mark areas in memory map as usable
	if (CHECK_FLAG(mbinfo->flags, 6))
	{
		MbMemoryMap *mmap = (MbMemoryMap*)mbinfo->mmapaddr;
		for (mmap = (MbMemoryMap*)mbinfo->mmapaddr;
			(uintptr_t)mmap < mbinfo->mmapaddr + mbinfo->mmaplength;
			mmap = (MbMemoryMap*)((uint32_t)mmap + mmap->size + sizeof (mmap->size)))
		{
			if (mmap->type == 1)
			{
				// Mark free pages
				uint32_t start = (mmap->baseaddrlow + 4095) / 4096;
				uint32_t end = (mmap->baseaddrlow + mmap->lengthlow) / 4096;
				uint32_t size = end - start;
				mmMarkPhysPageAvailable(start, size);
			}
		}
	}
	else
	{
		return KE_ERROR_UNKNOWN;
	}
	// Mark used pages
	mmMarkPhysPageRange(kernel_begin - 0xC0000000, kernel_end - 0xC0000000, 1);
	mmMarkPhysPageRange((uintptr_t)mbinfo & ~0xFFF, ((uintptr_t)mbinfo & ~0xFFF) + 4096, 1);
	uintptr_t sec_addr = mbinfo->u.elfsections.addr;
	uintptr_t sec_size = mbinfo->u.elfsections.size;
	uint32_t sec_count = mbinfo->u.elfsections.num;
	mmMarkPhysPageRange(sec_addr & ~0xFFF, (sec_addr + sec_size + 4095) & ~0xFFF, 1);
	ElfSectionHeader *sheaders = (ElfSectionHeader*)sec_addr;
	for (i = 0; i < sec_count; i++)
	{
		if (sheaders[i].sh_addr < 0xC0000000)
		{
			mmMarkPhysPageRange(sheaders[i].sh_addr & ~0xFFF, (sheaders[i].sh_addr + sheaders[i].sh_size + 4095) & ~0xFFF, 1);
		}
	}
	if (CHECK_FLAG(mbinfo->flags, 3))
	{
		MbModule *mod = (MbModule*)mbinfo->modaddr;
		mmMarkPhysPageRange((uintptr_t)mod & ~0xFFF, ((uintptr_t)mod + mbinfo->modcount * sizeof(MbModule) + 4095) & ~0xFFF, 1);
		mmMarkPhysPageRange((uintptr_t)mod->string & ~0xFFF, ((uintptr_t)mod->string & ~0xFFF) + 4096, 1);
		for (i = 0; i < mbinfo->modcount; i++)
		{
			mmMarkPhysPageRange(mod->start, (mod->end + 4095) & ~0xFFF, 1);
			mod++;
		}
	}
	else
	{
		kePrint("Error: No module info available.\n");
		while (1);
	}
	
	phys_mem_avail[0] = phys_mem_avail[0] | 0xC0000000;
	return 0;
}

static uintptr_t mmSearchPhysicalMemoryLow(uint32_t size);

static uintptr_t mmSearchPhysicalMemory(uint32_t size)
{
	// TODO: We don't support such large continuous parts of memory. Do we need
	// that?
	if (size > 32) return 0;
	
	// Create mask used for comparisons
	uint32_t mask = 0;
	uint32_t i;
	for (i = 0; i < size; i++)
	{
		mask |= 1 << (31 - i);
	}
	
	// Search for free page
	for (i = 128; i < 0x8000; i++)
	{
		uint32_t avail = phys_mem_avail[i];
		if (avail == 0xFFFFFFFF) continue;
		uint32_t free = phys_mem[i];
		if (free == 0xFFFFFFFF) continue;
		uint32_t j;
		for (j = 0; j <= 32 - size; ++j)
		{
			if (((free & (mask >> j)) == 0) && ((avail & (mask >> j)) == 0))
			{
				return i * 32 * 0x1000 + j * 0x1000;
			}
		}
	}
	
	return mmSearchPhysicalMemoryLow(size);
}
static uintptr_t mmSearchPhysicalMemoryLow(uint32_t size)
{
	// TODO: We don't support such large continuous parts of memory. Do we need
	// that?
	if (size > 32) return 0;
	
	// Create mask used for comparisons
	uint32_t mask = 0;
	uint32_t i;
	for (i = 0; i < size; i++)
	{
		mask |= 1 << (31 - i);
	}
	
	// Search for free page
	for (i = 0; i < 128; ++i)
	{
		uint32_t avail = phys_mem_avail[i];
		if (avail == 0xFFFFFFFF) continue;
		uint32_t free = phys_mem[i];
		if (free == 0xFFFFFFFF) continue;
		uint32_t j;
		for (j = 0; j <= 32 - size; ++j)
		{
			if (((free & (mask >> j)) == 0) && ((avail & (mask >> j)) == 0))
			{
				return i * 32 * 0x1000 + j * 0x1000;
			}
		}
	}
	
	return 0;
}

uintptr_t mmAllocPhysicalMemory(uint32_t flags, uintptr_t phys_addr,
	uint32_t size)
{
	// Get physical address
	uintptr_t addr = 0;
	if (flags & MM_MEMORY_ALLOC_PHYS)
	{
		addr = phys_addr;
	}
	else if (flags & MM_MEMORY_ALLOC_DMA)
	{
		addr = mmSearchPhysicalMemoryLow(size / 0x1000);
	}
	else
	{
		addr = mmSearchPhysicalMemory(size / 0x1000);
	}
	if (addr == 0) return 0;
	
	// Mark pages as used
	uint32_t i;
	for (i = 0; i < size / 0x1000; i++)
	{
		mmMarkPhysPage(addr / 0x1000 + i, 1);
	}
	return addr;
}
void mmFreePhysicalMemory(uintptr_t addr, uint32_t size)
{
	addr &= ~0xFFF;
	size &= ~0xFFF;
	uint32_t i;
	for (i = 0; i < size / 0x1000; i++)
	{
		mmMarkPhysPage(addr / 0x1000 + i, 0);
	}
}

uint32_t mmGetPhysPageCount(void)
{
	return free_pages;
}

