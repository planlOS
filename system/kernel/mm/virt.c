/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "mm/memory.h"
#include "ke/errors.h"
#include "ke/debug.h"
#include <string.h>

uint32_t kernel_directory[1024] __attribute__ ((aligned (4096)));

#define MM_KERNEL_PAGE_TABLES 0xF0000000

static int mmInitMemoryRange(uintptr_t phys, uintptr_t virt, uint32_t size, uint32_t mode)
{
	if (phys + size > 0x400000) return KE_ERROR_INVALIDADDR;
	uint32_t i;
	for (i = 0; i < (size + 4095) / 0x1000; ++i)
	{
		uintptr_t target = virt + i * 0x1000;
		uintptr_t src = phys + i * 0x1000;
		// Write directly onto the page table using the physical address
		uint32_t *page_table = (uint32_t*)(kernel_directory[target >> 22] & ~0xFFF);
		// TODO: Access mode
		page_table[(target / 0x1000) % 1024] = src | 0x3;
	}
	return 0;
}

int mmInitVirtualMemory(MultibootInfo *mbinfo, uintptr_t kernel_begin,
	uintptr_t kernel_end)
{
	// Zero the page directory
	int i;
	for (i = 0; i < 1024; i++)
	{
		kernel_directory[i] = 0;
	}
	
	// Map the directory onto itself
	kernel_directory[MM_KERNEL_PAGE_TABLES >> 22] = ((uintptr_t)kernel_directory - MM_KERNEL_BASE) | 0x3;
	
	// Initialize page tables
	// TODO: We waste way too much memory here
	uintptr_t page_table_phys = mmAllocPhysicalMemory(MM_MEMORY_ALLOC_DMA, 0, 0x1000);
	memset((void*)(page_table_phys + MM_KERNEL_BASE), 0, 0x1000);
	kernel_directory[0] = page_table_phys | 0x3;
	for (i = 768; i < 1024; i++)
	{
		if (kernel_directory[i] == 0)
		{
			page_table_phys = mmAllocPhysicalMemory(MM_MEMORY_ALLOC_DMA, 0, 0x1000);
			memset((void*)(page_table_phys + MM_KERNEL_BASE), 0, 0x1000);
			kernel_directory[i] = page_table_phys | 0x7;
		}
	}
	kernel_directory[MM_KERNEL_PAGE_TABLES >> 22] = ((uintptr_t)kernel_directory - MM_KERNEL_BASE) | 0x3;

	// Map kernel image
	// TODO: Don't allow patching kernel image
	uint32_t error = mmInitMemoryRange(kernel_begin - MM_KERNEL_BASE, kernel_begin,
		(kernel_end - kernel_begin + 4095) & ~0xFFF,
		MM_MAP_READ | MM_MAP_WRITE | MM_MAP_EXECUTE);
	if (error) return error;
	error = mmInitMemoryRange(0xB8000, 0xC00B8000,
		0x4000,
		MM_MAP_READ | MM_MAP_WRITE);
	if (error) return error;

	// Enable paging
	asm volatile("mov %%eax, %%cr3" :: "a" ((uintptr_t)kernel_directory - MM_KERNEL_BASE));
	return 0;
}

int mmCreateAddressSpace(MmAddressSpace *addrspace)
{
	// Allocate memory
	addrspace->phys_addr = mmAllocPhysicalMemory(0, 0, 0x1000);
	if (!addrspace->phys_addr) return KE_ERROR_OOM;
	// Map page directory into kernel space
	uintptr_t kernel_addr = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
	if (!kernel_addr) return KE_ERROR_KERNEL_OOM;
	mmMapKernelMemory(addrspace->phys_addr, kernel_addr, MM_MAP_READ | MM_MAP_WRITE);
	addrspace->virt_addr = (uint32_t*)kernel_addr;
	memset(addrspace->virt_addr, 0, 768 * 4);
	memcpy(addrspace->virt_addr + 768, kernel_directory + 768, 256 * 4);
	return 0;
}
static int mmClonePageTable(uintptr_t *dest, uintptr_t *src)
{
	uint32_t i;
	for (i = 0; i < 1024; i++)
	{
		uintptr_t page_phys = src[i] & ~0xFFF;
		uintptr_t page_new_phys = 0;
		uintptr_t page = 0;
		uintptr_t page_new = 0;
		if (page_phys)
		{
			// Map page
			page = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
			if (!page) return KE_ERROR_KERNEL_OOM;
			mmMapKernelMemory(page_phys, page, MM_MAP_READ | MM_MAP_WRITE);
			// Create new page
			page_new_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
			if (!page_new_phys) return KE_ERROR_OOM;
			dest[i] = page_new_phys | (src[i] & 0xFFF);
			// Map new page
			page_new = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
			if (!page_new) return KE_ERROR_KERNEL_OOM;
			mmMapKernelMemory(page_new_phys, page_new, MM_MAP_READ | MM_MAP_WRITE);
			// Copy page
			memcpy((void*)page_new, (void*)page, 0x1000);
			// Unmap pages
			mmMapKernelMemory(0, page_new, 0);
			mmMapKernelMemory(0, page, 0);
		}
	}
	return 0;
}
int mmCloneAddressSpace(MmAddressSpace *dest, MmAddressSpace *src)
{
	uint32_t i;
	for (i = 0; i < 768; i++)
	{
		uintptr_t page_table_phys = src->virt_addr[i] & ~0xFFF;
		uintptr_t page_table_new_phys = 0;
		uintptr_t page_table = 0;
		uintptr_t page_table_new = 0;
		if (page_table_phys)
		{
			// Map page table
			page_table = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
			if (!page_table) return KE_ERROR_KERNEL_OOM;
			mmMapKernelMemory(page_table_phys, page_table, MM_MAP_READ | MM_MAP_WRITE);
			// Create new page table
			page_table_new_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
			if (!page_table_new_phys) return KE_ERROR_OOM;
			dest->virt_addr[i] = page_table_new_phys | 0x7;
			// Map new page table
			page_table_new = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
			if (!page_table_new) return KE_ERROR_KERNEL_OOM;
			mmMapKernelMemory(page_table_new_phys, page_table_new, MM_MAP_READ | MM_MAP_WRITE);
			// Clone page table
			memset((void*)page_table_new, 0, 0x1000);
			int status = mmClonePageTable((uintptr_t*)page_table_new, (uintptr_t*)page_table);
			// Unmap tables
			mmMapKernelMemory(0, page_table_new, 0);
			mmMapKernelMemory(0, page_table, 0);
			if (status) return status;
		}
	}
	return 0;
}
int mmDestroyAddressSpace(MmAddressSpace *addrspace)
{
	mmClearAddressSpace(addrspace);
	mmMapKernelMemory(0, (uintptr_t)addrspace->virt_addr, 0);
	mmFreePhysicalMemory(addrspace->phys_addr, 0x1000);
	return 0;
}
static void mmClearPageTable(uintptr_t *page_table)
{
	uint32_t i;
	for (i = 0; i < 1024; i++)
	{
		uintptr_t page_phys = page_table[i] & ~0xFFF;
		if (page_phys)
			mmFreePhysicalMemory(page_phys, 0x1000);
	}
}
int mmClearAddressSpace(MmAddressSpace *addrspace)
{
	// Get addr which we can use to map the page table
	uintptr_t page_table = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
	if (!page_table) return KE_ERROR_KERNEL_OOM;
	// Loop through page tables
	uint32_t i;
	for (i = 0; i < 768; i++)
	{
		uintptr_t page_table_phys = addrspace->virt_addr[i] & ~0xFFF;
		if (page_table_phys)
		{
			// Map page table
			mmMapKernelMemory(page_table_phys, page_table, MM_MAP_READ | MM_MAP_WRITE);
			// Clear page table
			mmClearPageTable((uintptr_t*)page_table);
			// Unmap page table
			mmMapKernelMemory(0, page_table, 0);
			mmFreePhysicalMemory(page_table_phys, 0x1000);
		}
	}
	memset(addrspace->virt_addr, 0, 768 * sizeof(uintptr_t));
	return 0;
}

int mmMapMemory(MmAddressSpace *addrspace, uintptr_t phys, uintptr_t virt,
	uint32_t mode)
{
	virt &= ~0xFFF;
	phys &= ~0xFFF;
	// Get page table
	uintptr_t page_table_phys = addrspace->virt_addr[virt >> 22];
	uintptr_t page_table = 0;
	if (!page_table_phys)
	{
		// No page table present, create one
		page_table_phys = mmAllocPhysicalMemory(0, 0, 0x1000);
		if (!page_table_phys) return KE_ERROR_OOM;
		addrspace->virt_addr[virt >> 22] = page_table_phys | 0x7;
		page_table = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
		if (!page_table) return KE_ERROR_KERNEL_OOM;
		mmMapKernelMemory(page_table_phys, page_table, MM_MAP_READ | MM_MAP_WRITE);
		memset((void*)page_table, 0, 0x1000);
	}
	else
	{
		// Map page table
		page_table = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
		if (!page_table) return KE_ERROR_KERNEL_OOM;
		mmMapKernelMemory(page_table_phys, page_table, MM_MAP_READ | MM_MAP_WRITE);
	}
	uint32_t table_index = (virt / 0x1000) % 1024;
	if (((uintptr_t*)page_table)[table_index])
	{
		kePrint("Warning: %x already mapped!\n", virt);
	}
	// Set flags
	phys |= 0x5; // Present | User
	if (mode & MM_MAP_WRITE) phys |= 0x2;
	if (mode & MM_MAP_UNCACHEABLE) phys |= 0x18;
	// Fill table entry
	((uintptr_t*)page_table)[table_index] = phys;
	
	// Unmap page table
	mmMapKernelMemory(0, page_table, 0);
	keSyncVirtMemory();
	return KE_ERROR_NOTIMPLEMENTED;
}
int mmMapKernelMemory(uintptr_t phys, uintptr_t virt,
	uint32_t mode)
{
	virt &= ~0xFFF;
	phys &= ~0xFFF;
	uint32_t pgdir_index = virt >> 22;
	uint32_t pgtab_index = (virt / 0x1000) % 1024;
	uint32_t *page_tables = (uint32_t*)MM_KERNEL_PAGE_TABLES;
	phys |= 0x1; // Present
	if (mode & MM_MAP_USER) phys |= 0x4;
	if (mode & MM_MAP_WRITE) phys |= 0x2;
	if (mode & MM_MAP_UNCACHEABLE) phys |= 0x18;
	if (mode)
		*(page_tables + pgdir_index * 1024 + pgtab_index) = phys;
	else
		*(page_tables + pgdir_index * 1024 + pgtab_index) = 0;
	asm volatile("invlpg %0" :: "m" (*(char*)virt));
	keSyncVirtMemory();
	return 0;
}
uintptr_t mmGetPhysAddress(MmAddressSpace *addrspace, uintptr_t virt)
{
	uint32_t pgdir_index = virt >> 22;
	uint32_t pgtab_index = (virt / 0x1000) % 1024;
	// Get page table
	uintptr_t page_table_phys = addrspace->virt_addr[pgdir_index];
	if (!page_table_phys) return 0;
	// Map page table
	uintptr_t page_table = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
	if (!page_table) return 0;
	mmMapKernelMemory(page_table_phys, page_table, MM_MAP_READ | MM_MAP_WRITE);
	uint32_t entry = ((uintptr_t*)page_table)[pgtab_index];
	// Unmap page table
	mmMapKernelMemory(0, page_table, 0);
	return entry & ~0xFFF;
}
uintptr_t mmKernelGetPhysAddress(uintptr_t virt)
{
	if (virt < MM_KERNEL_BASE) return 0;
	uint32_t pgdir_index = virt >> 22;
	uint32_t pgtab_index = (virt / 0x1000) % 1024;
	uint32_t *page_tables = (uint32_t*)MM_KERNEL_PAGE_TABLES;
	return *(page_tables + pgdir_index * 1024 + pgtab_index) & ~0xFFF;
}
static int mmPageIsFree(MmAddressSpace *addrspace, uintptr_t addr)
{
	uint32_t pgdir_index = addr >> 22;
	uint32_t pgtab_index = (addr / 0x1000) % 1024;
	// Get page table
	uintptr_t page_table_phys = addrspace->virt_addr[pgdir_index];
	if (!page_table_phys) return 1;
	// Map page table
	uintptr_t page_table = mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, 0x1000);
	if (!page_table) return KE_ERROR_KERNEL_OOM;
	mmMapKernelMemory(page_table_phys, page_table, MM_MAP_READ | MM_MAP_WRITE);
	uint32_t entry = ((uintptr_t*)page_table)[pgtab_index];
	// Unmap page table
	mmMapKernelMemory(0, page_table, 0);
	return entry == 0;
}
uintptr_t mmFindFreePages(MmAddressSpace *addrspace, uintptr_t max, uintptr_t min,
	int downwards, uint32_t size)
{
	// TODO: This is horribly inefficient
	// Check arguments
	if (max < min) return KE_ERROR_INVALID_ARG;
	if (max == min) return 0;
	max &= ~0xFFF;
	min &= ~0xFFF;
	uint32_t page;
	uint32_t pagecount = 0;
	
	// Search for free pages
	if (downwards)
	{
		for (page = max; page >= min; page -= 0x1000)
		{
			if (mmPageIsFree(addrspace, page))
			{
				pagecount++;
				if (pagecount == size) return page;
			}
			else
			{
				pagecount = 0;
			}
			if (page == MM_MIN_USER_PAGE) break;
		}
	}
	else
	{
		for (page = min; page <= max; page += 0x1000)
		{
			if (mmPageIsFree(addrspace, page))
			{
				pagecount++;
				if (pagecount == size) return page - pagecount * 0x1000 + 0x1000;
			}
			else
			{
				pagecount = 0;
			}
			if (page == MM_MAX_USER_PAGE) break;
		}
	}
	return 0;
}
int mmKernelPageIsFree(uintptr_t addr)
{
	uint32_t pgdir_index = addr >> 22;
	uint32_t pgtab_index = (addr / 0x1000) % 1024;
	uint32_t *page_tables = (uint32_t*)MM_KERNEL_PAGE_TABLES;
	return *(page_tables + pgdir_index * 1024 + pgtab_index) == 0;
}
uintptr_t mmFindFreeKernelPages(uintptr_t max, uintptr_t min,
	int downwards, uint32_t size)
{
	size = (size + 0xFFF) / 0x1000;
	// Check arguments
	if (max < min) return KE_ERROR_INVALID_ARG;
	if (max == min) return 0;
	max &= ~0xFFF;
	min &= ~0xFFF;
	uint32_t page;
	uint32_t pagecount = 0;
	
	// Search for free pages
	if (downwards)
	{
		for (page = max; page >= min; page -= 0x1000)
		{
			if (mmKernelPageIsFree(page))
			{
				pagecount++;
				if (pagecount == size + 1) return page;
			}
			else
			{
				pagecount = 0;
			}
			if (page == MM_MIN_KERNEL_PAGE) break;
		}
	}
	else
	{
		for (page = min; page <= max; page += 0x1000)
		{
			if (mmKernelPageIsFree(page))
			{
				pagecount++;
				if (pagecount == size) return page - pagecount * 0x1000 + 0x1000;
			}
			else
			{
				pagecount = 0;
			}
			if (page == MM_MAX_KERNEL_PAGE) break;
		}
	}
	return 0;
}

void keSyncVirtMemory(void)
{
	// TODO
}

