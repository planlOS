/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file net.h
 * Basic network functions.
 */

#ifndef NET_NET_H_INCLUDED
#define NET_NET_H_INCLUDED

#include <stdint.h>
#include <ke/spinlock.h>

/**
 * Information about a network device
 */
typedef struct NetNetworkDevice
{
	/**
	 * Name of the device. Filled in by netRegisterDevice.
	 */
	char *name;
	/**
	 * Device specific send function. Must be filled in by the driver.
	 */
	int (*send)(struct NetNetworkDevice *device, void *data, int length);
	
	/**
	 * IP address of the device.
	 */
	uint8_t ip[4];
	/**
	 * MAC address of the device. Must be filled in by the driver.
	 */
	uint64_t mac;
	
	/**
	 * Pointer to the lwip device.
	 */
	void *lwipdev;
} NetNetworkDevice;

/**
 * Converts a 16bit integer from network byte order to native byte order and
 * vice versa.
 */
uint16_t netConvert16(uint16_t value);
/**
 * Converts a 32bit integer from network byte order to native byte order and
 * vice versa.
 */
uint32_t netConvert32(uint32_t value);
/**
 * Converts a 64bit integer from network byte order to native byte order and
 * vice versa.
 */
uint64_t netConvert64(uint64_t value);

/**
 * Initializes the networking system.
 */
void netInit(void);

/**
 * Registers a new network device.
 * \param device Device to be registered.
 * \param name First part of the name of the device (e.g. "eth" for ethernet
 * devices).
 */
void netRegisterDevice(NetNetworkDevice *device, char *name);
/**
 * Removes a network device from the system.
 */
void netUnregisterDevice(NetNetworkDevice *device);

/**
 * Callback for network devices after they have received data.
 */
void netReceiveData(NetNetworkDevice *device, void *data, int length);

#endif

