/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file socket.h
 * Socket file system functions.
 */

#ifndef NET_SOCKET_H_INCLUDED
#define NET_SOCKET_H_INCLUDED

#include "fs/fs.h"

#define NET_SOCKET_STREAM 1
#define NET_SOCKET_DGRAM 2

#define NET_SOCKET_IOCTL_SETSOCKOPT 1
#define NET_SOCKET_IOCTL_BIND 2
#define NET_SOCKET_IOCTL_LISTEN 3
#define NET_SOCKET_IOCTL_ACCEPT 4

/**
 * Initializes the socket file system.
 */
void netInitSocket(void);

/**
 * Opens a new socket and writes the file handle data to socket.
 */
int netOpenSocket(FsFileHandle *socket, int type);

#endif

