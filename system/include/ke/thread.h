/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file thread.h
 * Thread management functions
 */

#ifndef KE_THREAD_H_INCLUDED
#define KE_THREAD_H_INCLUDED

#include "ke/process.h"

#define KE_THREAD_RUNNING 0
#define KE_THREAD_SLEEP 1
#define KE_THREAD_FSREQUEST 2
#define KE_THREAD_KILLED 3
#define KE_THREAD_WAIT 4
#define KE_THREAD_INUSE 5

/**
 * Structure containing the information about a thread.
 */
typedef struct KeThread
{
	/**
	 * Bottom address of the kernel stack
	 * \todo This is not yet freed when the kernel is destroyed.
	 */
	uintptr_t kernelstack_bottom;
	/**
	 * Current kernel stack top
	 */
	uintptr_t kernelstack;
	/**
	 * Bottom address of the user stack (if allocated).
	 */
	uintptr_t userstack;
	/**
	 * Size of the user stack.
	 */
	uintptr_t userstack_size;
	
	/**
	 * User stack frame on the kernel stack, 0, if this is a kernel thread.
	 */
	uintptr_t userframe;
	
	/**
	 * Current status of the thread.
	 */
	uint32_t status;
	/**
	 * Timeout variable used for sleeping/waiting etc.
	 */
	uint64_t timeout;
	
	/**
	 * Process which this thread belongs to.
	 */
	KeProcess *process;
	
	/**
	 * If this lock is locked, the thread is currently being executed on a CPU.
	 */
	KeSpinlock active;
} KeThread;

/**
 * Creates a user thread in the given process.
 * \param process Process into which the thread will be inserted.
 * \param entry Entry point for the thread
 * \param paramcount Number of parameters passed to the entry function. The
 * parameters are passed after this number.
 */
KeThread *keCreateThread(KeProcess *process, uintptr_t entry, uint32_t paramcount, ...);
/**
 * Stops and destroys a thread.
 */
int keDestroyThread(KeThread *thread);

/**
 * Clones a thread in another process. No memory is copied by this, only the raw
 * process is copied using an existing user stack.
 * \param process Process for the new thread.
 * \param thread Thread in another process which is cloned.
 * \param stack kernel stack of the thread being cloned.
 */
KeThread *keCloneThread(KeProcess *process, KeThread *thread, uintptr_t stack);

/**
 * Creates a new kernel thread.
 * \param entry Pointer to the entry function.
 * \param paramcount Number of parameters passed to the function.
 */
KeThread *keCreateKernelThread(uintptr_t entry, uint32_t paramcount, ...);
/**
 * Creates a new idle thread. The idle thread just executes
 * "while (1) asm("hlt");".
 */
KeThread *keCreateIdleThread(void);

/**
 * Sets a syscall parameter in the given thread.
 * \param thread Thread to be modified.
 * \param index Index of the syscall parameter.
 * \param value New value of the parameter.
 */
void keThreadSetParam(KeThread *thread, uint8_t index, uint32_t value);
/**
 * Returns a syscall parameter in the given thread.
 * \param thread Thread to be modified.
 * \param index Index of the syscall parameter.
 */
uint32_t keThreadGetParam(KeThread *thread, uint8_t index);

/**
 * Returns a thread which can be currently executed and locks it.
 * \param oldthread If set to 1, this function returns the current thread if
 * there is no other thread available.
 */
KeThread *keGetSchedulableThread(int oldthread);

/**
 * Sleeps for a certain amount of time.
 * \param ms Time in milliseconds
 */
void keSleep(uint32_t ms);
/**
 * Stops the currently running thread.
 */
void keExitThread(void);

#endif

