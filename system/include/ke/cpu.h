/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file cpu.h
 * CPU management functions. Especially in SMP systems the operating system has
 * to keep track on which CPU the current code is running.
 */

#ifndef KE_CPU_H_INCLUDED
#define KE_CPU_H_INCLUDED

#include <stdint.h>
#include "ke/thread.h"
#include "ke/level.h"
#include "ke/timer.h"

/**
 * Structure holding the information about a CPU.
 */
typedef struct
{
	/**
	 * APIC ID of the CPU.
	 */
	uint32_t id;
	/**
	 * If set to 1, this CPU is active.
	 */
	uint32_t running;
	/**
	 * Pointer to the idle thread. The idle thread is not inserted into the
	 * thread list but is instead created once for every CPU.
	 */
	KeThread *idlethread;
	/**
	 * Pointer to the currently executed thread. On rescheduling, the active-lock
	 * of the thread is locked.
	 */
	KeThread *currentthread;
	/**
	 * Set to 1 if the CPU is executing an interrupt.
	 * \todo: This is completely broken atm
	 */
	uint32_t interrupt;
	/**
	 * Current execution level.
	 */
	KeExecLevel level;
	/**
	 * Next timer to expire.
	 */
	KeTimer *next_timer;
	/**
	 * Scheduler timer of the current CPU.
	 */
	KeTimer *sched_timer;
	/**
	 * Time at which the last timer was activated.
	 */
	uint64_t time;
	/**
	 * Unix time when the CPU timer was started.
	 */
	uint32_t starttime;
} KeCPU;

/**
 * Reads in a cpuid value.
 * \param code Code of the request.
 * \param output Pointer to four variables holding the register data which is
 * read.
 */
int keCPUID(uint32_t code, uint32_t *output);

/**
 * Writes something to a machine specific register.
 */
void keWriteMSR(uint32_t index, uint64_t value);
/**
 * Reads from a machine specific register.
 */
uint64_t keReadMSR(uint32_t index);

/**
 * Adds a CPU with the given APIC ID to the system.
 */
KeCPU *keAddCPU(uint32_t id);
/**
 * Returns the currently used CPU.
 */
KeCPU *keGetCurrentCPU(void);
/**
 * Returns the number of active CPUs in the system.
 */
uint32_t keGetCPUCount(void);

#endif

