/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file level.h
 * Functions for changing execution levels.
 */

#ifndef KE_LEVEL_H_INCLUDED
#define KE_LEVEL_H_INCLUDED

/**
 * The execution level sets whether interrupts will interrupt the running code.
 * This is necessary especially as quite some functions need locks, calling this
 * code once from threads and once from an interrupt handler at the same time
 * results in a deadlock as the code executed before the interrupt cannot unlock
 * the needed locks any more.
 */
typedef enum
{
	/**
	 * Normal execution level. IRQs can interrupt the code.
	 */
	KE_LEVEL_NORMAL,
	/**
	 * Higher execution level. No IRQs can interrupt the code.
	 * This is the default mode for interrupt handlers.
	 */
	KE_LEVEL_HIGH
} KeExecLevel;

/**
 * Sets the execution level for the current CPU.
 * \return Previous execution level
 */
KeExecLevel keSetExecutionLevel(KeExecLevel level);
/**
 * Returns the execution level of the current CPU.
 * \return Current execution level
 */
KeExecLevel keGetExecutionLevel(void);

#endif

