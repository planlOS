/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file spinlock.h
 * Simple spinlocks
 */
#ifndef KE_SPINLOCK_H_INCLUDED
#define KE_SPINLOCK_H_INCLUDED

#include <stdint.h>

/**
 * Simple spinlock
 */
typedef struct
{
	uint32_t lock;
} KeSpinlock;

/**
 * Initializes a spinlock and unlocks it.
 */
int keInitSpinlock(KeSpinlock *spinlock);
/**
 * Destroys a spinlock.
 */
int keDestroySpinlock(KeSpinlock *spinlock);
/**
 * Locks a spinlock. Blocks if the spinlock cannot be locked.
 * Warning: At the moment, this just busy-waits.
 */
int keLockSpinlock(KeSpinlock *spinlock);
/**
 * Locks a spinlock, returns if the spinlock is already locked.
 * \return Returns 0 if the spinlock was locked by the function.
 */
int keTryLockSpinlock(KeSpinlock *spinlock);
/**
 * Unlocks a spinlock.
 */
int keUnlockSpinlock(KeSpinlock *spinlock);

#endif

