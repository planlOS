/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file dma.h
 * ISA direct memory access
 */

#ifndef KE_DMA_H_INCLUDED
#define KE_DMA_H_INCLUDED

#include <stdint.h>

#define KE_DMA_MODE_READ (1 << 2)
#define KE_DMA_MODE_WRITE (2 << 2)
#define KE_DMA_MODE_ON_DEMAND (0 << 6)
#define KE_DMA_MODE_SINGLE (1 << 6)
#define KE_DMA_MODE_BLOCK (2 << 6)

/**
 * Opens an ISA DMA channel.
 * \param channel Channel to open
 * \param mode Mode for the channel
 * \param buffer Buffer to read/write data to/from
 * \param length Number of bytes to be transferred
 */
int keOpenDMA(uint8_t channel, uint8_t mode, void *buffer, uint32_t length);
/**
 * Closes a DMA channel.
 */
int keCloseDMA(uint8_t channel);

/**
 * Reads the data from the DMA buffer.
 */
int keReadDMA(uint8_t channel);
/**
 * Writes the data to the DMA buffer.
 */
int keWriteDMA(uint8_t channel);

#endif

