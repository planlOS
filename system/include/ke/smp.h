/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef KE_SMP_H_INCLUDED
#define KE_SMP_H_INCLUDED

#include <stdint.h>

typedef struct FloatingPointerStruct
{
	int8_t signature[4];
	uint32_t configtable;
	uint8_t length;
	uint8_t revision;
	uint8_t checksum;
	uint8_t feature0;
	uint8_t feature1;
	uint8_t feature2;
	uint8_t feature3;
	uint8_t feature4;
} __attribute((packed)) FloatingPointerStruct;

typedef struct SMPConfigTable
{
	int8_t signature[4];
	uint16_t tablelength;
	uint8_t revision;
	uint8_t checksum;
	int8_t oemid[8];
	int8_t productid[12];
	uint32_t oemtableptr;
	uint16_t oemtablesize;
	uint16_t entrycount;
	uint32_t apicaddr;
	uint16_t extendedlength;
	uint16_t extendedchecksum;
} __attribute((packed)) SMPConfigTable;

typedef struct SMPProcessorEntry
{
	uint8_t type;
	uint8_t lapicid;
	uint8_t lapicversion;
	uint8_t flags;
	uint32_t signature;
	uint32_t featureflags;
	uint32_t reserved1;
	uint32_t reserved2;
} __attribute((packed)) SMPProcessorEntry;

#define KE_SMP_PROCESSOR_ENABLED 1
#define KE_SMP_PROCESSOR_BOOTSTRAP 2

#define KE_SMP_STACK_SIZE 0x4000

void keInitSMP(void);
void keStartAPs(void);

uint32_t keGetCPUCount(void);

#endif

