/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file pci.h
 * PCI functions.
 */

#ifndef KE_PCI_H_INCLUDED
#define KE_PCI_H_INCLUDED

#include <stdint.h>

#define KE_ADDR_MEMORY 0
#define KE_ADDR_PORT 1

/**
 * PCI device resource. Can be either a port address or a memory address.
 */
typedef struct
{
	/**
	 * Base address of the resource.
	 */
	uintptr_t addr;
	/**
	 * Approximated size of the resource.
	 */
	uint32_t size;
	/**
	 * Type of the resource.
	 */
	uint8_t type;
} KePCIResource;

/**
 * Struct holding the information about a PCI device.
 */
typedef struct
{
	uint8_t bus;
	uint8_t slot;
	uint8_t func;
	
	/**
	 * Device class.
	 */
	uint8_t devclass;
	/**
	 * Device subclass.
	 */
	uint8_t subclass;
	/**
	 * Device revision.
	 */
	uint16_t revision;
	
	/**
	 * Device ID.
	 */
	uint16_t device;
	/**
	 * Vendor ID.
	 */
	uint16_t vendor;
	
	/**
	 * Number of valid resources.
	 */
	uint32_t rescount;
	/**
	 * Resources. Only rescount entries are valid.
	 */
	KePCIResource res[6];
	/**
	 * Device IRQ.
	 */
	uint8_t irq;
} KePCIDevice;

/**
 * Initializes the PCI functions.
 */
int keInitPCI(void);

/**
 * Fills device with the info about the device at the given address.
 * \param device Pointer to the struct to be filled.
 * \param bus Bus of the device.
 * \param slot Slot of the device on the given bus.
 * \param func Function number.
 * \return Returns 0 if the device is available.
 */
int keGetPCIDevice(KePCIDevice *device, uint8_t bus, uint8_t slot, uint8_t func);
/**
 * Returns the number of PCI devices in the system.
 */
int keGetPCIDeviceCount(void);
/**
 * Returns a pointer to structs holding the information about the PCI devices
 * in the system. The returned buffer holds keGetPCIDeviceCount() devices.
 */
KePCIDevice *keGetPCIDevices(void);

#endif

