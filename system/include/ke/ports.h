/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file ports.h
 * Port IO acess functions.
 */

#ifndef PORTS_H_INCLUDED
#define PORTS_H_INCLUDED

#include <stdint.h>

/**
 * Writes one byte to the port.
 */
static inline void outb(uint16_t port, uint8_t data)
{
	asm("outb %0, %1"::"a"(data), "Nd"(port));
}
/**
 * Reads one byte from the port.
 */
static inline uint8_t inb(uint16_t port)
{
	uint8_t data;
	asm("inb %1, %0":"=a"(data):"Nd"(port));
	return data;
}
/**
 * Writes one byte to the port and waits a bit.
 */
static inline void outb_wait(uint16_t port, uint8_t data)
{
	asm("outb %0, %1"::"a"(data), "Nd"(port));
	asm("outb %%al, $0x80"::"a"(0));
}
/**
 * Writes one word to the port.
 */
static inline void outw(uint16_t port, uint16_t data)
{
	asm("outw %0, %1"::"a"(data), "Nd"(port));
}
/**
 * Reads one word from the port.
 */
static inline uint16_t inw(uint16_t port)
{
	uint16_t data;
	asm("inw %1, %0":"=a"(data):"Nd"(port));
	return data;
}
/**
 * Writes one 32bit integer to the port.
 */
static inline void outl(uint16_t port, uint32_t data)
{
	asm("outl %0, %1"::"a"(data), "Nd"(port));
}
/**
 * Reads a 32bit integer from the port.
 */
static inline uint32_t inl(uint16_t port)
{
	uint32_t data;
	asm("inl %1, %0":"=a"(data):"Nd"(port));
	return data;
}

#endif

