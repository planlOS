/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef KE_INTERRUPTS_H_INCLUDED
#define KE_INTERRUPTS_H_INCLUDED

#include <stdint.h>
#include "ke/cpu.h"

// Exception handlers
void keInt00(void);
void keInt01(void);
void keInt02(void);
void keInt03(void);
void keInt04(void);
void keInt05(void);
void keInt06(void);
void keInt07(void);
void keInt08(void);
void keInt09(void);
void keInt0a(void);
void keInt0b(void);
void keInt0c(void);
void keInt0d(void);
void keInt0e(void);
void keInt0f(void);
void keInt10(void);
void keInt11(void);
void keInt12(void);
void keInt13(void);
void keInt14(void);
void keInt15(void);
void keInt16(void);
void keInt17(void);
void keInt18(void);
void keInt19(void);
void keInt1a(void);
void keInt1b(void);
void keInt1c(void);
void keInt1d(void);
void keInt1e(void);
void keInt1f(void);

// IRQ handlers
void keInt20(void);
void keInt21(void);
void keInt22(void);
void keInt23(void);
void keInt24(void);
void keInt25(void);
void keInt26(void);
void keInt27(void);
void keInt28(void);
void keInt29(void);
void keInt2a(void);
void keInt2b(void);
void keInt2c(void);
void keInt2d(void);
void keInt2e(void);
void keInt2f(void);

// APIC interrupts
void keInt30(void);
void keInt31(void);
void keInt32(void);
void keInt33(void);
void keInt34(void);
void keInt35(void);
void keInt36(void);
void keInt37(void);

// Syscall
void keInt80(void);

typedef struct IDTEntry
{
	uint16_t offset_low;
	uint16_t selector;
	uint8_t zero;
	uint8_t type;
	uint16_t offset_high;
} __attribute__((packed)) IDTEntry;

typedef struct IDTDescriptor
{
	uint16_t size;
	uint32_t base;
} __attribute__((packed)) IDTDescriptor;

typedef struct IntStackFrame
{
	uint32_t gs;
	uint32_t fs;
	uint32_t es;
	uint32_t ds;
	uint32_t edi;
	uint32_t esi;
	uint32_t ebp;
	uint32_t esp_;
	uint32_t ebx;
	uint32_t edx;
	uint32_t ecx;
	uint32_t eax;
	uint32_t intno;
	uint32_t error;
	uint32_t eip;
	uint32_t cs;
	uint32_t eflags;
	uint32_t esp;
	uint32_t ss;
} __attribute__((packed)) IntStackFrame;

void keInitPIC(void);
void keInitPIC2(void);

void keRegisterIRQ(uint32_t irqno, void (*handler)(KeCPU*, uint8_t));

#endif
 
