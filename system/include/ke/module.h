/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file module.h
 * Module/kernel symbol functions.
 */

#ifndef KE_MODULE_H_INCLUDED
#define KE_MODULE_H_INCLUDED

#include "ke/multiboot.h"

/**
 * Reads in all kernel symbols from the multiboot information.
 */
int keInitKernelSymbols(MultibootInfo *info);
/**
 * Loads all modules which are in memory.
 */
int keInitModules(MultibootInfo *info);

/**
 * Resolves a memory address and returns the name/function offset. This includes
 * private symbols.
 * \param addr Instruction address to be resolved.
 * \param name Pointer to a variable which will be filled with the name.
 * \param offset Pointer to an integer which will be filled with the difference
 * between the beginning of the function and the address.
 */
int keResolveAddress(uintptr_t addr, char **name, uintptr_t *offset);
/**
 * Resolves a public kernel symbol and returns the name.
 */
uintptr_t keResolveSymbol(char *name);
/**
 * Resolves a memory address which is not within the kernel and returns the
 * module/name/function offset. This includes private symbols.
 * \todo: No function name resolving yet.
 * \param addr Instruction address to be resolved.
 * \param module Pointer to a variable which will be filled with the module
 * path.
 * \param name Pointer to a variable which will be filled with the name.
 * \param offset Pointer to an integer which will be filled with the difference
 * between the beginning of the function and the address.
 */

int keResolveModuleAddress(uintptr_t addr, char **module, char **name, uintptr_t *offset);

#endif

