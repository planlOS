/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file apic.h
 * Advanced programmable interrupt controller
 */

#ifndef KE_APIC_H_INCLUDED
#define KE_APIC_H_INCLUDED

#include <stdint.h>

/**
 * Initialized the APIC. Is called by the operating system at startup.
 */
int keAPICInit(void);

/**
 * Returns the APIC ID of the current CPU.
 */
uint32_t keAPICGetCPU(void);

/**
 * Writes into an APIC register.
 */
void keAPICWrite(uint16_t offset, uint32_t value);
/**
 * Reads from an APIC register.
 */
uint32_t keAPICRead(uint16_t offset);
/**
 * Send an INIT IPI to all CPUs but the currently used.
 */
void keAPICWriteInit(void);
/**
 * Send a STARTUP IPI to all CPUs but the currently used.
 */
void keAPICWriteStartup(uintptr_t entry);
/**
 * Sends a custom IPI.
 * \param intno Interrupt to be triggered.
 * \param self If set to 1, include currently used CPU.
 */
void keAPICBroadcast(uint8_t intno, int self);

#endif

