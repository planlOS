/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file debug.h
 * Functions for debug output.
 * Output is sent over the serial line, and in the early startup phases also to
 * the screen.
 */

#ifndef KE_DEBUG_H_INCLUDED
#define KE_DEBUG_H_INCLUDED

/**
 * Initializes the debug output.
 */
void kePrintInit(void);
/**
 * Prints something over the serial line.
 * \param fmt printf-like format string
 */
void kePrint(const char *fmt, ...);
/**
 * Prints something over the serial line using an array for arguments.
 * \param fmt printf-like format string
 * \param args Pointer to an array with the variable arguments.
 */
void kePrintList(const char *fmt, int **args);

#endif

