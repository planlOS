/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file timer.h
 * Time/timer functions
 */

#ifndef KE_TIMER_H_INCLUDED
#define KE_TIMER_H_INCLUDED

#include <stdint.h>

struct KeTimer;

/**
 * Timer callback function
 */
typedef void (*KeTimerCallback)(struct KeTimer *timer);

/**
 * Generic timer struct. If the timer is triggered, an interrupt is caused, so
 * don't use this timer with too small intervals as it has absolute priority.
 */
typedef struct KeTimer
{
	/**
	 * Time to be waited in microseconds. If the timer is periodic, this is a
	 * relative value, otherwise it's an absolute value which can be computed by
	 * adding your time to keGetTime().
	 */
	uint64_t time;
	/**
	 * Sets whether this is a periodic timer, e.g. gets reactivated after it
	 * expired.
	 */
	int periodic;
	
	/**
	 * Callback to be called after the timer has expired.
	 */
	KeTimerCallback callback;
	
	/**
	 * Absolute time at which the timer expires. Must not be set by the user.
	 */
	uint64_t targettime;
	
	/**
	 * 1, if the timer is currently active, e.g. is the next timer in the queue.
	 * Must not be set by the user.
	 */
	int active;
	/**
	 * 1, if the timer is queued. Must not be set by the user.
	 */
	int queued;
	/**
	 * Next timer in the queue.
	 */
	struct KeTimer *next;
	/**
	 * Previous timer in the queue.
	 */
	struct KeTimer *prev;
} KeTimer;

/**
 * Initializes the timer functions. Is called by the operating system at
 * startup.
 */
void keInitTimer(uint32_t frequency);

/**
 * Installs the timer system on the current CPU.
 */
void keInstallTimer(void);

/**
 * Creates a new timer. The user then has to specify callback in the timer
 * struct before passing it to keSetTimer().
 */
KeTimer *keCreateTimer(void);
/**
 * Unqueues and destroys a timer.
 * \todo Stopping the timer does not quite work yet.
 */
int keDestroyTimer(KeTimer *timer);
/**
 * Adds a timer to the timer queue.
 * \param timer Timer to be activated
 * \param time Time to be waited. If the timer is not periodic, this is an
 * absolute value.
 * \param periodic If set to 1, the timer is activated again if it expires.
 */
int keSetTimer(KeTimer *timer, uint64_t time, int periodic);
/**
 * Cancels an activated timer.
 * \todo Does not quite work yet if the timer does not belong to the current
 * CPU.
 */
int keCancelTimer(KeTimer *timer);
/**
 * Adjusts a timer.
 * \todo Does not quite work yet if the timer does not belong to the current
 * CPU.
 */
int keAdjustTimer(KeTimer *timer, uint64_t time);

/**
 * Returns the current time in microseconds since the start of the operating
 * system.
 */
uint64_t keGetTime(void);
/**
 * Returns the interval in which threads are rescheduled.
 */
uint32_t keGetTimePerTick(void);

/**
 * Returns the current time in seconds since unix epoch.
 */
uint32_t keGetUnixTime(void);

#endif

