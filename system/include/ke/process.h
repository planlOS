/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file process.h
 * Process management functions
 */

#ifndef KE_PROCESS_H_INCLUDED
#define KE_PROCESS_H_INCLUDED

#include <stdint.h>
#include <planlos/signals.h>
#include "mm/memory.h"
#include "fs/fs.h"

/**
 * Process information
 */
typedef struct KeProcess
{
	/**
	 * Address space of the process
	 */
	MmAddressSpace memory;
	/**
	 * Process ID
	 */
	uint32_t pid;
	/**
	 * Array of threads in the process.
	 */
	struct KeThread **threads;
	/**
	 * Number of threads
	 */
	uint32_t threadcount;
	/**
	 * Process lock. Has to be locked whenever the process is modified.
	 */
	KeSpinlock lock;
	
	/**
	 * If this is set to 1, the process is a zombie process (was closed, but not
	 * waited for).
	 */
	int zombie;
	
	/**
	 * Array containing the open file handles. Entries can be 0 if the file was
	 * closed.
	 */
	FsFileHandle **fd;
	/**
	 * Number of entries in the fd array.
	 */
	uint32_t fdcount;
	/**
	 * Current working directory.
	 */
	char *cwd;
	
	/**
	 * Signal handlers for the current process.
	 */
	uintptr_t signal_handlers[NSIG];
	
	struct KeThread *waitthread;
} KeProcess;

/**
 * Initialitzes the process system.
 */
int keInitProcessManager(void);

/**
 * Creates a new process, with an empty address space and no threads attached.
 */
KeProcess *keCreateProcess(void);
/**
 * Stops all threads belonging to a process and stops the process (makes it a
 * zombie process).
 */
int keTerminateProcess(KeProcess *process);
/**
 * Stops all threads belonging to a process and completely destroys the process.
 */
int keDestroyProcess(KeProcess *process);
/**
 * Clears the address space of the process and stops all threads in it but the
 * current thread.
 */
int keClearProcess(KeProcess *process, struct KeThread *current);

/**
 * Returns the process with the given ID.
 */
KeProcess *keGetProcess(uint32_t pid);

/**
 * Sends a signal to the process.
 */
void keSendSignal(KeProcess *process, struct KeThread *thread, int signal);

#endif

