/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file multiboot.h
 * Multiboot information
 */

#ifndef KE_MULTIBOOT_H_INCLUDED
#define KE_MULTIBOOT_H_INCLUDED

#include <stdint.h>

#define MULTIBOOT_HEADER_MAGIC 0x1BADB002
#define MULTIBOOT_HEADER_FLAGS 0x00000003
#define MULTIBOOT_BOOTLOADER_MAGIC 0x2BADB002

/**
 * a.out symbol table. Not used anywhere.
 */
typedef struct AoutSymbolTable
{
	uint32_t tabsize;
	uint32_t strsize;
	uint32_t addr;
	uint32_t reserved;
} AoutSymbolTable;

/**
 * ELF section header table holding the information about the kernel executable.
 */
typedef struct ElfSectionHeaderTable
{
	uint32_t num;
	uint32_t size;
	uint32_t addr;
	uint32_t shindex;
} ElfSectionHeaderTable;

/**
 * Multiboot module information.
 */
typedef struct MbModule
{
	/**
	 * Start of the physical module memory.
	 */
	uint32_t start;
	/**
	 * End of the physical module memory.
	 */
	uint32_t end;
	/**
	 * File name of the module.
	 */
	uint32_t string;
	uint32_t reserved;
} MbModule;

/**
 * Multiboot memory map.
 */
typedef struct MbMemoryMap
{
	/**
	 * Size of the rest of the structure not including this member (can be more
	 * than 20 bytes).
	 */
	uint32_t size;
	/**
	 * Low 32 bits of the base address.
	 */
	uint32_t baseaddrlow;
	/**
	 * High 32 bits of the base address.
	 */
	uint32_t baseaddrhigh;
	/**
	 * Low 32 bits of the length of the memory range.
	 */
	uint32_t lengthlow;
	/**
	 * High 32 bits of the length of the memory range.
	 */
	uint32_t lengthhigh;
	/**
	 * Type of the memory range.
	 */
	uint32_t type;
} MbMemoryMap;

/**
 * Multiboot information structure.
 */
typedef struct MultibootInfo
{
	/**
	 * Flags telling what information is included in the struct.
	 */
	uint32_t flags;
	/**
	 * Amount of base memory.
	 */
	uint32_t lowermem;
	/**
	 * Approximated amount of upper memory.
	 */
	uint32_t uppermem;
	/**
	 * Device the operating system was booted from. The first byte contains the
	 * BIOS number of the device, the rest contains the partition information.
	 */
	uint32_t bootdevice;
	/**
	 * Kernel command line.
	 */
	uint32_t cmdline;
	/**
	 * Number of modules loaded by the boot loader.
	 */
	uint32_t modcount;
	/**
	 * Pointer to the module info block (array of MbModule).
	 */
	uint32_t modaddr;
	/**
	 * Kernel symbol information.
	 */
	union
	{
		AoutSymbolTable aoutsym;
		ElfSectionHeaderTable elfsections;
	} u;
	/**
	 * Length of the memory map.
	 */
	uint32_t mmaplength;
	/**
	 * Pointer to the first memory range information (MbMemoryMap).
	 */
	uint32_t mmapaddr;
} MultibootInfo;

#define CHECK_FLAG(flags,bit) ((flags) & (1 << (bit)))

#endif

