/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef KE_ERRORS_H_INCLUDED
#define KE_ERRORS_H_INCLUDED

#define KE_ERROR_UNKNOWN 1
#define KE_ERROR_NOTIMPLEMENTED 2
#define KE_ERROR_INVALIDADDR 3
#define KE_ERROR_OOM 4
#define KE_ERROR_KERNEL_OOM 5
#define KE_ERROR_INVALID_ARG 6
#define KE_ERROR_PAGE_FAULT 7
#define KE_ERROR_PROTECTION_FAULT 8
#define KE_ERROR_BUSY 9

#endif

