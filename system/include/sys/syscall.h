/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file syscall.h
 * System call functions.
 */

#ifndef SYS_SYSCALL_H_INCLUDED
#define SYS_SYSCALL_H_INCLUDED

#include "ke/thread.h"
#include <planlos/syscalls.h>

#define SYS_FCNTL_DUP 1

/**
 * Safely maps user memory to the kernel space.
 * \param thread Calling thread.
 * \param addr Virtual address in the address space of the process.
 * \param size Number of bytes to be mapped.
 * \param writeable If set to 1, the memory must be writeable by the process. If
 * it is not, the function will fail.
 * \return Kernel address for the memory. Must be unmapped with
 * sysUnmapUserMemory.
 */
void *sysMapUserMemory(KeThread *thread, uintptr_t addr, uint32_t size, uint32_t writeable);
/**
 * Unmaps memory mapped previously by sysMapUserMemory.
 * \param data Kernel address of the memory.
 * \param size Number of bytes to be unmapped. 
 */
void sysUnmapUserMemory(void *data, uint32_t size);

/**
 * Allocates a file descriptor in the process and fills it with the file handle.
 * \return New file descriptor.
 */
int sysCreateFileDescriptor(KeProcess *process, FsFileHandle *fh);

/**
 * Entry point for a system call.
 * \param thread Current thread which called the syscall.
 * \param index System call number.
 * \param param1 Pointer to the first parameter.
 * \param param2 Pointer to the second parameter.
 * \param param3 Pointer to the third parameter.
 * \param param4 Pointer to the forth parameter.
 * \param param5 Pointer to the fifth parameter.
 */
void sysSyscall(KeThread *thread, uintptr_t index, uintptr_t *param1,
	uintptr_t *param2, uintptr_t *param3, uintptr_t *param4, uintptr_t *param5);

#endif

