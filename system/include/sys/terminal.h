/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SYS_TERMINAL_H_INCLUDED
#define SYS_TERMINAL_H_INCLUDED

#include "fs/devfs.h"
#include "fs/request.h"

typedef struct
{
	FsDeviceFile file;
	char *inputbuffer;
	int inputbuffer_size;
	int inputbuffer_pos;
	char *screendata;
	int fgcolor;
	int brightfg;
	int bgcolor;
	int x;
	int y;
	int echo;
	FsRequest **requests;
	int requestcount;
	struct KeProcess *owner;
	int opencount;
} SysTextTerminal;

typedef struct
{
	FsDeviceFile file;
} SysGfxTerminal;

#define SYS_KEY_ESCAPE 256
#define SYS_KEY_BACKSPACE 257
#define SYS_KEY_LCONTROL 258
#define SYS_KEY_LSHIFT 259
#define SYS_KEY_RSHIFT 260
#define SYS_KEY_LALT 261
#define SYS_KEY_CAPS 262
#define SYS_KEY_F1 263
#define SYS_KEY_F2 264
#define SYS_KEY_F3 265
#define SYS_KEY_F4 266
#define SYS_KEY_F5 267
#define SYS_KEY_F6 268
#define SYS_KEY_F7 269
#define SYS_KEY_F8 270
#define SYS_KEY_F9 271
#define SYS_KEY_F10 272
#define SYS_KEY_F11 273
#define SYS_KEY_F12 274
#define SYS_KEY_NUM 275
#define SYS_KEY_SCROLL 276

#define SYS_MODIFIER_SHIFT 1

int sysInitTerminals(void);

void sysSetCurrentTerminal(int terminal);
int sysGetCurrentTerminal(void);

int sysTerminalInjectKey(int key, int modifiers, int down);

char *sysTerminalPreparePanic(int *width, int *height);

#endif

