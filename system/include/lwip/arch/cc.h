
#ifndef LWIP_ARCH_CC_H_INCLUDED
#define LWIP_ARCH_CC_H_INCLUDED

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef uint32_t u32_t;
typedef uint16_t u16_t;
typedef uint8_t u8_t;
typedef int32_t s32_t;
typedef int16_t s16_t;
typedef int8_t s8_t;
typedef uintptr_t mem_ptr_t;

#define BYTE_ORDER LITTLE_ENDIAN

void LWIP_PLATFORM_ASSERT(char *msg);

#define PERF_START
#define PERF_STOP(x)

#endif

