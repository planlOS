
#ifndef LWIP_ARCH_SYS_ARCH_H_INCLUDED
#define LWIP_ARCH_SYS_ARCH_H_INCLUDED

#include "ke/spinlock.h"
#include "ke/thread.h"
#include "ke/level.h"

typedef KeSpinlock *sys_sem_t;

#define SYS_SEM_NULL 0

/*sys_sem_t sys_sem_new(u8_t count);
void sys_sem_free(sys_sem_t sem);
void sys_sem_signal(sys_sem_t sem);
u32_t sys_arch_sem_wait(sys_sem_t sem, u32_t timeout);*/

typedef struct sys_mbox_entry_t
{
	void *data;
	struct sys_mbox_entry_t *next;
} sys_mbox_entry_t;

struct sys_mbox
{
	sys_mbox_entry_t *firstentry;
	KeSpinlock lock;
};
typedef struct sys_mbox *sys_mbox_t;

#define SYS_MBOX_NULL 0

/*sys_mbox_t sys_mbox_new(void);
void sys_mbox_free(sys_mbox_t mbox);
void sys_mbox_post(sys_mbox_t mbox, void *msg);
u32_t sys_arch_mbox_fetch(sys_mbox_t mbox, void **msg, u32_t timeout);
u32_t sys_arch_mbox_tryfetch(sys_mbox_t mbox, void **msg);*/

struct sys_thread
{
	KeThread *thread;
	char *name;
	struct sys_timeouts timeouts;
};

typedef struct sys_thread *sys_thread_t;

typedef KeExecLevel sys_prot_t;

sys_prot_t sys_arch_protect(void);
void sys_arch_unprotect(sys_prot_t p);

#endif

