
#define SYS_LIGHTWEIGHT_PROT 1
#define MEM_LIBC_MALLOC 1
#define MEM_SIZE 30000
//#define MEM_USE_POOLS 1
#define MEM_ALIGNMENT 4
//#define MEMP_OVERFLOW_CHECK 1
#define LWIP_DHCP 1
#define LWIP_DNS 1
#define LWIP_NETIF_API 1
#define LWIP_HAVE_LOOPIF 1
#define LWIP_COMPAT_SOCKETS 0
#define MEMP_NUM_TCP_PCB 10
#define MEMP_NUM_NETBUF 10
#define MEMP_NUM_NETCONN 10

#define PACK_STRUCT_STRUCT __attribute__((packed))

#define LWIP_PROVIDE_ERRNO

#define MEMP_NUM_SYS_TIMEOUT 10

//#define PBUF_DEBUG LWIP_DBG_ON
//#define MEMP_DEBUG LWIP_DBG_ON
//#define TCPIP_DEBUG LWIP_DBG_ON
//#define LWIP_DBG_MIN_LEVEL LWIP_DBG_MASK_LEVEL
//#define TCP_QLEN_DEBUG LWIP_DBG_ON
//#define TCP_OUTPUT_DEBUG LWIP_DBG_ON
//#define LWIP_DEBUG

#define LWIP_PLATFORM_DIAG(...) {char tmp[100]; snprintf(tmp, 100, __VA_ARGS__); kePrint(tmp); }
#define U16_F "d"
#define U32_F "d"
#define S16_F "d"
#define S32_F "d"
#define X32_F "x"
#define X16_F "x"

