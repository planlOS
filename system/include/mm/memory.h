/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MM_MEMORY_H_INCLUDED
#define MM_MEMORY_H_INCLUDED

#include "ke/multiboot.h"
#include "ke/spinlock.h"

#define MM_KERNEL_BASE 0xC0000000

typedef struct
{
	uintptr_t phys_addr;
	uint32_t *virt_addr;
} MmAddressSpace;

#define MM_MEMORY_ADDR_SHARED 1

typedef struct MmMemoryArea
{
	uint32_t type;
	uintptr_t virt_addr;
	uintptr_t size;
	struct MmMemoryArea *next;
} MmMemoryArea;

int mmInitMemoryManager(MultibootInfo *mbinfo, uintptr_t kernel_begin,
	uintptr_t kernel_end);
int mmInitPhysicalMemory(MultibootInfo *mbinfo, uintptr_t kernel_begin,
	uintptr_t kernel_end);
int mmInitVirtualMemory(MultibootInfo *mbinfo, uintptr_t kernel_begin,
	uintptr_t kernel_end);

KeSpinlock *mmGetMemoryLock(void);

int mmCreateAddressSpace(MmAddressSpace *addrspace);
int mmCloneAddressSpace(MmAddressSpace *dest, MmAddressSpace *src);
int mmDestroyAddressSpace(MmAddressSpace *addrspace);
int mmClearAddressSpace(MmAddressSpace *addrspace);

#define MM_MEMORY_ALLOC_DMA 0x1
#define MM_MEMORY_ALLOC_PHYS 0x2

uintptr_t mmAllocPhysicalMemory(uint32_t flags, uintptr_t phys_addr,
	uint32_t size);
void mmFreePhysicalMemory(uintptr_t addr, uint32_t size);

#define MM_MAP_READ 0x1
#define MM_MAP_WRITE 0x2
#define MM_MAP_EXECUTE 0x4
#define MM_MAP_UNCACHEABLE 0x8
#define MM_MAP_USER 0x10

int mmMapMemory(MmAddressSpace *addrspace, uintptr_t phys, uintptr_t virt,
	uint32_t mode);
int mmMapKernelMemory(uintptr_t phys, uintptr_t virt,
	uint32_t mode);
uintptr_t mmGetPhysAddress(MmAddressSpace *addrspace, uintptr_t virt);
uintptr_t mmKernelGetPhysAddress(uintptr_t virt);

#define MM_MIN_USER_PAGE 0x00001000
#define MM_MAX_USER_PAGE (MM_KERNEL_BASE - 0x1000)
#define MM_MIN_KERNEL_PAGE MM_KERNEL_BASE
#define MM_MAX_KERNEL_PAGE 0xEFFFF000

uintptr_t mmFindFreePages(MmAddressSpace *addrspace, uintptr_t max, uintptr_t min,
	int downwards, uint32_t size);
uintptr_t mmFindFreeKernelPages(uintptr_t max, uintptr_t min,
	int downwards, uint32_t size);
int mmKernelPageIsFree(uintptr_t addr);

uint32_t mmGetPhysPageCount(void);

void keSyncVirtMemory(void);

#endif

