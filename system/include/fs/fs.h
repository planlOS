/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file fs.h
 * File system access functions.
 */

#ifndef FS_FS_H_INCLUDED
#define FS_FS_H_INCLUDED

#include <stdint.h>
#include "mm/memory.h"

/**
 * File system driver can only create one file system.
 */
#define FS_DRIVER_SINGLE 1
/**
 * File system driver can not be mounted manually.
 */
#define FS_DRIVER_NOMOUNT 2
/**
 * File system does not need any data source (e.g. /dev).
 */
#define FS_DRIVER_NODATA 4

struct FsRequest;
struct KeProcess;

/**
 * File system information.
 */
typedef struct FsFileSystem
{
	/**
	 * Path at which the file system was mounted.
	 */
	char *path;
	/**
	 * Device file which works as the data source for this file system.
	 */
	struct FsFileHandle *device;
	/**
	 * Driver which made this file system.
	 */
	struct FsFileSystemDriver *driver;
	
	/**
	 * Callback for unmounting the file system.
	 */
	int (*unmount)(struct FsFileSystem *fs);
	/**
	 * Callback for file system requests.
	 */
	int (*query_request)(struct FsFileSystem *fs, struct FsRequest *request);
} FsFileSystem;

/**
 * File system driver.
 */
typedef struct FsFileSystemDriver
{
	/**
	 * File system driver flags.
	 */
	uint32_t flags;
	
	/**
	 * Mounts a file system using this driver.
	 */
	FsFileSystem *(*mount)(struct FsFileSystemDriver *driver, const char *path,
		const char *device, uint32_t flags);
} FsFileSystemDriver;

/**
 * File information as created by a file system.
 */
typedef struct FsFile
{
	/**
	 * Number of file handles referring to this file. The file is not closed
	 * unless this goes down to 0.
	 */
	uint32_t refcount;
	/**
	 * File system which created this file.
	 */
	FsFileSystem *fs;
} FsFile;

/**
 * File handle information
 */
typedef struct FsFileHandle
{
	/**
	 * The file which this file handle refers to.
	 */
	FsFile *file;
	/**
	 * Current reading/writing position.
	 */
	uint64_t position;
	/**
	 * File flags as specified at fsOpen().
	 */
	uint32_t flags;
} FsFileHandle;

/**
 * Initializes the file system.
 */
int fsInit(void);

/**
 * Registers a file system driver with the given name.
 */
int fsRegisterDriver(FsFileSystemDriver *driver, const char *name);
/**
 * Removes a file system driver.
 */
int fsUnregisterDriver(FsFileSystemDriver *driver);
/**
 * Returns the file system driver with the given name.
 */
FsFileSystemDriver *fsGetDriver(const char *name);
/**
 * Mounts a file system onto a directory.
 * \param driver Driver to be used.
 * \param path Path for the file system.
 * \param device Device to be used as the data source for the file system.
 * \param flags Mounting flags like readonly access.
 */
int fsMount(FsFileSystemDriver *driver, const char *path, const char *device,
	uint32_t flags);
/**
 * Unmounts the file system at the given path.
 */
int fsUnmount(const char *path);

#define FS_OPEN_CREATE 1
#define FS_OPEN_READ 2
#define FS_OPEN_WRITE 4
#define FS_OPEN_RW 6

#define FS_MKNOD_FILE 1
#define FS_MKNOD_DIR 2

/**
 * Opens a file for use from within the kernel and creates a file handle.
 * \param filename Absolute file name.
 * \param mode Flags for the open request.
 */
FsFileHandle *fsOpen(const char *filename, uint32_t mode);
/**
 * Overrides the process variable for the open request.
 */
FsFileHandle *fsProcessOpen(struct KeProcess *process, const char *filename, uint32_t mode);
/**
 * Closes a file handle.
 */
int fsClose(FsFileHandle *file);
/**
 * Creates a file system node.
 */
int fsMknod(const char *filename, uint32_t mode);

/**
 * Writes data to an opened file.
 */
int fsWrite(FsFileHandle *file, void *buffer, int size);
/**
 * Reads data from an opened file.
 */
int fsRead(FsFileHandle *file, void *buffer, int size, int blocking);
/**
 * Changes the current position within a file. The position can be different
 * within multiple file handles even if they point to one file internally.
 * \param file File handle to be used.
 * \param offset New offset, relative to whence.
 * \param whence Origin for the seek request, can be 0 (from the start of the
 * file), 1 (from the current position) or 2 (from the end of the file).
 * \return Current absolute file offset.
 */
uint64_t fsSeek(FsFileHandle *file, int64_t offset, int whence);
/**
 * Issues a file ioctl request.
 */
int fsIOCTL(FsFileHandle *file, int request, ...);
/**
 * Returns the buffer size for the ioctl parameter or 0 if only one number is
 * passed to it directly as an argument.
 */
int fsGetIOCTLSize(FsFileHandle *file, int request);

#endif

