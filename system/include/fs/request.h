/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file request.h
 * Raw file system request functions.
 */

#ifndef FS_REQUEST_H_INCLUDED
#define FS_REQUEST_H_INCLUDED

#include "fs/fs.h"
#include "ke/spinlock.h"
#include "ke/thread.h"

/**
 * File system request type
 */
typedef enum
{
	FS_REQUEST_READ,
	FS_REQUEST_WRITE,
	FS_REQUEST_SEEK,
	FS_REQUEST_IOCTL,
	FS_REQUEST_OPEN,
	FS_REQUEST_CLOSE,
	FS_REQUEST_MKNOD,
	FS_REQUEST_IOCTLSIZE
} FsRequestType;

/**
 * File system request
 */
typedef struct FsRequest
{
	/**
	 * Request type.
	 */
	FsRequestType type;
	/**
	 * Target file system.
	 */
	FsFileSystem *fs;
	/**
	 * Target file. Can only be 0 for FS_REQUEST_OPEN and FS_REQUEST_MKNOD.
	 */
	FsFile *file;
	/**
	 * Generic flags for the file system request.
	 */
	uint32_t flags;
	
	/**
	 * Size of the request buffer.
	 */
	uint32_t bufferlength;
	/**
	 * Request buffer for data transmission.
	 */
	void *buffer;
	
	/**
	 * Offset for read/write and seek operations.
	 */
	int64_t offset;
	/**
	 * Origin for seek operations. 0 means start of the file, 2 means end
	 */
	int whence;
	
	/**
	 * If set to 0, don't perform any operations which would block the calling
	 * thread.
	 */
	int block;
	
	/**
	 * Status of the request.
	 */
	int status;
	/**
	 * Return value for the request type. Holds for example the number of bytes
	 * read/written, or -1 in case of an error.
	 */
	int return_value;
	/**
	 * If set to 1, the request has been finished.
	 */
	int finished;
	
	/**
	 * Calling process.
	 */
	KeProcess *process;
	
	/**
	 * Thread waiting for the request to be completed. This will in many cases
	 * be 0, so don't rely on this being the thread which issued the request.
	 */
	KeThread *caller;
	
	/**
	 * Request lock. Can be used by file systems.
	 */
	KeSpinlock lock;
} FsRequest;

/**
 * Creates a file system request.
 * \param request Request to be issued.
 * \param wait If set to 1, blocks until the request has been completed.
 * \param process Used to override the process member of the request. If it is
 * 0, the process is retrieved automatically.
 */
int fsCreateRequest(FsRequest *request, int wait, KeProcess *process);
/**
 * Waits for a request to be completed.
 */
int fsWaitRequest(FsRequest *request);

/**
 * Finishes a request and wakes up any thread waiting for it.
 */
int fsFinishRequest(FsRequest *request);

#endif

