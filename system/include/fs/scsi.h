/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
 * \file scsi.h
 * SCSI device interface.
 */

#ifndef FS_SCSI_H_INCLUDED
#define FS_SCSI_H_INCLUDED

#include "ke/spinlock.h"

/**
 * SCSI packet direction.
 */
typedef enum
{
	FS_SCSI_NO_DATA,
	FS_SCSI_READ,
	FS_SCSI_WRITE
} FsSCSIDirection;

/**
 * SCSI packet data
 */
typedef struct
{
	/**
	 * Packet direction.
	 */
	FsSCSIDirection direction;
	/**
	 * Data buffer.
	 */
	void *buffer;
	/**
	 * Number of bytes to be read/written.
	 */
	uint32_t length;
	
	/**
	 * SCSI command.
	 */
	uint8_t command[16];
	/**
	 * Size of the SCSI command.
	 */
	uint8_t commandsize;
} FsSCSIPacket;

/**
 * SCSI device information.
 */
typedef struct FsSCSIDevice
{
	/**
	 * Callback for file system requests.
	 */
	int (*request)(struct FsSCSIDevice *device, FsSCSIPacket *packet);
	/**
	 * Lock to protect device agains multiple simultaneous accesses.
	 */
	KeSpinlock opened;
} FsSCSIDevice;

/**
 * Registers a new SCSI device.
 */
int fsRegisterSCSIDevice(FsSCSIDevice *device);
/**
 * Removes an SCSI device from the system.
 */
int fsRemoveSCSIDevice(FsSCSIDevice *device);

/**
 * Returns the number of registered SCSI devices.
 */
uint32_t fsGetSCSIDeviceCount(void);
/**
 * Opens a single SCSI device.
 */
FsSCSIDevice *fsOpenSCSIDevice(uint32_t index);
/**
 * Closes an SCSI device.
 */
void fsCloseSCSIDevice(FsSCSIDevice *device);

#endif

