/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/errors.h"
#include "ke/debug.h"
#include "ke/ports.h"
#include "ke/interrupts.h"
#include "sys/terminal.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static int keycodes[128 * 3] =
{
	// 00
	0, 0, 0,
	SYS_KEY_ESCAPE, SYS_KEY_ESCAPE, SYS_KEY_ESCAPE,
	'1', '!', 0,
	'2', '"', 0,
	'3', 0, 0,
	'4', '$', 0,
	'5', '%', 0,
	'6', '&', 0,
	'7', '/', '{',
	'8', '(', '[',
	'9', ')', ']',
	'0', '=', '}',
	0, '?', '\\',
	0, 0, 0,
	SYS_KEY_BACKSPACE, SYS_KEY_BACKSPACE, SYS_KEY_BACKSPACE,
	'\t', '\t', '\t',
	// 10
	'q', 'Q', '@',
	'w', 'W', 0,
	'e', 'E', 0,
	'r', 'R', 0,
	't', 'T', 0,
	'z', 'Z', 0,
	'u', 'U', 0,
	'i', 'I', 0,
	'o', 'O', 0,
	'p', 'P', 0,
	0, 0, 0,
	'+', '*', '~',
	'\n', '\n', '\n',
	SYS_KEY_LCONTROL, SYS_KEY_LCONTROL, SYS_KEY_LCONTROL,
	'a', 'A', 0,
	's', 'S', 0,
	// 20
	'd', 'D', 0,
	'f', 'F', 0,
	'g', 'G', 0,
	'h', 'H', 0,
	'j', 'J', 0,
	'k', 'K', 0,
	'l', 'L', 0,
	0, 0, 0,
	0, 0, 0,
	0, 0, 0,
	SYS_KEY_LSHIFT, SYS_KEY_LSHIFT, SYS_KEY_LSHIFT,
	'#', '\'', 0,
	'y', 'Y', 0,
	'x', 'X', 0,
	'c', 'C', 0,
	'v', 'V', 0,
	// 30
	'b', 'B', 0,
	'n', 'N', 0,
	'm', 'M', 0,
	',', ';', 0,
	'.', ':', 0,
	'-', '_', 0,
	SYS_KEY_RSHIFT, SYS_KEY_RSHIFT, SYS_KEY_RSHIFT,
	'*', '*', '*',
	SYS_KEY_LALT, SYS_KEY_LALT, SYS_KEY_LALT,
	' ', ' ', ' ',
	SYS_KEY_CAPS, SYS_KEY_CAPS, SYS_KEY_CAPS,
	SYS_KEY_F1, SYS_KEY_F1, SYS_KEY_F1,
	SYS_KEY_F2, SYS_KEY_F2, SYS_KEY_F2,
	SYS_KEY_F3, SYS_KEY_F3, SYS_KEY_F3,
	SYS_KEY_F4, SYS_KEY_F4, SYS_KEY_F4,
	SYS_KEY_F5, SYS_KEY_F5, SYS_KEY_F5,
	// 40
	SYS_KEY_F6, SYS_KEY_F6, SYS_KEY_F6,
	SYS_KEY_F7, SYS_KEY_F7, SYS_KEY_F7,
	SYS_KEY_F8, SYS_KEY_F8, SYS_KEY_F8,
	SYS_KEY_F9, SYS_KEY_F9, SYS_KEY_F9,
	SYS_KEY_F10, SYS_KEY_F10, SYS_KEY_F10,
	SYS_KEY_NUM, SYS_KEY_NUM, SYS_KEY_NUM,
	SYS_KEY_SCROLL, SYS_KEY_SCROLL, SYS_KEY_SCROLL,
	'7', '7', '7',
	'8', '8', '8',
	'9', '9', '9',
	'-', '-', '-',
	'4', '4', '4',
	'5', '5', '5',
	'6', '6', '6',
	'+', '+', '+',
	'1', '1', '1',
	// 50
	'2', '2', '2',
	'3', '3', '3',
	'0', '0', '0',
	'.', '.', '.',
	0, 0, 0,
	0, 0, 0,
	0, 0, 0,
	SYS_KEY_F11, SYS_KEY_F11, SYS_KEY_F11,
	SYS_KEY_F12, SYS_KEY_F12, SYS_KEY_F12,
};

int lshift = 0;
int rshift = 0;
int caps = 0;
int altgr = 0;
int lalt = 0;

void ps2Handler(KeCPU *cpu, uint8_t intno)
{
	uint8_t data = inb(0x60);
	int pushed = !(data & 0x80);
	int shiftpressed = lshift || rshift;
	if (caps) shiftpressed = !shiftpressed;
	// Translate key
	int key = keycodes[(data & 0x7F) * 3 + shiftpressed];
	//kePrint("%x: %c\n", data & 0x7F, key);
	// Modifiers
	if (key == SYS_KEY_LSHIFT) lshift = pushed;
	if (key == SYS_KEY_RSHIFT) rshift = pushed;
	if (key == SYS_KEY_CAPS) caps = pushed;
	if (key == SYS_KEY_LALT) lalt = pushed;
	int modifiers = 0;
	if (shiftpressed) modifiers |= SYS_MODIFIER_SHIFT;
	if (altgr) modifiers |= 2;
	if (lalt) modifiers |= 4;
	// Inject key into terminal system
	sysTerminalInjectKey(key, modifiers, pushed);
}

int modLoad(void)
{
	// Register IRQ
	keRegisterIRQ(0x01, ps2Handler);
	
	return 0;
}

int modUnload(void)
{
	return 0;
}

