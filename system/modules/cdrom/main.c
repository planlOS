/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/errors.h"
#include "ke/debug.h"
#include "fs/fs.h"
#include "fs/scsi.h"
#include "fs/request.h"
#include "fs/devfs.h"
#include "ke/spinlock.h"
#include "ke/thread.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cdi/lists.h>

// TODO: This is kind of incomplete... -.-

union cdrom_command
{
	struct
	{
		uint8_t opcode;
		uint8_t reserved0;
		uint32_t address;
		uint8_t reserved1;
		uint16_t length;
		uint8_t reserved2;
	} __attribute__ ((packed)) dfl;
	struct
	{
		uint8_t opcode;
		uint8_t reserved0;
		uint32_t address;
		uint32_t length;
		uint16_t reserved1;
	} __attribute__ ((packed)) ext;
};

struct cdrom_device
{
	FsDeviceFile file;
	FsSCSIDevice *device;
	KeThread *thread;
	KeSpinlock request_lock;
	cdi_list_t requests;
};

static cdi_list_t cdromdevices = 0;

static int cdrom_read_sector(struct cdrom_device *device, uint32_t sector, char *buffer)
{
	FsSCSIPacket packet;
	memset(&packet, 0, sizeof(FsSCSIPacket));
	packet.direction = FS_SCSI_READ;
	packet.buffer = buffer;
	packet.length = 2048;
	packet.commandsize = 12;
	((union cdrom_command*)packet.command)->ext.opcode = 0xA8;
	((union cdrom_command*)packet.command)->ext.address = (sector << 24) + ((sector & 0xFF00) << 8) + ((sector & 0xFF0000) >> 8) + ((sector & 0xFF000000) >> 24);
	((union cdrom_command*)packet.command)->ext.length = 0x01000000;
	if (device->device->request(device->device, &packet))
	{
		kePrint("cdrom: Read failed.\n");
		return KE_ERROR_UNKNOWN;
	}
	return 0;
}

static uint32_t cdrom_read(struct cdrom_device *device, uint32_t offset, uint32_t length, char *buffer)
{
	char *sector = malloc(2048);
	uint32_t read = 0;
	if (offset & 2047)
	{
		// Read partial first block
		if (cdrom_read_sector(device, offset / 2048, sector))
		{
			free(sector);
			return 0;
		}
		if (length < 2048 - (offset & 2047))
		{
			memcpy(buffer, sector + (offset & 2047), length);
			free(sector);
			return length;
		}
		memcpy(buffer, sector + (offset & 2047), 2048 - (offset & 2047));
		length -= 2048 - (offset & 2047);
		buffer += 2048 - (offset & 2047);
		offset += 2048 - (offset & 2047);
		read = 2048 - (offset & 2047);
	}
	while (length > 2048)
	{
		// Read complete block
		if (cdrom_read_sector(device, offset / 2048, sector))
		{
			free(sector);
			return read;
		}
		memcpy(buffer, sector, 2048);
		offset += 2048;
		length -= 2048;
		buffer += 2048;
		read += 2048;
	}
	if (length)
	{
		// Read partial last block
		if (cdrom_read_sector(device, offset / 2048, sector))
		{
			free(sector);
			return read;
		}
		memcpy(buffer, sector, length);
		read += length;
	}
	free(sector);
	return read;
}

static void cdrom_perform_request(struct cdrom_device *device,
	FsRequest *request)
{
	switch (request->type)
	{
		case FS_REQUEST_READ:
			request->return_value = cdrom_read(device, request->offset, request->bufferlength, request->buffer);
			fsFinishRequest(request);
			break;
		case FS_REQUEST_IOCTL:
			request->return_value = -1;
			fsFinishRequest(request);
			break;
		default:
			request->return_value = -1;
			fsFinishRequest(request);
	}
}

static void cdrom_thread(struct cdrom_device *device)
{
	while (1)
	{
		// Process requests
		keLockSpinlock(&device->request_lock);
		while (cdi_list_size(device->requests) > 0)
		{
			FsRequest *request = cdi_list_pop(device->requests);
			keUnlockSpinlock(&device->request_lock);
			
			cdrom_perform_request(device, request);
			
			keLockSpinlock(&device->request_lock);
		}
		// Wait until next request
		device->thread->status = KE_THREAD_FSREQUEST;
		keUnlockSpinlock(&device->request_lock);
		// Schedule out of the thread
		asm volatile("int $0x32");
	}
}

static int cdrom_request(struct FsDeviceFile *file, FsRequest *request)
{
	struct cdrom_device *device = (struct cdrom_device*)file;
	switch (request->type)
	{
		case FS_REQUEST_READ:
		case FS_REQUEST_IOCTL:
			keLockSpinlock(&device->request_lock);
			device->requests = cdi_list_push(device->requests, request);
			if (device->thread->status == KE_THREAD_FSREQUEST)
			{
				device->thread->status = KE_THREAD_RUNNING;
			}
			keUnlockSpinlock(&device->request_lock);
			return 0;
		case FS_REQUEST_SEEK:
			request->return_value = 0;
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_WRITE:
		default:
			return -1;
	}
}

static int cdrom_lock_device(FsSCSIDevice *device, int index, int lock)
{
	// Lock drive
	FsSCSIPacket packet;
	memset(&packet, 0, sizeof(FsSCSIPacket));
	packet.direction = FS_SCSI_NO_DATA;
	((union cdrom_command*)packet.command)->dfl.opcode = 0x1E;
	packet.command[4] = 1;
	packet.buffer = 0;
	packet.length = 0;
	packet.commandsize = 12;
	uint32_t status = device->request(device, &packet);
	if (status)
	{
		kePrint("cdrom %d: Lock failed, %x\n", index, status);
		fsCloseSCSIDevice(device);
		return KE_ERROR_UNKNOWN;
	}
	return 0;
}

/*static void cdrom_sense(FsSCSIDevice *device, uint32_t index)
{
	char sense_data[18];
	FsSCSIPacket packet;
	packet.direction = FS_SCSI_READ;
	packet.buffer = sense_data;
	packet.length = 18;
	packet.commandsize = 12;
	((union cdrom_command*)packet.command)->dfl.opcode = 0x12;
}*/

int cdrom_init(FsSCSIDevice *device, uint32_t index)
{
	// Send inquiry command
	FsSCSIPacket packet;
	uint32_t status;
	memset(&packet, 0, sizeof(FsSCSIPacket));
	unsigned char *inqdata = malloc(96);
	memset(inqdata, 0xF0, 96);
	packet.direction = FS_SCSI_READ;
	packet.buffer = inqdata;
	packet.length = 96;
	packet.commandsize = 12;
	((union cdrom_command*)packet.command)->dfl.opcode = 0x12;
	packet.command[4] = 96;
	status = device->request(device, &packet);
	if (status)
	{
		kePrint("cdrom %d: Inquiry failed, %x\n", index, status);
		fsCloseSCSIDevice(device);
		return KE_ERROR_UNKNOWN;
	}
	kePrint("cdrom%d: ", index);
	uint32_t i;
	for (i = 0; i < 8; i++)
	{
		char buffer[3];
		snprintf(buffer, 3, "%02x", inqdata[i]);
		kePrint("%s", buffer);
		if (!strcmp(buffer, "0"))
			kePrint("0");
	}
	kePrint("\n");
	free(inqdata);
	if (cdrom_lock_device(device, index, 1))
		return KE_ERROR_UNKNOWN;
	// Start drive
	packet.direction = FS_SCSI_NO_DATA;
	((union cdrom_command*)packet.command)->dfl.opcode = 0x1B;
	packet.buffer = 0;
	packet.length = 0;
	packet.command[4] = 0;
	packet.command[4] = 3;
	packet.commandsize = 12;
	status = device->request(device, &packet);
	if (status)
	{
		kePrint("cdrom %d: Start failed, %x\n", index, status);
		fsCloseSCSIDevice(device);
		return KE_ERROR_UNKNOWN;
	}
	
	// Add drive to device list
	struct cdrom_device *cdrom = malloc(sizeof(struct cdrom_device));
	cdrom->device = device;
	memset(&cdrom->file, 0, sizeof(cdrom->file));
	char *filename = malloc(7);
	snprintf(filename, 7, "cdrom%d", index);
	cdrom->file.path = filename;
	cdrom->file.query_request = cdrom_request;
	keInitSpinlock(&cdrom->request_lock);
	cdrom->requests = cdi_list_create();
	cdrom->thread = keCreateKernelThread((uintptr_t)cdrom_thread, 1, cdrom);
	fsCreateDeviceFile(&cdrom->file);
	if (!cdromdevices) cdromdevices = cdi_list_create();
	cdromdevices = cdi_list_push(cdromdevices, cdrom);
	
	return 0;
}

int modLoad(void)
{
	uint32_t scsicount = fsGetSCSIDeviceCount();
	if (scsicount == 0) return KE_ERROR_UNKNOWN;
	
	uint32_t i;
	for (i = 0; i < scsicount; i++)
	{
		FsSCSIDevice *device = fsOpenSCSIDevice(i);
		cdrom_init(device, i);
	}
	
	return 0;
}

int modUnload(void)
{
	return 0;
}

