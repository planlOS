
/*
 * Copyright (c) 2007 Kevin Wolf
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it 
 * and/or modify it under the terms of the Do What The Fuck You Want 
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/projects/COPYING.WTFPL for more details.
 */  

#ifndef CDI_H_INCLUDED
#define CDI_H_INCLUDED

#include <cdi/lists.h>

typedef enum
{
	CDI_UNKNOWN = 0,
	CDI_NETWORK = 1,
	CDI_STORAGE = 2,
	CDI_VIDEO = 3
} cdi_device_type_t;

struct cdi_driver;
struct cdi_device
{
	cdi_device_type_t type;
	const char *name;
	struct cdi_driver *driver;
};

struct cdi_driver
{
	cdi_device_type_t type;
	const char *name;
	cdi_list_t devices;

	void (*init_device)(struct cdi_device *device);
	void (*remove_device)(struct cdi_device *device);

	void (*destroy)(struct cdi_driver *driver);
};

/**
 * Has to be called before any other calls to CDI functions. Initialized the CDI
 * framework.
 *
 * Subsequent calls don't have any effect on the progam.
 */
void cdi_init(void);

/**
 * Executes all registered drivers. This function does not necessarily return to
 * the calling code.
 */
void cdi_run_drivers(void);

/**
 * Initializes the data structures for a driver.
 */
void cdi_driver_init(struct cdi_driver* driver);

/**
 * Destroys the data structures for a driver.
 */
void cdi_driver_destroy(struct cdi_driver* driver);

/**
 * Registers the driver for a new device.
 *
 * \param driver Driver to be registered
 */
void cdi_driver_register(struct cdi_driver* driver);

#endif

