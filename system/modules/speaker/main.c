/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ke/debug.h"
#include "ke/timer.h"
#include "ke/ports.h"

static KeTimer *speakertimer;
static KeTimer *freqtimer;

unsigned char data[256];
int currentframe = 0;

int phase = 0;

inline void speakerOn(void)
{
	uint8_t tmp = inb(0x61);
	tmp |= 0x2;
	outb(0x61, tmp);
}
inline void speakerOff(void)
{
	uint8_t tmp = inb(0x61);
	tmp &= ~0x2;
	outb(0x61, tmp);
}

static uint32_t rate = 1000000 / 40000;

static void timerCallback(struct KeTimer *timer)
{
	unsigned char sample = data[currentframe];
	
	if (phase == 0)
	{
		phase = 1;
		speakerOn();
		keAdjustTimer(timer, sample * rate / 256);
	}
	else
	{
		phase = 0;
		speakerOff();
		currentframe++;
		currentframe %= 256;
		keAdjustTimer(timer, rate - sample * rate / 256);
	}
}

int freq = 0;
static void changeFreq(struct KeTimer *timer)
{
	//kePrint("Changing frequency.\n");
	freq++;
	
	int i;
	for (i = 0; i < 256; i++)
	{
		data[i] = i * freq;
	}
	
	freq %= 10;
}

int modLoad(void)
{
	kePrint("Initializing speaker.\n");
	
	int i;
	for (i = 0; i < 256; i++)
	{
		data[i] = i * 2;
	}
	
	uint8_t tmp = inb(0x61);
	tmp |= 0x3;
	outb(0x61, tmp);
	
	// Beep:
	/*uint32_t max =  1193180;
	uint16_t counter = max / (10000 + (10000 * data[currentframe] / 256));
	
	outb(0x43, 0xB6);
	outb(0x42, counter);
	outb(0x42, counter >> 8);*/
	
	speakertimer = keCreateTimer();
	speakertimer->callback = timerCallback;
	keSetTimer(speakertimer, rate, 1);
	freqtimer = keCreateTimer();
	freqtimer->callback = changeFreq;
	keSetTimer(freqtimer, 2000000, 1);
	
	return 0;
}

int modUnload(void)
{
	return 0;
}

