/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/dma.h>
#include <ke/debug.h>
#include <ke/dma.h>

int cdi_dma_open(struct cdi_dma_handle* handle, uint8_t channel, uint8_t mode,
	size_t length, void* buffer)
{
	if (!handle) return -1;
	if (keOpenDMA(channel, mode, buffer, length)) return -1;
	// Save info in struct
	handle->channel = channel;
	handle->length = length;
	handle->mode = mode;
	handle->buffer = buffer;
	return 0;
}

int cdi_dma_read(struct cdi_dma_handle* handle)
{
	if (!handle) return -1;
	if (keReadDMA(handle->channel)) return -1;
	return 0;
}

int cdi_dma_write(struct cdi_dma_handle* handle)
{
	if (!handle) return -1;
	if (keWriteDMA(handle->channel)) return -1;
	return 0;
}

int cdi_dma_close(struct cdi_dma_handle* handle)
{
	if (!handle) return -1;
	if (keCloseDMA(handle->channel)) return -1;
	return 0;
}

