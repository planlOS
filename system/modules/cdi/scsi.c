/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/scsi.h>
#include <cdi.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fs/scsi.h>
#include <ke/errors.h>

struct scsi_device
{
	FsSCSIDevice scsidev;
	struct cdi_scsi_device *device;
};

static cdi_list_t scsidevices;

static int cdi_scsi_request(struct FsSCSIDevice *device, FsSCSIPacket *packet)
{
	struct scsi_device *dev = (struct scsi_device*)device;
	struct cdi_scsi_packet cdipacket;
	memset(&cdipacket, 0, sizeof(cdipacket));
	cdipacket.buffer = packet->buffer;
	cdipacket.bufsize = packet->length;
	cdipacket.cmdsize = packet->commandsize;
	cdipacket.direction = packet->direction;
	memcpy(cdipacket.command, packet->command, 16);
	int retval = ((struct cdi_scsi_driver*)(dev->device->dev.driver))->request(dev->device, &cdipacket);
	return retval;
}

void cdi_scsi_packet_free(struct cdi_scsi_packet *packet)
{
	free(packet);
}

void cdi_scsi_driver_init(struct cdi_scsi_driver *driver)
{
	cdi_driver_init((struct cdi_driver*)driver);
}

void cdi_scsi_driver_destroy(struct cdi_scsi_driver *driver)
{
	cdi_driver_destroy((struct cdi_driver*)driver);
}

void cdi_scsi_driver_register(struct cdi_scsi_driver *driver)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(driver->drv.devices); i++)
	{
		// Create device file
		struct cdi_scsi_device *device = cdi_list_get(driver->drv.devices, i);
		struct scsi_device *sd = malloc(sizeof(struct scsi_device));
		memset(sd, 0, sizeof(struct scsi_device));
		sd->device = device;
		sd->scsidev.request = cdi_scsi_request;
		fsRegisterSCSIDevice(&sd->scsidev);
		if (!scsidevices) scsidevices = cdi_list_create();
		scsidevices = cdi_list_push(scsidevices, sd);

	}
	
	cdi_driver_register(&driver->drv);
}

