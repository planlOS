/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/pci.h>
#include <ke/pci.h>
#include <stdlib.h>

static struct cdi_pci_device *devices = 0;

void cdi_pci_get_all_devices(cdi_list_t list)
{
	uint32_t devicecount = keGetPCIDeviceCount();
	if (!devices)
	{
		// Get device info
		devices = malloc(sizeof(struct cdi_pci_device) * devicecount);
		KePCIDevice *devinfo = keGetPCIDevices();
		uint32_t i;
		for (i = 0; i < devicecount; i++)
		{
			devices[i].bus = devinfo[i].bus;
			devices[i].dev = devinfo[i].slot;
			devices[i].function = devinfo[i].func;
			
			devices[i].vendor_id = devinfo[i].vendor;
			devices[i].device_id = devinfo[i].device;
			devices[i].class_id = ((uint16_t)devinfo[i].devclass << 8) + devinfo[i].subclass;
			devices[i].rev_id = devinfo[i].revision;
			
			devices[i].irq = devinfo[i].irq;
			devices[i].resources = cdi_list_create();
			
			// Insert resources
			int32_t j;
			for (j = devinfo[i].rescount; j >= 0; j--)
			{
				struct cdi_pci_resource *res = malloc(sizeof(struct cdi_pci_resource));
				res->type = devinfo[i].res[j].type;
				res->start = devinfo[i].res[j].addr;
				res->length = devinfo[i].res[j].size;
				res->address = 0;
				devices[i].resources = cdi_list_push(devices[i].resources, res);
			}
		}
	}
	// Add devices to list
	uint32_t i;
	for (i = 0; i < devicecount; i++)
	{
		cdi_list_push(list, &devices[i]);
	}
}

void cdi_pci_device_destroy(struct cdi_pci_device *device)
{
}

void cdi_pci_alloc_ioports(struct cdi_pci_device *device)
{
}

void cdi_pci_free_ioports(struct cdi_pci_device *device)
{
}

void cdi_pci_alloc_memory(struct cdi_pci_device *device)
{
}

void cdi_pci_free_memory(struct cdi_pci_device *device)
{
}

