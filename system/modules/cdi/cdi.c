/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi.h>

static cdi_list_t drivers;

void cdi_destroy(void)
{
	// TODO
}

void cdi_init(void)
{
	drivers = cdi_list_create();
}

void cdi_run_drivers(void)
{
	// Initialize all devices
	uint32_t i;
	for (i = 0; i < cdi_list_size(drivers); i++)
	{
		struct cdi_driver *driver = cdi_list_get(drivers, i);
		uint32_t j;
		for (j = 0; j < cdi_list_size(driver->devices); j++)
		{
			struct cdi_device *device = cdi_list_get(driver->devices, j);
			device->driver = driver;
			
			// Initialize device
			if (driver->init_device)
			{
				driver->init_device(device);
			}
		}
	}
}

void cdi_driver_init(struct cdi_driver* driver)
{
	driver->devices = cdi_list_create();
}

void cdi_driver_destroy(struct cdi_driver* driver)
{
	cdi_list_destroy(driver->devices);
}

void cdi_driver_register(struct cdi_driver* driver)
{
	drivers = cdi_list_push(drivers, driver);
}

