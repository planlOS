/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/misc.h>
#include <ke/debug.h>
#include <ke/interrupts.h>
#include <ke/level.h>
#include <mm/memory.h>

struct
{
	struct cdi_device *device;
	void (*handler)(struct cdi_device*);
} interrupt_handlers[16];

void cdi_irq_handler(KeCPU *cpu, uint8_t intno)
{
	interrupt_handlers[intno].handler(interrupt_handlers[intno].device);
}

void cdi_register_irq(uint8_t irq, void (*handler)(struct cdi_device*), 
	struct cdi_device* device)
{
	interrupt_handlers[irq].device = device;
	interrupt_handlers[irq].handler = handler;
	keRegisterIRQ(irq, cdi_irq_handler);
}
    
int cdi_alloc_phys_mem(size_t size, void** vaddr, void** paddr)
{
	KeExecLevel oldlevel = keSetExecutionLevel(KE_LEVEL_HIGH);
	keLockSpinlock(mmGetMemoryLock());
	// Allocate physical memory
	*paddr = (void*)mmAllocPhysicalMemory(MM_MEMORY_ALLOC_DMA, 0, (size + 0xFFF) & ~0xFFF);
	if (!*paddr)
	{
		keUnlockSpinlock(mmGetMemoryLock());
		keSetExecutionLevel(oldlevel);
		return -1;
	}
	// Allocate virtual memory
	*vaddr = (void*)mmFindFreeKernelPages(MM_MAX_KERNEL_PAGE, MM_MIN_KERNEL_PAGE, 1, (size + 0xFFF) & ~0xFFF);
	if (!*vaddr)
	{
		keUnlockSpinlock(mmGetMemoryLock());
		keSetExecutionLevel(oldlevel);
		return -1;
	}
	// Map memory
	uint32_t pagecount = (size + 0xFFF) / 0x1000;
	uint32_t i;
	for (i = 0; i < pagecount; i++)
	{
		mmMapKernelMemory((uintptr_t)*paddr + i * 0x1000, (uintptr_t)*vaddr + i * 0x1000, MM_MAP_READ | MM_MAP_WRITE);
	}
	keUnlockSpinlock(mmGetMemoryLock());
	keSetExecutionLevel(oldlevel);
	return 0;
}

void* cdi_alloc_phys_addr(size_t size, uintptr_t paddr)
{
	kePrint("cdi_alloc_phys_addr unimplemented.\n");
	return 0;
}

int cdi_ioports_alloc(uint16_t start, uint16_t count)
{
	// TODO?
	return 0;
}

int cdi_ioports_free(uint16_t start, uint16_t count)
{
	// TODO?
	return 0;
}

void cdi_sleep_ms(uint32_t ms)
{
	keSleep(ms);
}

static int is_leapyear(unsigned int year)
{
	if (year % 400 == 0) return 1;
	if (year % 100 == 0) return 0;
	if (year % 4 == 0) return 1;
	return 0;
}

static unsigned int month_get_days(unsigned int year, unsigned int month)
{
	int leapyear = is_leapyear(year);
	unsigned int days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if (leapyear) days[1]++;
	return days[month];
}

uint64_t cdi_time_by_date(unsigned int year, unsigned int month,
	unsigned int day)
{
	uint64_t time = 0;
	uint32_t i;
	for (i = 1970; i < year; i++)
	{
		time += 3600 * 24 * 365;
		if (is_leapyear(year)) time += 3600 * 24;
	}
	for (i = 0; i < month - 1; i++)
	{
		time += 3600 * 24 * month_get_days(year, i);
	}
	time += day * 3600 * 24;
	return time;
}

