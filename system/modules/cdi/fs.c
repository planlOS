/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/fs.h>
#include <ke/debug.h>
#include <ke/errors.h>
#include <ke/spinlock.h>
#include <fs/request.h>
#include <fs/fs.h>
#include <stdlib.h>
#include <string.h>

struct file_system_driver
{
	FsFileSystemDriver driver;
	struct cdi_fs_driver *cdidriver;
	cdi_list_t filesystems;
};
static cdi_list_t fsdrivers = 0;

struct file_system
{
	FsFileSystem fs;
	struct cdi_fs_filesystem cdifs;
};

struct file
{
	FsFile file;
	struct cdi_fs_res *res;
	int type;
	cdi_list_t dirlist;
	uint32_t mode;
};

#define FILE_TYPE_FILE 0
#define FILE_TYPE_DIR 1

KeSpinlock request_lock;

/*static void cdi_fs_check_filesystem_part(struct cdi_fs_filesystem *fs, struct cdi_fs_res *top, char *msg)
{
	struct cdi_fs_res *res = top;
	struct cdi_fs_stream stream;
	stream.fs = fs;
	stream.res = res;
	stream.error = 0;
	// Loop through children
	if (!stream.res->loaded) stream.res->res->load(&stream);
	uint32_t i;
	for (i = 0; i < cdi_list_size(res->children); i++)
	{
		struct cdi_fs_res *child = cdi_list_get(res->children, i);
		if (child->name < 0xC0000000)
		{
			kePrint("%s Child of %s invalid: %x\n", msg, res->name, child->name);
			kePanic(0, 10);
		}
		cdi_fs_check_filesystem_part(fs, child, msg);
	}
}
static void cdi_fs_check_filesystem(struct cdi_fs_filesystem *fs, char *msg)
{
	cdi_fs_check_filesystem_part(fs, fs->root_res, msg);
}*/

static struct cdi_fs_res *cdi_fs_get_file(struct cdi_fs_filesystem *fs, char *path)
{
	char *tmppath = strdup(path);
	char *currentpath = tmppath;
	struct cdi_fs_res *res = fs->root_res;
	struct cdi_fs_stream stream;
	stream.fs = fs;
	stream.res = res;
	stream.error = 0;
	// Loop through parts of the path
	do
	{
		char *nextpath = strchr(currentpath, '/');
		if (nextpath)
		{
			*nextpath = 0;
			nextpath++;
		}
		if (!stream.res->loaded) stream.res->res->load(&stream);
		uint32_t i;
		uint32_t found = 0;
		for (i = 0; i < cdi_list_size(res->children); i++)
		{
			struct cdi_fs_res *child = cdi_list_get(res->children, i);
			if (!strcmp(currentpath, child->name))
			{
				res = child;
				stream.res = child;
				currentpath = nextpath;
				found = 1;
				break;
			}
		}
		if (!found)
		{
			kePrint("directory: %s not found.\n", currentpath);
			return 0;
		}
	}
	while (currentpath);
	return res;
}

static int cdi_fs_open(struct FsFileSystem *fs, struct FsRequest *request)
{
	struct cdi_fs_filesystem *cdifs = &((struct file_system*)fs)->cdifs;
	char *path = strdup(request->buffer);
	// Get file
	struct cdi_fs_res *res = 0;
	struct cdi_fs_stream stream;
	stream.fs = cdifs;
	stream.error = 0;
	
	char *filename = 0;
	// Get parent directory
	if (strrchr(path, '/'))
	{
		filename = strrchr(path, '/') + 1;
		*strrchr(path, '/') = 0;
		res = cdi_fs_get_file(cdifs, path);
		if (!res)
		{
			free(path);
			return -1;
		}
		stream.res = res;
	}
	else
	{
		filename = path;
		res = cdifs->root_res;
		stream.res = res;
	}
	
	if (!stream.res->loaded) stream.res->res->load(&stream);
	if (strcmp(filename, ""))
	{
		// Look for file in directory
		uint32_t found = 0;
		uint32_t i;
		for (i = 0; i < cdi_list_size(res->children); i++)
		{
			struct cdi_fs_res *child = cdi_list_get(res->children, i);
			if (!strcmp(filename, child->name))
			{
				found = 1;
				res = child;
				if (!res->loaded) res->res->load(&stream);
				break;
			}
		}
		if (!found)
		{
			if (request->flags & FS_OPEN_CREATE)
			{
				kePrint("Creating file %s.\n", filename);
				stream.res = 0;
				// Create file
				res->dir->create_child(&stream, filename, res);
				if (!stream.res->loaded) stream.res->res->load(&stream);
				stream.res->res->assign_class(&stream, CDI_FS_CLASS_FILE);
				res = stream.res;
			}
			else
			{
				kePrint("cdi_fs: File not found.\n");
				free(path);
				return -1;
			}
		}
	}
	// Fill file structure
	struct file *file = malloc(sizeof(struct file));
	memset(file, 0, sizeof(struct file));
	file->res = res;
	if (res->dir)
		file->type = FILE_TYPE_DIR;
	else
		file->type = FILE_TYPE_FILE;
	file->file.fs = fs;
	file->mode = request->flags;
	cdifs->files = cdi_list_push(cdifs->files, file);
	free(path);
	request->file = &file->file;
	return 0;
}
static int cdi_fs_close(struct FsFileSystem *fs, struct FsRequest *request)
{
	struct cdi_fs_filesystem *cdifs = &((struct file_system*)fs)->cdifs;
	// Find file
	uint32_t i;
	for (i = 0; i < cdi_list_size(cdifs->files); i++)
	{
		struct file *file = cdi_list_get(cdifs->files, i);
		if (&file->file == request->file)
		{
			// Close resource and delete file
			struct cdi_fs_stream stream;
			stream.res = file->res;
			stream.fs = cdifs;
			stream.error = 0;
			// FIXME: The file might be opened twice, or there are children opened
			//file->res->res->unload(&stream);
			//if (file->dirlist) cdi_list_destroy(file->dirlist);
			free(file);
			cdi_list_remove(cdifs->files, i);
			return 0;
		}
	}
	return -1;
}
static int cdi_fs_mknod(struct FsFileSystem *fs, struct FsRequest *request)
{
	struct cdi_fs_filesystem *cdifs = &((struct file_system*)fs)->cdifs;
	char *path = strdup(request->buffer);
	// Get file
	struct cdi_fs_res *res = 0;
	struct cdi_fs_stream stream;
	stream.fs = cdifs;
	stream.error = 0;
	
	char *filename = 0;
	// Get parent directory
	if (strrchr(path, '/'))
	{
		filename = strrchr(path, '/') + 1;
		*strrchr(path, '/') = 0;
		res = cdi_fs_get_file(cdifs, path);
		if (!res)
		{
			free(path);
			return -1;
		}
		stream.res = res;
	}
	else
	{
		filename = path;
		res = cdifs->root_res;
		stream.res = res;
	}
	
	if (!stream.res->loaded) stream.res->res->load(&stream);
	if (strcmp(filename, ""))
	{
		// Look for file in directory
		uint32_t found = 0;
		uint32_t i;
		for (i = 0; i < cdi_list_size(res->children); i++)
		{
			struct cdi_fs_res *child = cdi_list_get(res->children, i);
			if (!strcmp(filename, child->name))
			{
				return -1;
			}
		}
		if (!found)
		{
			kePrint("Creating file %s.\n", filename);
			stream.res = 0;
			// Create file
			res->dir->create_child(&stream, filename, res);
			if (!stream.res->loaded) stream.res->res->load(&stream);
			if (request->flags == FS_MKNOD_FILE)
				stream.res->res->assign_class(&stream, CDI_FS_CLASS_FILE);
			else if (request->flags == FS_MKNOD_DIR)
				stream.res->res->assign_class(&stream, CDI_FS_CLASS_DIR);
			else
				return -1;
			return 0;
		}
	}
	return -1;
}

static int cdi_fs_read(struct FsFileSystem *fs, struct FsRequest *request)
{
	struct cdi_fs_filesystem *cdifs = &((struct file_system*)fs)->cdifs;
	struct file *file = (struct file*)request->file;
	if (file->type == FILE_TYPE_FILE)
	{
		if (!(file->mode & FS_OPEN_READ)) return -1;
		// Read data from file
		struct cdi_fs_stream stream;
		stream.res = file->res;
		stream.fs = cdifs;
		stream.error = 0;
		if (!file->res->file->read) return -1;
		ssize_t ret = file->res->file->read(&stream, request->offset,
			request->bufferlength, request->buffer);
		if (ret <= 0)
		{
			if ((stream.error == CDI_FS_ERROR_EOF) || (stream.error == 0))
				return 0;
			else
				return -1;
		}
		return ret;
	}
	else
	{
		struct cdi_fs_stream stream;
		stream.res = file->res;
		stream.fs = cdifs;
		stream.error = 0;
		if (request->bufferlength < 1) return -1;
		if (!file->dirlist)
		{
			if (file->res->dir == 0) return -1;
			file->dirlist = file->res->dir->list(&stream);
		}
		if (request->offset < 2)
		{
			strncpy(request->buffer, request->offset?"..":".", request->bufferlength);
			return 1;
		}
		struct cdi_fs_res *child = cdi_list_get(file->dirlist, request->offset - 2);
		if (child)
		{
			strncpy(request->buffer, child->name, request->bufferlength);
			return 1;
		}
		else return 0;
	}
}
static int cdi_fs_write(struct FsFileSystem *fs, struct FsRequest *request)
{
	struct cdi_fs_filesystem *cdifs = &((struct file_system*)fs)->cdifs;
	struct file *file = (struct file*)request->file;
	if (file->type != FILE_TYPE_FILE) return -1;
	if (!(file->mode & FS_OPEN_WRITE)) return -1;
	// Write data to file
	struct cdi_fs_stream stream;
	stream.res = file->res;
	stream.fs = cdifs;
	stream.error = 0;
	if (!file->res->file->write) return -1;
	ssize_t ret = file->res->file->write(&stream, request->offset,
		request->bufferlength, request->buffer);
	if (ret == 0)
	{
		return -1;
	}
	return ret;
}
static int cdi_fs_seek(struct FsFileSystem *fs, struct FsRequest *request)
{
	struct cdi_fs_filesystem *cdifs = &((struct file_system*)fs)->cdifs;
	struct file *file = (struct file*)request->file;
	// Get file size
	struct cdi_fs_stream stream;
	stream.res = file->res;
	stream.fs = cdifs;
	stream.error = 0;
	if (file->type == FILE_TYPE_FILE)
	{
		int64_t size = file->res->res->meta_read(&stream, CDI_FS_META_SIZE);
		kePrint("Seek: File size: %d\n", (int)size);
		// TODO: These casts to int are broken
		switch (request->whence)
		{
			case 0:
				if ((int)request->offset > size)
					request->offset = size;
				if ((int)request->offset < 0)
					request->offset = 0;
				return 0;
			case 1:
				if ((int)request->offset > size)
					request->offset = size;
				if ((int)request->offset < 0)
					request->offset = 0;
				return 0;
			case 2:
				if ((int)request->offset > 0)
					request->offset = 0;
				if ((int)request->offset < -size)
					request->offset = -size;
				request->offset = (int)request->offset + size;
				return 0;
			default:
				request->offset = 0;
				return -1;
		}
	}
	else
	{
		int64_t size = cdi_list_size(file->dirlist) + 2;
		// TODO: These casts to int are broken
		switch (request->whence)
		{
			case 0:
				if ((int)request->offset > size)
					request->offset = size;
				if ((int)request->offset < 0)
					request->offset = 0;
				return 0;
			case 1:
				if ((int)request->offset > size)
					request->offset = size;
				if ((int)request->offset < 0)
					request->offset = 0;
				return 0;
			case 2:
				if ((int)request->offset > 0)
					request->offset = 0;
				if ((int)request->offset < -size)
					request->offset = -size;
				request->offset = (int)request->offset + size;
				return 0;
			default:
				request->offset = 0;
				return -1;
		}
	}
}

static int cdi_fs_unmount(struct FsFileSystem *fs)
{
	// TODO
	return KE_ERROR_UNKNOWN;
}
static int cdi_fs_request(struct FsFileSystem *fs, struct FsRequest *request)
{
	if (!request) return -1;
	keLockSpinlock(&request_lock);
	switch (request->type)
	{
		case FS_REQUEST_OPEN:
			request->return_value = cdi_fs_open(fs, request);
			keUnlockSpinlock(&request_lock);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_CLOSE:
			request->return_value = cdi_fs_close(fs, request);
			keUnlockSpinlock(&request_lock);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_MKNOD:
			request->return_value = cdi_fs_mknod(fs, request);
			keUnlockSpinlock(&request_lock);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_READ:
			request->return_value = cdi_fs_read(fs, request);
			keUnlockSpinlock(&request_lock);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_WRITE:
			request->return_value = cdi_fs_write(fs, request);
			keUnlockSpinlock(&request_lock);
			fsFinishRequest(request);
			return 0;
		case FS_REQUEST_SEEK:
			request->return_value = cdi_fs_seek(fs, request);
			keUnlockSpinlock(&request_lock);
			fsFinishRequest(request);
			return 0;
		default:
			keUnlockSpinlock(&request_lock);
			return KE_ERROR_UNKNOWN;
	}
}

static FsFileSystem *cdi_fs_mount(FsFileSystemDriver *driver, const char *path,
	const char *device, uint32_t flags)
{
	struct file_system_driver *fsdrv = (struct file_system_driver*)driver;
	// Create file system
	struct file_system *fs = malloc(sizeof(struct file_system));
	memset(fs, 0, sizeof(struct file_system));
	fs->cdifs.driver = fsdrv->cdidriver;
	fs->cdifs.files = cdi_list_create();
	fs->fs.path = strdup(path);
	fs->fs.unmount = cdi_fs_unmount;
	fs->fs.query_request = cdi_fs_request;
	if (device && strcmp(device, ""))
	{
		// Open source device
		fs->fs.device = fsOpen(device, 0);
		if (!fs->fs.device)
		{
			free(fs->fs.path);
			free(fs);
			return 0;
		}
		fs->cdifs.data = fs->fs.device;
	}
	// Initialize file system
	if (fsdrv->cdidriver->fs_init(&fs->cdifs))
	{
		fsdrv->filesystems = cdi_list_push(fsdrv->filesystems, fs);
	}
	else
	{
		free(fs->fs.path);
		free(fs);
		return 0;
	}
	return &fs->fs;
}

void cdi_fs_driver_init(struct cdi_fs_driver *driver)
{
	cdi_driver_init(&driver->drv);
}
void cdi_fs_driver_destroy(struct cdi_fs_driver *driver)
{
	cdi_driver_destroy(&driver->drv);
}
void cdi_fs_driver_register(struct cdi_fs_driver *driver)
{
	cdi_driver_register(&driver->drv);
	kePrint("File system: %s\n", driver->drv.name);
	struct file_system_driver *fsdrv = malloc(sizeof(struct file_system_driver));
	memset(fsdrv, 0, sizeof(struct file_system_driver));
	fsdrv->cdidriver = driver;
	fsdrv->driver.mount = cdi_fs_mount;
	fsdrv->filesystems = cdi_list_create();
	if (!fsdrivers) fsdrivers = cdi_list_create();
	fsdrivers = cdi_list_push(fsdrivers, driver);
	fsRegisterDriver(&fsdrv->driver, driver->drv.name);
}


size_t cdi_fs_data_read(struct cdi_fs_filesystem *fs, uint64_t start,
	size_t size, void *buffer)
{
	if (!fs->data) return 0;
	fsSeek(fs->data, start, 0);
	int read = fsRead(fs->data, buffer, size, 1);
	return read;
}

size_t cdi_fs_data_write(struct cdi_fs_filesystem* fs, uint64_t start,
	size_t size, const void* buffer)
{
	if (!fs->data) return 0;
	fsSeek(fs->data, start, 0);
	int written = fsWrite(fs->data, (void*)buffer, size);
	return written;
}

