/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/storage.h>
#include <fs/devfs.h>
#include <fs/request.h>
#include <stdlib.h>
#include <string.h>
#include <ke/spinlock.h>
#include <ke/debug.h>

struct storage_device
{
	FsDeviceFile file;
	struct cdi_storage_device *device;
	KeThread *thread;
	KeSpinlock request_lock;
	cdi_list_t requests;
};

static cdi_list_t msdevices = 0;

static uint32_t cdi_storage_read(struct storage_device *msd, uint32_t offset, uint32_t length, void *buffer)
{
	struct cdi_storage_device *device = msd->device;
	struct cdi_storage_driver *driver = (struct cdi_storage_driver*)device->dev.driver;
	
	size_t block_size = device->block_size;
	// Blocks to be read
	uint64_t block_read_start = offset / block_size;
	uint64_t block_read_end = (offset + length) / block_size;
	uint64_t block_read_count = block_read_end - block_read_start;
	
	if (((offset % block_size) == 0) && (((offset + length) %  block_size) == 0))
	{
		// Only read whole blocks
		if (driver->read_blocks(device, block_read_start, block_read_count, buffer) != 0)
		{
			return -1;
		}
		return length;
	}
	else
	{
		// TODO: Optimize this
		block_read_count++;
		uint8_t buffer[block_read_count * block_size];
		
		// Buffer data
		if (driver->read_blocks(device, block_read_start, block_read_count, buffer) != 0)
		{
			return -1;
		}
		
		// Copy data from buffer
		memcpy(buffer, buffer + (offset % block_size), length);
	}
	return length;
}
static uint32_t cdi_storage_write(struct storage_device *msd, uint32_t offset, uint32_t length, void *data)
{
	struct cdi_storage_device *device = msd->device;
	struct cdi_storage_driver *driver = (struct cdi_storage_driver*)device->dev.driver;

	size_t block_size = device->block_size;
	uint64_t block_write_start = offset / block_size;
	uint8_t buffer[block_size];
	size_t blockoffset;
	size_t tmp_size;

	// Wenn die Startposition nicht auf einer Blockgrenze liegt, muessen wir
	// hier zuerst den ersten Block laden, die gewuenschten Aenderungen machen,
	// und den Block wieder Speichern.
	blockoffset = (offset % block_size);
	if (blockoffset != 0)
	{
		tmp_size = block_size - blockoffset;
		tmp_size = (tmp_size > length ? length : tmp_size);

		if (driver->read_blocks(device, block_write_start, 1, buffer) != 0) {
			return -1;
		}
		memcpy(data + blockoffset, data, tmp_size);

		// Buffer abspeichern
		if (driver->write_blocks(device, block_write_start, 1, buffer) != 0)
		{
			return -1;
		}
		
		length -= tmp_size;
		data += tmp_size;
		block_write_start++;
	}
	
	// Jetzt wird die Menge der ganzen Blocks auf einmal geschrieben, falls
	// welche existieren
	tmp_size = length / block_size;
	if (tmp_size != 0) {
		// Buffer abspeichern
		if (driver->write_blocks(device, block_write_start, tmp_size, data) != 0) 
		{
			return -1;
		}
		length -= tmp_size * block_size;
		data += tmp_size * block_size;
		block_write_start += block_size;        
	}

	// Wenn der letzte Block nur teilweise beschrieben wird, geschieht das hier
	if (length != 0) {
		// Hier geschieht fast das Selbe wie oben beim ersten Block
		if (driver->read_blocks(device, block_write_start, 1, buffer) != 0) {
			return -1;
		}
		memcpy(data, buffer, length);

		// Buffer abspeichern
		if (driver->write_blocks(device, block_write_start, 1, buffer) != 0) {
			return -1;
		}
	}
	return 0;
}
static void cdi_storage_perform_request(struct storage_device *device,
	FsRequest *request)
{
	switch (request->type)
	{
		case FS_REQUEST_READ:
			request->return_value = cdi_storage_read(device, request->offset, request->bufferlength, request->buffer);
			fsFinishRequest(request);
			break;
		case FS_REQUEST_WRITE:
			request->return_value = cdi_storage_write(device, request->offset, request->bufferlength, request->buffer);
			fsFinishRequest(request);
			break;
		case FS_REQUEST_IOCTL:
			kePrint("IOCTL.\n");
			fsFinishRequest(request);
			break;
		default:
			fsFinishRequest(request);
	}
}

static void cdi_storage_thread(struct storage_device *device)
{
	//kePrint("Worker thread: %s\n", device->file.path);
	while (1)
	{
		// Process requests
		keLockSpinlock(&device->request_lock);
		while (cdi_list_size(device->requests) > 0)
		{
			FsRequest *request = cdi_list_pop(device->requests);
			keUnlockSpinlock(&device->request_lock);
			
			kePrint("Performing request on %s\n", device->file.path);
			cdi_storage_perform_request(device, request);
			
			keLockSpinlock(&device->request_lock);
		}
		// Wait until next request
		//kePrint("Pausing worker thread.\n");
		device->thread->status = KE_THREAD_FSREQUEST;
		keUnlockSpinlock(&device->request_lock);
		// Schedule out of the thread
		asm volatile("int $0x32");
		//kePrint("Thread woke up.\n");
	}
}

static int cdi_storage_request(struct FsDeviceFile *file, FsRequest *request)
{
	struct storage_device *device = (struct storage_device*)file;
	switch (request->type)
	{
		case FS_REQUEST_READ:
		case FS_REQUEST_WRITE:
		case FS_REQUEST_IOCTL:
			keLockSpinlock(&device->request_lock);
			device->requests = cdi_list_push(device->requests, request);
			if (device->thread->status == KE_THREAD_FSREQUEST)
			{
				kePrint("Waking up thread.\n");
				device->thread->status = KE_THREAD_RUNNING;
			}
			keUnlockSpinlock(&device->request_lock);
			return 0;
		case FS_REQUEST_SEEK:
			// TODO
			break;
		default:
			return -1;
	}
	return -1;
}

void cdi_storage_driver_init(struct cdi_storage_driver *driver)
{
	cdi_driver_init(&driver->drv);
}

void cdi_storage_driver_destroy(struct cdi_storage_driver *driver)
{
	cdi_driver_destroy(&driver->drv);
}

void cdi_storage_driver_register(struct cdi_storage_driver *driver)
{
	uint32_t i;
	for (i = 0; i < cdi_list_size(driver->drv.devices); i++)
	{
		// Create device file
		struct cdi_storage_device *device = cdi_list_get(driver->drv.devices, i);
		struct storage_device *sd = malloc(sizeof(struct storage_device));
		sd->device = device;
		memset(&sd->file, 0, sizeof(sd->file));
		sd->file.path = device->dev.name;
		sd->file.query_request = cdi_storage_request;
		keInitSpinlock(&sd->request_lock);
		sd->requests = cdi_list_create();
		sd->thread = keCreateKernelThread((uintptr_t)cdi_storage_thread, 1, sd);
		fsCreateDeviceFile(&sd->file);
		if (!msdevices) msdevices = cdi_list_create();
		msdevices = cdi_list_push(msdevices, sd);
	}
	
	cdi_driver_register(&driver->drv);
}

