/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cdi/net.h>
#include <net/net.h>
#include <string.h>
#include <stdlib.h>

struct network_device
{
	NetNetworkDevice dev;
	struct cdi_net_device *cdidev;
};

static struct network_device **devices = 0;
static uint32_t devicecount = 0;

static int cdi_net_send(NetNetworkDevice *device, void *data, int length)
{
	struct network_device *dev = (struct network_device*)device;
	dev->cdidev->send_packet(dev->cdidev, data, length);
	return length;
}

void cdi_net_driver_init(struct cdi_net_driver *driver)
{
	cdi_driver_init(&driver->drv);
}

void cdi_net_driver_destroy(struct cdi_net_driver *driver)
{
	cdi_driver_destroy(&driver->drv);
}

void cdi_net_device_init(struct cdi_net_device *device)
{
	// Create device struct
	struct network_device *dev = malloc(sizeof(struct network_device));
	memset(dev, 0, sizeof(struct network_device));
	dev->cdidev = device;
	dev->dev.mac = device->mac;
	dev->dev.send = cdi_net_send;
	// Add to list
	devices = realloc(devices, (devicecount + 1) * sizeof(struct network_device*));
	devices[devicecount] = dev;
	devicecount++;
	// Register network device
	netRegisterDevice(&dev->dev, "eth");
}

void cdi_net_receive(struct cdi_net_device *device, void *buffer, size_t size)
{
	uint32_t i;
	for (i = 0; i < devicecount; i++)
	{
		if (devices[i]->cdidev == device)
		{
			netReceiveData(&devices[i]->dev, buffer, size);
			return;
		}
	}
}

uint32_t string_to_ip(char *s)
{
	uint8_t ip[4] = {0, 0, 0, 0};
	int currentidx = 0;
	while (*s)
	{
		if (*s == '.')
		{
			currentidx++;
			if (currentidx == 4) break;
		}
		else if ((*s >= '0') && (*s <= '9'))
		{
			ip[currentidx] *= 10;
			ip[currentidx] += *s;
		}
		else break;
		s++;
	}
	return *(uint32_t*)ip;
}

