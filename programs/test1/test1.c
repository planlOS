
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>

void size_changed(int sig)
{
	struct winsize size;
	int status = ioctl(0, TIOCGWINSZ, &size);
	if (status != -1)
	{
		char tmp[30];
		snprintf(tmp, 30, "\033[%d;%dH", size.ws_row, size.ws_col);
		write(1, tmp, strlen(tmp));
		write(1, "*", 1);
	}
}

int main(int argc, char **argv)
{
	signal(SIGWINCH, size_changed);
	// Initialize screen
	write(1, "\033[2J", 4);
	write(1, "\033[0;0H", 6);
	write(1, "\033[30;41m", 8);
	write(1, "Hello ", 6);
	write(1, "\033[33;40m", 8);
	write(1, "World!\n", 7);
	
	struct winsize size;
	int status = ioctl(0, TIOCGWINSZ, &size);
	if (status != -1)
	{
		write(1, "\033[37;40m", 8);
		printf("Size: %d/%d.\n", size.ws_row, size.ws_col);
		write(1, "\033[53;180H", 9);
		write(1, "*", 1);
	}
	
	write(1, "\033[4;0H", 6);
	int i;
	for (i = 0; i < argc; i++)
	{
		printf("Arg. %d: %s\n", i, argv[i]);
	}
	
	int time1 = time(0);
	struct timeval tv1;
	gettimeofday(&tv1, 0);
	write(1, "\033[30;41m", 8);
	write(1, "fork()!       \n", 15);
	for (i = 0; i < 1000; i++)
	{
		int pid = fork();
		if (!pid)
		{
			exit(0);
		}
		waitpid(pid, 0, 0);
	}
	write(1, "DONE!         \n", 15);
	int time2 = time(0);
	struct timeval tv2;
	gettimeofday(&tv2, 0);
	printf("%d seconds (%d).\n", time2 - time1, time2);
	
	if (tv1.tv_usec > tv2.tv_usec)
	{
		tv2.tv_usec += 1000000;
		tv2.tv_sec -= 1;
	}
	int difference = tv2.tv_usec / 1000 - tv1.tv_usec / 1000 + tv2.tv_sec * 1000 - tv1.tv_sec * 1000;
	write(1, "\033[7;0H", 6);
	printf("%d seconds.\n", difference);
	
	char c;
	while ((c = getchar()) != 'q')
	{
		putchar(c);
	}
	return 0;
}

