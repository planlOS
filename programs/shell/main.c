
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>

static int current_child = -1;

void sigint_handler(int signal)
{
	if (current_child != -1)
	{
		kill(current_child, SIGINT);
	}
}

int main(int argc, char **argv)
{
	signal(SIGINT, sigint_handler);
	
	char command[256] = "";
	while (1)
	{
		char cwd[256];
		getcwd(cwd, 256);
		write(1, cwd, strlen(cwd));
		write(1, ">", 1);
		// Read in command
		int length = 0;
		while (length < 255)
		{
			read(0, command + length, 1);
			if (command[length] == '\n')
				break;
			length++;
		}
		command[length] = 0;
		if (!strcmp(command, "")) continue;
		if (!strcmp(command, "exit")) break;
		if (!strncmp(command, "cd ", 3))
		{
			char *path = command + 3;
			if (chdir(path))
			{
				const char *msg = "Could not change directory.\n";
				write(1, msg, strlen(msg));
			}
			continue;
		}
		// Execute program
		current_child = fork();
		if (current_child == -1)
		{
			const char *msg = "Could not fork shell.\n";
			write(1, msg, strlen(msg));
			continue;
		}
		else if (!current_child)
		{
			// Run program
			if (execlp(command, command, NULL))
			{
				const char *msg = "Could not run program.\n";
				write(1, msg, strlen(msg));
				exit(1);
			}
		}
		else
		{
			// Wait for program to exit
			waitpid(current_child, 0, 0);
			current_child = -1;
		}
	}
	return 0;
}

