/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SYS_STAT_H_INCLUDED
#define SYS_STAT_H_INCLUDED

#include <sys/types.h>

#define S_IRWXO 0007
#define S_IROTH 0001
#define S_IWOTH 0002
#define S_IXOTH 0004

#define S_IRWXG 0070
#define S_IRGRP 0010
#define S_IWGRP 0020
#define S_IXGRP 0040

#define S_IRWXU 0700
#define S_IRUSR 0100
#define S_IWUSR 0200
#define S_IXUSR 0400

#define S_ISUID 01000
#define S_ISGID 02000

#define S_ISVTX 04000

#define S_IFMT 0xF00
#define S_IFBLK 0x100
#define S_IFCHR 0x200
#define S_IFIFO 0x300
#define S_IFREG 0x400
#define S_IFDIR 0x500
#define S_IFLNK 0x600
#define S_IFSOCK 0x700

#define S_ISBLK(m) ((m & S_IFMT) == S_IFBLK)
#define S_ISCHR(m) ((m & S_IFMT) == S_IFCHR)
#define S_ISDIR(m) ((m & S_IFMT) == S_IFDIR)
#define S_ISFIFO(m) ((m & S_IFMT) == S_IFIFO)
#define S_ISREG(m) ((m & S_IFMT) == S_IFREG)
#define S_ISLNK(m) ((m & S_IFMT) == S_IFLNK)

struct stat
{
	dev_t st_dev;
	ino_t st_ino;
	mode_t st_mode;
	nlink_t st_nlink;
	uid_t st_uid;
	gid_t st_gid;
	dev_t st_rdev;
	off_t st_size;
	time_t st_atime;
	time_t st_mtime;
	time_t st_ctime;
	blksize_t st_blksize;
	blkcnt_t st_blocks;
};
struct stat64
{
	dev_t st_dev;
	ino64_t st_ino;
	mode_t st_mode;
	nlink_t st_nlink;
	uid_t st_uid;
	gid_t st_gid;
	dev_t st_rdev;
	off64_t st_size;
	time_t st_atime;
	time_t st_mtime;
	time_t st_ctime;
	blksize64_t st_blksize;
	blkcnt_t st_blocks;
};
int mknod(const char *path, mode_t mode, dev_t dev);
int mkdir(const char *path, mode_t mode);

int chmod(const char *path, mode_t mode);
int fchmod(int fd, mode_t mode);
int fstat(int fd, struct stat *s);
int lstat(const char *path, struct stat *s);
int stat(const char *path, struct stat *s);
int fstat64(int fd, struct stat64 *s);
int stat64(const char *path, struct stat64 *s);

#endif

