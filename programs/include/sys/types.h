/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SYS_TYPES_H_INCLUDED
#define SYS_TYPES_H_INCLUDED

#include <stdint.h>

typedef uint32_t blkcnt_t;
typedef uint64_t blkcnt64_t;
typedef uint32_t blksize_t;
typedef uint64_t blksize64_t;
typedef int32_t clock_t;
typedef uint32_t clockid_t;
typedef uint32_t dev_t;
typedef int32_t sblkcnt_t;
typedef int64_t sblkcnt64_t;
typedef uint32_t fsfilcnt_t;
typedef uint32_t gid_t;
typedef uint32_t id_t;
typedef uint32_t ino_t;
typedef uint64_t ino64_t;
typedef uint32_t mode_t;
typedef uint32_t nlink_t;
typedef uint32_t off_t;
typedef uint64_t off64_t;
typedef int32_t pid_t;
typedef int32_t ssize_t;
typedef int64_t suseconds_t;
typedef uint32_t time_t;
typedef uint32_t timer_t;
typedef uint32_t trace_attr_t;
typedef uint32_t uid_t;
typedef uint64_t useconds_t;
typedef unsigned char u_char;

typedef uint32_t pthread_spinlock_t;

#endif

