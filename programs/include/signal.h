/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SIGNAL_H_INCLUDED
#define SIGNAL_H_INCLUDED

#include <sys/types.h>
#include <planlos/signals.h>

#define SIG_DFL (void(*)(int))(1)
#define SIG_ERR (void(*)(int))(2)
#define SIG_HOLD (void(*)(int))(3)
#define SIG_IGN (void(*)(int))(4)

#define SA_NOCLDSTOP 1
#define SIG_BLOCK 2
#define SIG_UNBLOCK 3
#define SIG_SETMASK 4
#define SA_ONSTACK 5
#define SA_RESETHAND 6
#define SA_RESTART 7
#define SA_SIGINFO 8
#define SA_NOCLDWAIT 9
#define SA_NODEFER 10
#define SS_ONSTACK 11
#define SS_DISABLE 12
#define MINSIGSTKSZ 13
#define SIGSTKSZ 14

#define BRKINT 1
#define ICRNL 2
#define IGNBRK 3
#define IGNCR 4
#define IGNPAR 5
#define INLCR 6
#define INPCK 7
#define ISTRIP 8
#define IXANY 9
#define IXOFF 10
#define IXON 11
#define PARMRK 12

#define OPOST 1

#define ECHO 1
#define ECHOE 2
#define ECHOK 3
#define ECHONL 4
#define ICANON 5
//#define IEXTEN 6
#define ISIG 7
#define NOFLSH 8
#define TOSTOP 9

#define CSIZE 1
#define CSTOPB 2
#define CREAD 3
//#define PARENB 4
#define PARODD 5
#define HUPCL 6
#define CLOCAL 7

#define CS5 5
#define CS6 6
#define CS7 7
#define CS8 8

#define TCSANOW 1
#define TCSADRAIN 2
#define TCSAFLUSH 3

typedef volatile int sig_atomic_t;
typedef int sigset_t;

union sigval
{
	int sival_int;
	void *sival_ptr;
};

typedef struct
{
	int si_signo;
	int si_code;
	int si_errno;
	pid_t si_pid;
	uid_t si_uid;
	void *si_addr;
	int si_status;
	long si_band;
	union sigval si_value;
} siginfo_t;

struct sigaction
{
	void (*sa_handler)(int);
	sigset_t sa_mask;
	int sa_flags;
	void (*sa_sigaction)(int, siginfo_t *, void *);
};

extern char **sys_siglist;
extern int sys_nerr;

int kill(pid_t pid, int sig);
int raise(int sig);
void (*signal(int sig, void (*func)(int)))(int);

int sigdelset(sigset_t *set, int signo);
int sigemptyset(sigset_t *set);
int sigfillset(sigset_t *set);

int sigpending(sigset_t *set);
int sigprocmask(int how, const sigset_t *set, sigset_t *oset);

int sigaddset(sigset_t *set, int signo);

int sigaction(int sig, const struct sigaction *act, struct sigaction *oact);

int sigismember(const sigset_t *set, int signo);

#endif

