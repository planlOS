/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef TERMIOS_H_INCLUDED
#define TERMIOS_H_INCLUDED

#include <sys/types.h>

typedef int cc_t;
typedef int speed_t;
typedef int tcflag_t;

#define B0 1
#define B50 2
#define B75 3
#define B110 4
#define B134 5
#define B150 6
#define B200 7
#define B300 8
#define B600 9
#define B1200 10
#define B1800 11
#define B2400 12
#define B4800 13
#define B9600 14
#define B19200 15
#define B38400 16

#define VEOF -1
#define VEOL '\n'
#define VERASE ' '
#define VINTR 0
#define VKILL 0
#define VMIN 0
#define VQUIT 0
#define VSTART 0
#define VSTOP 0
#define VSUSP 0
#define VTIME 0

#define NCCS 16

#define TCIFLUSH 1
#define TCIOFLUSH 2
#define TCOFLUSH 3

#define ECHO 1
#define ECHOE 2
#define ECHOK 3
#define ECHONL 4
#define ICANON 5
#define IEXTEN 6
#define ISIG 7
#define NOFLSH 8
#define TOSTOP 9
#define XCASE 10

#define CSIZE 1
#define CS5 5
#define CS6 6
#define CS7 7
#define CS8 8
#define CSTOPB 2
#define CREAD 3
#define PARENB 4
#define PARODD 5
#define HUPCL 6
#define CLOCAL 7
#define CBAUD 8

struct termios
{
	tcflag_t c_iflag;
	tcflag_t c_oflag;
	tcflag_t c_cflag;
	tcflag_t c_lflag;
	cc_t c_cc[NCCS];
};

speed_t cfgetispeed(const struct termios *t);
speed_t cfgetospeed(const struct termios *t);
int cfsetispeed(struct termios *t, speed_t speed);
int cfsetospeed(struct termios *t, speed_t speed);
int tcdrain(int fd);
int tcflow(int fd, int action);
int tcflush(int fd, int queue_selector);
int tcgetattr(int fd, struct termios *t);
pid_t tcgetsid(int fd);
int tcsendbreak(int fd, int dur);
int tcsetattr(int fd, int optional_actions, const struct termios *t);

#endif

