/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef UNISTD_H_INCLUDED
#define UNISTD_H_INCLUDED

#include <sys/types.h>

extern char **environ;

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

#define R_OK 1
#define W_OK 2
#define X_OK 4
#define F_OK 8

ssize_t write(int fildes, const void *buf, size_t nbyte);
ssize_t read(int fildes, void *buf, size_t nbyte);
int close(int fildes);
off_t lseek(int fildes, off_t offset, int whence);
int unlink(const char *filename);

unsigned sleep(unsigned seconds);

pid_t fork(void);

int pipe(int fd[2]);

int dup(int fd);
int dup2(int fd, int fd2);

int execl(const char *path, const char *arg0, ...);
int execle(const char *path, const char *arg0, ...);
int execlp(const char *file, const char *arg0, ...);
int execv(const char *path, char *const argv[]);
int execve(const char *file, char *const argv[], char *const env[]);
int execvp(const char *path, char *const argv[]);

char *getcwd(char *buf, size_t size);
int chdir(const char *path);

extern char *optarg;
extern int opterr;
extern int optind;
extern int optopt;
extern int optreset;

int getopt(int argc, char *const *argv, const char *optstring);

pid_t getpid(void);

int access(const char *path, int amode);

pid_t tcgetpgrp(int fd);

#endif

