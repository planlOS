/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FCNTL_H_INCLUDED
#define FCNTL_H_INCLUDED

#include <unistd.h>

#define F_DUPFD 1
#define F_GETFD 2
#define F_SETFD 3
#define F_GETFL 4
#define F_SETFL 5
#define F_GETLK 6
#define F_SETLK 7
#define F_SETLKW 8
#define F_GETOWN 9
#define F_SETOWN 10

#define FD_CLOEXEC 1

#define O_CREAT 0x1
#define O_EXCL 0x2
#define O_NOCTTY 0x4
#define O_TRUNC 0x8
#define O_APPEND 0x10
#define O_DSYNC 0x20
#define O_NONBLOCK 0x40
#define O_RSYNC 0x80
#define O_SYNC 0x100

#define O_ACCMODE 0xE00
#define O_RDONLY 0x200
#define O_RDWR 0x400
#define O_WRONLY 0x800

int creat(const char *path, mode_t mode);
int fcntl(int fd, int cmd, ...);
int open(const char *path, int oflag, ...);
int open64(const char *path, int oflag, ...);

#endif

