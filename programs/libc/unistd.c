/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <unistd.h>
#include <fcntl.h>
#include <planlos/syscalls.h>
#include <string.h>
#include <stdlib.h>

char **environ = 0;

pid_t fork(void)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_FORK));
	return status;
}

static char *exec_get_path(const char *file, const char *paths)
{
	while (strlen(paths) > 0)
	{
		const char *nextpath = strchr(paths, ':');
		if (!nextpath) nextpath = paths + strlen(paths);
		if (nextpath && (nextpath != paths))
		{
			char *path = malloc(strlen(file) + 1 + ((uintptr_t)nextpath - (uintptr_t)paths));
			memset(path, 0, strlen(file) + 1 + ((uintptr_t)nextpath - (uintptr_t)paths));
			strncpy(path, paths, ((uintptr_t)nextpath - (uintptr_t)paths));
			strcat(path, "/");
			strcat(path, file);
			// Test path
			int fd = open(path, O_RDONLY);
			if (fd != -1)
			{
				close(fd);
				return path;
			}
			
			free(path);
		}
		if (strchr(paths, ':'))
			paths = nextpath + 1;
		else
			break;
	}
	return 0;
}

int execl(const char *path, const char *arg0, ...)
{
	char **args = (char**)&arg0;
	return execv(path, args);
}
int execle(const char *path, const char *arg0, ...)
{
	char **args = (char**)&arg0;
	char **env = args;
	while (*env) env++;
	env++;
	return execve(path, args, env);
}
int execlp(const char *file, const char *arg0, ...)
{
	char **args = (char**)&arg0;
	return execvp(file, args);
}
int execv(const char *path, char *const argv[])
{
	int argcount;
	for (argcount = 0; argv[argcount] != 0; argcount++);
	char **env = environ;
	int envcount = 0;
	if (env)
		while (*env) envcount++;
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_EXEC), "b"(path),
		"c"(argcount), "d"(argv), "S"(envcount), "D"(environ));
	return status;
}
int execve(const char *path, char *const argv[], char *const env[])
{
	int argcount = 0;
	if (argv)
		for (; argv[argcount] != 0; argcount++);
	int envcount = 0;
	if (env)
		for (; env[envcount] != 0; envcount++);
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_EXEC), "b"(path),
		"c"(argcount), "d"(argv), "S"(envcount), "D"(env));
	return status;
}
int execvp(const char *file, char *const argv[])
{
	char *path = 0;
	if (!strchr(file, '/'))
	{
		char *pathvar = getenv("PATH");
		if (pathvar)
			path = exec_get_path(file, pathvar);
		if (!path)
			path = exec_get_path(file, "/bin:/usr/bin");
		if (!path) return -1;
	}
	if (!path)
		return execve(file, argv, 0);
	else
	{
		int result = execve(path, argv, 0);
		free(path);
		return result;
	}
}

char *getcwd(char *buf, size_t size)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_GETWD), "b"(buf), "c"(size));
	if (status == -1) return 0;
	return buf;
}
int chdir(const char *path)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_CHDIR), "b"(path));
	return status;
}
int dup(int fd)
{
	return fcntl(fd, F_DUPFD, 0);
}
int dup2(int fd, int fd2)
{
	return fcntl(fd, F_DUPFD, fd2);
}

pid_t getpid(void)
{
	int pid;
	asm volatile("int $0x80" : "=b"(pid): "a"(SYS_GETPID));
	return pid;

}

int isatty(int fildes)
{
	return 1;
}

int access(const char *path, int amode)
{
	return R_OK;
}

char *setlocale(int category, const char *locale)
{
	return 0;
}

pid_t tcgetpgrp(int fd)
{
	return -1;
}
pid_t getpgrp(void)
{
	return 0;
}

char *getenv(const char *name)
{
	if (!environ) return 0;
	char **env = environ;
	while (*env)
	{
		if (!strncmp(name, *env, (uintptr_t)strchr(*env, '=') - (uintptr_t)*env))
		{
			return strchr(*env, '=') + 1;
		}
	}
	return 0;
}

int putenv(char *string)
{
	char *value = strchr(string, '=');
	if (!value)
	{
		// Remove from environment
		int found = -1;
		int i = 0;
		char **env = environ;
		while (*env)
		{
			int namelength = (uintptr_t)strchr(*env, '=') - (uintptr_t)*env;
			if (namelength < (int)strlen(string)) continue;
			if (!strncmp(string, *env, (uintptr_t)strchr(*env, '=') - (uintptr_t)*env))
			{
				found = i;
			}
			i++;
		}
		
		if (found == -1) return -1;
		
		char **newenv = malloc(sizeof(char*) * i);
		memcpy(newenv, environ, found * sizeof(char*));
		memcpy(newenv + found, environ + found + 1, (i + 1 - found) * sizeof(char*));
		free(environ);
		environ = newenv;
		return 0;
	}
	// Set to new value
	char *name = strdup(string);
	*strchr(name, '=') = 0;
	int found = -1;
	int i = 0;
	char **env = environ;
	while (*env)
	{
		if (!strncmp(name, *env, (uintptr_t)strchr(*env, '=') - (uintptr_t)*env))
		{
			found = i;
		}
		i++;
	}
	int size = i;
	if (found == -1)
	{
		char **newenv = malloc(sizeof(char*) * (size + 2));
		memcpy(newenv, environ, sizeof(char*) * size);
		newenv[size] = string;
		newenv[size + 1] = 0;
		free(environ);
		environ = newenv;
	}
	else
	{
		environ[found] = string;
	}
	free(name);
	return 0;
}

int setenv(const char *name, const char *value, int overwrite)
{
	// Set to new value
	int found = -1;
	int i = 0;
	char **env = environ;
	while (*env)
	{
		if (!strncmp(name, *env, (uintptr_t)strchr(*env, '=') - (uintptr_t)*env))
		{
			found = i;
		}
		i++;
	}
	int size = i;
	if (found == -1)
	{
		char **newenv = malloc(sizeof(char*) * (size + 2));
		memcpy(newenv, environ, sizeof(char*) * size);
		newenv[size] = malloc(strlen(name) + strlen(value) + 2);
		strcpy(newenv[size], name);
		strcat(newenv[size], "=");
		strcat(newenv[size], value);
		newenv[size + 1] = 0;
		free(environ);
		environ = newenv;
	}
	else
	{
		environ[found] = malloc(strlen(name) + strlen(value) + 2);
		strcpy(environ[found], name);
		strcat(environ[found], "=");
		strcat(environ[found], value);
	}
	return 0;

}
int unsetenv(const char *name)
{
	// Remove from environment
	int found = -1;
	int i = 0;
	char **env = environ;
	if (!environ) return 0;
	while (*env)
	{
		if (!strncmp(name, *env, (uintptr_t)strchr(*env, '=') - (uintptr_t)*env))
		{
			found = i;
		}
		i++;
	}
	if (found == -1) return 0;
	
	char **newenv = malloc(sizeof(char*) * i);
	memcpy(newenv, environ, found * sizeof(char*));
	memcpy(newenv + found, environ + found + 1, (i + 1 - found) * sizeof(char*));
	free(environ);
	environ = newenv;
	return 0;
}

int clearenv(void)
{
	// TODO
	environ = NULL;
	return 0;
}

