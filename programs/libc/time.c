/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <planlos/syscalls.h>

time_t time(time_t *tloc)
{
	int seconds = 0;
	int useconds = 0;
	asm volatile("int $0x80" : "=b"(seconds), "=c"(useconds): "a"(SYS_TIME));
	return seconds;
}

unsigned sleep(unsigned seconds)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_SLEEP), "b"(seconds * 1000000));
	return 0;
}

int gettimeofday(struct timeval *t, void *tz)
{
	int seconds = 0;
	int useconds = 0;
	asm volatile("int $0x80" : "=b"(seconds), "=c"(useconds): "a"(SYS_TIME));
	t->tv_sec = seconds;
	t->tv_usec = useconds;
	return 0;
}

clock_t times(struct tms *buffer)
{
	return -1;
}

