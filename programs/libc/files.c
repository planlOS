/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <planlos/syscalls.h>

int open(const char *path, int oflag, ...)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_OPEN), "b"(path), "c"(oflag));
	return status;
}

int creat(const char *path, mode_t mode)
{
	return open(path, O_CREAT | O_TRUNC | O_WRONLY, mode);
}

ssize_t write(int fildes, const void *buf, size_t nbyte)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_WRITE), "b"(fildes), "c"(buf), "d"(nbyte));
	return status;
}

ssize_t read(int fildes, void *buf, size_t nbyte)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_READ), "b"(fildes), "c"(buf), "d"(nbyte));
	return status;
}

int close(int fildes)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_CLOSE), "b"(fildes));
	return status;
}

off_t lseek(int fildes, off_t offset, int whence)
{
	int position = -1;
	asm volatile("int $0x80" : "=b"(position): "a"(SYS_SEEK), "b"(fildes), "c"(offset), "d"(whence));
	return position;
}

int mknod(const char *path, mode_t mode, dev_t dev)
{
	int status = -1;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_MKNOD), "b"(path), "c"(mode));
	return status;
}
int mkdir(const char *path, mode_t mode)
{
	return mknod(path, mode | S_IFDIR, 0);
}

struct DIR
{
	int fd;
	struct dirent dirent;
};

#define O_DIR 0x1000

DIR *opendir(const char *path)
{
	int fd = open(path, O_RDONLY | O_DIR);
	if (fd == -1) return 0;
	DIR *dir = malloc(sizeof(DIR));
	dir->fd = fd;
	return dir;
}
int closedir(DIR *dir)
{
	close(dir->fd);
	free(dir);
	return 0;
}
struct dirent *readdir(DIR *dir)
{
	if (!dir) return 0;
	if (read(dir->fd, dir->dirent.d_name, NAME_MAX + 1) <= 0) return 0;
	return &dir->dirent;
}
void rewinddir(DIR *dir)
{
	seekdir(dir, 0);
}
void seekdir(DIR *dir, long index)
{
	if (dir)
	{
		lseek(dir->fd, index, SEEK_SET);
	}
}
long telldir(DIR *dir)
{
	if (!dir) return -1;
	return lseek(dir->fd, 0, SEEK_CUR);
}

int ioctl(int fd, int request, ...)
{
	int param = *(&request + 1);
	int status;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_IOCTL), "b"(fd), "c"(request), "d"(param));
	return status;
}

int unlink(const char *filename)
{
	return -1;
}

int pipe(int fd[2])
{
	int in;
	int out;
	asm volatile("int $0x80" : "=b"(in), "=c"(out): "a"(SYS_PIPE));
	if (in == -1)
		return -1;
	fd[1] = in;
	fd[0] = out;
	return 0;
}

int fcntl(int fd, int cmd, ...)
{
	uintptr_t param = *(uintptr_t*)(&cmd + 1);
	int status;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_FCNTL), "b"(fd), "c"(cmd), "d"(param));
	return status;
}

int stat(const char *path, struct stat *buf)
{
	return -1;
}

void FD_CLR(int fd, fd_set *fdset)
{
}
int FD_ISSET(int fd, fd_set *fdset)
{
	return 0;
}
void FD_SET(int fd, fd_set *fdset)
{
}
void FD_ZERO(fd_set *fdset)
{
}

/*int pselect(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds,
	const struct timespec *timeout, const sigset_t *sigmask)
{
}*/
int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds,
	struct timeval *timeout)
{
	return -1;
}


