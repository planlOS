/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <planlos/syscalls.h>

#define SOCKET_IOCTL_SETSOCKOPT 1
#define SOCKET_IOCTL_BIND 2
#define SOCKET_IOCTL_LISTEN 3
#define SOCKET_IOCTL_ACCEPT 4

int accept(int socket, struct sockaddr *address, socklen_t *address_len)
{
	char data[6];
	struct sockaddr_in *addr = (struct sockaddr_in*)address;
	memcpy(data, &addr->sin_addr, 4);
	memcpy(data + 4, &addr->sin_port, 2);
	return ioctl(socket, SOCKET_IOCTL_ACCEPT, data);
}
int bind(int socket, const struct sockaddr *address, socklen_t address_len)
{
	struct sockaddr_in *inaddr = (struct sockaddr_in*)address;
	char data[6];
	*((in_addr_t*)data) = inaddr->sin_addr.s_addr;
	*((in_port_t*)(data + 4)) = inaddr->sin_port;
	return ioctl(socket, SOCKET_IOCTL_BIND, data);
}
int connect(int socket, const struct sockaddr *address, socklen_t address_len)
{
	return -1;
}
int getpeername(int socket, struct sockaddr *address, socklen_t *address_len)
{
	return -1;
}
int getsockname(int socket, struct sockaddr *address, socklen_t *address_len)
{
	return -1;
}
int getsockopt(int socket, int level, int option_name, void *option_value,
	socklen_t *option_len)
{
	return -1;
}
int listen(int socket, int backlog)
{
	return ioctl(socket, SOCKET_IOCTL_LISTEN, backlog);
}
ssize_t recv(int socket, void *buffer, size_t length, int flags)
{
	return read(socket, buffer, length);
}
ssize_t recvfrom(int socket, void *buffer, size_t length, int flags,
	struct sockaddr *address, socklen_t *address_len)
{
	return -1;
}
ssize_t recvmsg(int socket, struct msghdr *message, int flags)
{
	return -1;
}
ssize_t send(int socket, const void *message, size_t length, int flags)
{
	return write(socket, message, length);
}
ssize_t sendmsg(int socket, const struct msghdr *message, int flags)
{
	return -1;
}
ssize_t sendto(int socket, const void *message, size_t length, int flags,
	const struct sockaddr *dest_addr, socklen_t dest_len)
{
	return -1;
}
int setsockopt(int socket, int level, int option_name, const void *option_value,
	socklen_t option_len)
{
	return -1;
}
int shutdown(int socket, int how)
{
	return -1;
}
int socket(int domain, int type, int protocol)
{
	int fd;
	asm volatile("int $0x80" : "=b"(fd): "a"(SYS_SOCKET));
	return fd;
}
int socketpair(int domain, int type, int protocol, int socket_vector[2])
{
	return -1;
}

