/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <planlos/syscalls.h>

unsigned int errno;

void abort(void)
{
	asm volatile("int $0x80" : : "a"(SYS_EXIT), "b"(-1));
}

void *sbrk(int increment)
{
	return 0;
}

void exit(int value)
{
	// TODO: Close files
	asm volatile("int $0x80" : : "a"(SYS_EXIT), "b"(value));
}

int atoi(const char *str)
{
	int number = 0;
	int negative = 0;
	if (*str == '-')
	{
		str++;
		negative = 1;
	}
	while (*str)
	{
		if ((*str >= '0') && (*str <= '9'))
		{
			number = number * 10 + (*str - '0');
			str++;
		}
		else
		{
			break;
		}
	}
	return number;
}

int abs(int n)
{
	if (n < 0) return -n;
	return n;
}

static int strtol_is_digit(const char c, int base)
{
	if ((c >= '0') && (c <= '9'))
	{
		if (c - '0' < base)
		{
			return 1;
		}
		return 0;
	}
	else if ((c >= 'a') && (c <= 'z'))
	{
		if (c - 'a' < base - 10)
		{
			return 1;
		}
	}
	else if ((c >= 'A') && (c <= 'Z'))
	{
		if (c - 'A' < base - 10)
		{
			return 1;
		}
	}
	return 0;
}
static int strtol_get_digit(const char c, int base)
{
	if ((c >= '0') && (c <= '9'))
	{
		return c - '0';
	}
	else if ((c >= 'a') && (c <= 'z'))
	{
		return c - 'a';
	}
	else if ((c >= 'A') && (c <= 'Z'))
	{
		return c - 'A';
	}
	else return 0;
}

long int strtol(const char *nptr, char **endptr, int base)
{
	int negative = 0;
	if (*nptr == '-')
	{
		negative = 1;
		nptr++;
	}
	
	long int number = 0;
	
	while (strtol_is_digit(*nptr, base))
	{
		number = number * base + strtol_get_digit(*nptr, base);
		nptr++;
	}
	if (endptr) *endptr = (char*)nptr;
	if (negative) number = -number;
	return number;
}

