/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <termios.h>

int tcflush(int fd, int queue_selector)
{
	write(1, "tcflush\n", 8);
	return -1;
}
int tcgetattr(int fd, struct termios *t)
{
	write(1, "tcgetattr\n", 10);
	return -1;
}
pid_t tcgetsid(int fd)
{
	return -1;
}
int tcsendbreak(int fd, int dur)
{
	return -1;
}
int tcsetattr(int fd, int optional_actions, const struct termios *t)
{
	return -1;
}

speed_t cfgetospeed(const struct termios *t)
{
	return B9600;
}

