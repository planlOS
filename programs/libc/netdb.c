/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void endhostent(void)
{
}
void endnetent(void)
{
}
void endprotoent(void)
{
}
void endservent(void)
{
}
struct hostent *gethostbyaddr(const void *addr, size_t len, int type)
{
	return 0;
}
struct hostent *gethostbyname(const char *name)
{
	return 0;
}
struct hostent *gethostent(void)
{
	return 0;
}
struct netent *getnetbyaddr(uint32_t net, int type)
{
	return 0;
}
struct netent *getnetbyname(const char *name)
{
	return 0;
}
struct netent *getnetent(void)
{
	return 0;
}
struct protoent *getprotobyname(const char *name)
{
	return 0;
}
struct protoent *getprotobynumber(int proto)
{
	return 0;
}
struct protoent *getprotoent(void)
{
	return 0;
}
struct servent *getservbyname(const char *name, const char *proto)
{
	return 0;
}
struct servent *getservbyport(int port, const char *proto)
{
	return 0;
}
struct servent *getservent(void)
{
	return 0;
}

void sethostent(int stayopen)
{
}
void setnetent(int stayopen)
{
}
void setprotoent(int stayopen)
{
}
void setservent(int stayopen)
{
}
void freeaddrinfo(struct addrinfo *info)
{
	if (info->ai_canonname)
		free(info->ai_canonname);
	free(info);
}
int getaddrinfo(const char *nodename, const char *servname, const struct addrinfo * hints, struct addrinfo ** res)
{
	// Resolve name
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	if ((nodename == 0) || !strcmp(nodename, "") || !strcmp(nodename, "localhost"))
	{
		((char*)&addr.sin_addr)[0] = 0;
		((char*)&addr.sin_addr)[1] = 0;
		((char*)&addr.sin_addr)[2] = 0;
		((char*)&addr.sin_addr)[3] = 0;
	}
	else
	{
		struct hostent *host = gethostbyname(nodename);
		if (!host)
		{
			return -1;
		}
		memcpy(&addr.sin_addr, host->h_addr_list[0], 4);
	}
	
	// Get service
	if ((servname == 0) || !strcmp(servname, ""))
	{
		return -1;
	}
	if (isdigit(servname[0]))
	{
		addr.sin_port = atoi(servname);
	}
	else
	{
		return -1;
	}
	// Fill in struct
	struct addrinfo *addrinfo = malloc(sizeof(struct addrinfo));
	memcpy(addrinfo, hints, sizeof(struct addrinfo));
	addrinfo->ai_family = addr.sin_family;
	addrinfo->ai_addrlen = sizeof(struct sockaddr_in);
	addrinfo->ai_addr = malloc(sizeof(struct sockaddr_in));
	memcpy(addrinfo->ai_addr, &addr, sizeof(addr));
	if (nodename)
		addrinfo->ai_canonname = strdup(nodename);
	else
		addrinfo->ai_canonname = 0;
	addrinfo->ai_next = 0;
	*res = addrinfo;
	return 0;
}

const char *gai_strerror(int error)
{
	return "";
}

