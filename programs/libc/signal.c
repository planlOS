/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <signal.h>
#include <planlos/syscalls.h>

int kill(pid_t pid, int sig)
{
	int status;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_KILL), "b"(pid), "c"(sig));
	return status;
}
int raise(int sig)
{
	int status;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_RAISE), "b"(sig));
	return status;
}
void (*signal(int sig, void (*func)(int)))(int)
{
	int status;
	asm volatile("int $0x80" : "=b"(status): "a"(SYS_SIGNAL), "b"(sig), "c"(func));
	return 0;
}

int sigdelset(sigset_t *set, int signo)
{
	return -1;
}
int sigemptyset(sigset_t *set)
{
	return -1;
}
int sigfillset(sigset_t *set)
{
	return -1;
}

int sigpending(sigset_t *set)
{
	return -1;
}
int sigprocmask(int how, const sigset_t *set, sigset_t *oset)
{
	return -1;
}

int sigaddset(sigset_t *set, int signo)
{
	return -1;
}

int sigaction(int sig, const struct sigaction *act, struct sigaction *oact)
{
	return -1;
}

int sigismember(const sigset_t *set, int signo)
{
	return -1;
}

