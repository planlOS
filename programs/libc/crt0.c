
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv);

struct FILE
{
	int fd;
	int append;
	int error;
};

void _start(int envc, char **envv, int argc, char **argv)
{
	if (envv)
	{
		environ = malloc(sizeof(char*) * (envc + 1));
		memcpy(environ, envv, sizeof(char*) * (envc + 1));
	}
	else environ = 0;
	
	stdin = malloc(sizeof(struct FILE));
	memset(stdin, 0, sizeof(struct FILE));
	stdout = malloc(sizeof(struct FILE));
	memset(stdout, 0, sizeof(struct FILE));
	stdout->fd = 1;
	stderr = malloc(sizeof(struct FILE));
	memset(stderr, 0, sizeof(struct FILE));
	stderr->fd = 2;
	
	main(argc, argv);
	exit(0);
}
