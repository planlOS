/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

#define ERROR_EOF 1
#define ERROR_ERROR 2

struct FILE
{
	int fd;
	int append;
	int error;
};

FILE *stderr = 0;
FILE *stdout = 0;
FILE *stdin = 0;

FILE *fopen(const char *filename, const char *mode)
{
	// Open file
	int openmode = 0;
	if (strchr(mode, 'r')) openmode = O_RDONLY;
	else if (strchr(mode, 'w')) openmode = O_WRONLY | O_CREAT;
	else if (strchr(mode, 'a')) openmode = O_WRONLY | O_CREAT;
	if (strchr(mode, '+')) openmode = (openmode & ~O_ACCMODE) | O_RDWR;
	int fd = open(filename, openmode);
	if (fd == -1) return 0;
	// Return file structure
	FILE *file = malloc(sizeof(FILE));
	memset(file, 0, sizeof(FILE));
	file->fd = fd;
	if (strchr(mode, 'a')) file->append = 1;
	return file;
}
int fclose(FILE *file)
{
	if (!file) return EOF;
	close(file->fd);
	free(file);
	return 0;
}
FILE *freopen(const char *filename, const char *mode, FILE *file)
{
	if (fclose(file) == EOF) return 0;
	return fopen(filename, mode);
}
int feof(FILE *file)
{
	return (file->error & ERROR_EOF) != 0;
}
int ferror(FILE *file)
{
	return (file->error & ERROR_ERROR) != 0;
}
int fseek(FILE *file, long int offset, int origin)
{
	if (!file) return EOF;
	file->error = 0;
	if (lseek(file->fd, offset, origin) == (off_t)-1) return EOF;
	return 0;
}
long int ftell(FILE *file)
{
	if (!file) return EOF;
	long int pos = lseek(file->fd, 0, SEEK_CUR);
	return pos;
}
void rewind(FILE *file)
{
	fseek(file, 0, SEEK_SET);
}
void clearerr(FILE *file)
{
	file->error = 0;
}

int fgetpos(FILE *file, fpos_t *pos)
{
	if (!file || !pos) return EOF;

	long int position = lseek(file->fd, 0, SEEK_CUR);
	if (position == -1)
	{
		*pos = 0;
		return EOF;
	}
	*pos = position;
	return 0;
}
int fsetpos(FILE *file, const fpos_t *pos)
{
	if (!file || !pos) return EOF;
	file->error = 0;
	if (lseek(file->fd, *pos, SEEK_SET) == (off_t)-1) return -1;
	return 0;
}

int fgetc(FILE *file)
{
	if (!file) return EOF;
	char c = 0;
	int size = read(file->fd, &c, 1);
	if (size == 0)
	{
		file->error = ERROR_EOF;
		return EOF;
	}
	else if (size == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return c;
}
char *fgets(char *str, int num, FILE *file)
{
	int n = 0;
	while (n < num)
	{
		char c = fgetc(file);
		if ((c == EOF) && ferror(file)) return 0;
		if ((c == EOF) && (n == 0))
		{
			return 0;
		}
		else if ((c == EOF) || (c == '\n') || (c == 0))
		{
			str[n] = 0;
			return str;
		}
		else
		{
			str[n] = c;
		}
		n++;
	}
	str[n - 1] = 0;
	return str;
}
int fscanf(FILE *file, const char *fmt, ...)
{
	return -1;
}
size_t fread(void *ptr, size_t size, size_t count, FILE *file)
{
	if (!file) return 0;
	int bytecount = read(file->fd, ptr, size * count);
	if (bytecount == -1)
	{
		file->error = ERROR_ERROR;
		return 0;
	}
	if (bytecount != (int)(size * count))
	{
		file->error = ERROR_EOF;
	}
	return bytecount;
}

int fputc(int c, FILE *file)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	int size = write(file->fd, &c, 1);
	if (size == 0)
	{
		file->error = ERROR_EOF;
		return EOF;
	}
	else if (size == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return c;
}
int fputs(const char *s, FILE *file)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	int size = write(file->fd, s, strlen(s));
	if (size == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return size;
}
int vfprintf(FILE *file, const char *fmt, va_list args)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	// FIXME: Greater length?
	char buffer[1024];
	vsnprintf(buffer, 1024, fmt, args);

	int written = write(file->fd, buffer, strlen(buffer));
	if (written == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return written;
}
int fprintf(FILE *file, const char *fmt, ...)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	// FIXME: Greater length?
	char buffer[1024];
	va_list args;
	va_start(args, fmt);
	vsnprintf(buffer, 1024, fmt, args);
	va_end(args);

	int written = write(file->fd, buffer, strlen(buffer));
	if (written == -1)
	{
		file->error = ERROR_ERROR;
		return EOF;
	}
	return written;
}
size_t fwrite(const void *ptr, size_t size, size_t count, FILE *file)
{
	if (!file) return EOF;
	if (file->append) lseek(file->fd, 0, SEEK_END);
	int written = write(file->fd, ptr, size * count);
	if (written == -1) return EOF;
	return written;
}

int fileno(FILE *file)
{
	if (!file) return -1;
	return file->fd;
}

FILE *tmpfile(void)
{
	// TODO
	return 0;
}
char *tmpnam(char *s)
{
	// TODO
	return s;
}

int ungetc(int character, FILE *file)
{
	return EOF;
}

int rename(const char *oldname, const char *newname)
{
	return -1;
}
int remove(const char *filename)
{
	return -1;
}

int printf(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int status = vfprintf(stdout, fmt, args);
	va_end(args);
	return status;
}
int puts(const char *s)
{
	printf("%s\n", s);
	return 0;
}
int putc(int c, FILE *file)
{
	return fputc(c, file);
}
int putchar(int c)
{
	return fputc(c, stdout);
}
int getchar(void)
{
	return fgetc(stdin);
}

void perror(const char *str)
{
	printf("%s\n", str);
}

int fflush(FILE *file)
{
	return 0;
}

int setvbuf(FILE *file, char *buf, int type, size_t size)
{
	return 0;
}

int scanf(const char *format, ... )
{
	return -1;
}
int sscanf(const char *s, const char *format, ... )
{
	return -1;
}

