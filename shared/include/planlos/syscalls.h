/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef PLANLOS_SYSCALLS_INCLUDED
#define PLANLOS_SYSCALLS_INCLUDED

#define SYS_EXIT 1
#define SYS_OPEN 2
#define SYS_CLOSE 3
#define SYS_READ 4
#define SYS_WRITE 5
#define SYS_FORK 6
#define SYS_SLEEP 7
#define SYS_WAITPID 8
#define SYS_EXEC 9
#define SYS_ALLOC 10
#define SYS_FREE 11
#define SYS_WAIT 12
#define SYS_SEEK 13
#define SYS_MKNOD 14
#define SYS_GETWD 15
#define SYS_CHDIR 16
#define SYS_PIPE 17
#define SYS_SOCKET 18
#define SYS_IOCTL 19
#define SYS_FCNTL 20
#define SYS_SIGNAL 21
#define SYS_RAISE 22
#define SYS_GETPID 23
#define SYS_TIME 24
#define SYS_KILL 25

#endif

