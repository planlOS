/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef PLANLOS_SIGNALS_INCLUDED
#define PLANLOS_SIGNALS_INCLUDED

#define SIGABRT 1
#define SIGALRM 2
#define SIGFPE 3
#define SIGHUP 4
#define SIGILL 5
#define SIGINT 6
#define SIGKILL 7
#define SIGPIPE 8
#define SIGQUIT 9
#define SIGSEGV 10
#define SIGTERM 11
#define SIGUSR1 12
#define SIGUSR2 13
#define SIGCHLD 14
#define SIGCONT 15
#define SIGSTOP 16
#define SIGTSTP 17
#define SIGTTIN 18
#define SIGTTOU 19
#define SIGBUS 20
#define SIGPOLL 21
#define SIGPROF 22
#define SIGSYS 23
#define SIGTRAP 24
#define SIGURG 25
#define SIGVTALRM 26
#define SIGXCPU 27
#define SIGXFSZ 28
#define SIGWINCH 29

#define NSIG 30

#endif

