/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef STDIO_H_INCLUDED
#define STDIO_H_INCLUDED

#include <stdint.h>
#include <stdarg.h>

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

#define EOF -1

#define L_tmpnam 13

#define BUFSIZ 256

#define _IOFBF 1
#define _IOLBF 2
#define _IONBF 3

#define NULL ((void*)0)

typedef unsigned int fpos_t;

typedef struct FILE FILE;
extern FILE *stderr;
extern FILE *stdout;
extern FILE *stdin;

FILE *fopen(const char *filename, const char *mode);
int fclose(FILE *file);
FILE *freopen(const char *filename, const char *mode, FILE *file);
int feof(FILE *file);
int ferror(FILE *file);
int fseek(FILE *file, long int offset, int origin);
long int ftell(FILE *file);
void rewind(FILE *file);
void clearerr(FILE *file);

int fgetpos(FILE *file, fpos_t *pos);
int fsetpos(FILE *file, const fpos_t *pos);

int fgetc(FILE *file);
char *fgets(char *str, int num, FILE *file);
int fscanf(FILE *file, const char *fmt, ...);
size_t fread(void *ptr, size_t size, size_t count, FILE *file);

int fputc(int c, FILE *file);
int fputs(const char *s, FILE *file);
int fprintf(FILE *file, const char *fmt, ...);
size_t fwrite(const void *ptr, size_t size, size_t count, FILE *file);

FILE *tmpfile(void);
char *tmpnam(char *s);

int ungetc(int character, FILE *file);

int rename(const char *oldname, const char *newname);
int remove(const char *filename);

int fflush(FILE *file);

int fileno(FILE *file);

int printf(const char *fmt, ...);
int puts(const char *s);
int putchar(int c);
int getchar(void);

int snprintf(char *buf, size_t n, const char *fmt, ...);
int sprintf(char *buf, const char *fmt, ...);
int vsnprintf(char *buf, size_t n, const char *fmt, va_list arg);
int vsprintf(char *buf, const char *fmt, va_list arg);

int asprintf(char **buf, const char *fmt, ...);
int vasprintf(char **buf, const char *fmt, va_list arg);

void perror(const char *str);

int setvbuf(FILE *file, char *buf, int type, size_t size);

#endif

