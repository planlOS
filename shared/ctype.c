/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>

int isalnum(int c)
{
	if ((c >= 'A') && (c <= 'Z')) return 1;
	if ((c >= 'a') && (c <= 'z')) return 1;
	if ((c >= '0') && (c <= '9')) return 1;
	return 0;
}
int isalpha(int c)
{
	if ((c >= 'A') && (c <= 'Z')) return 1;
	if ((c >= 'a') && (c <= 'z')) return 1;
	return 0;
}
int iscntrl(int c)
{
	if ((c >= 0) && (c < 0x20)) return 1;
	if (c == 0x7F) return 1;
	return 0;
}
int isdigit(int c)
{
	if ((c >= '0') && (c <= '9')) return 1;
	return 0;
}
int isgraph(int c)
{
	if ((c >= 0x21) && (c <= 0x7e)) return 1;
	return 0;
}
int islower(int c)
{
	if ((c >= 'a') && (c <= 'z')) return 1;
	return 0;
}
int isprint(int c)
{
	if ((c >= 0x20) && (c <= 0x7e)) return 1;
	return 0;
}
int ispunct(int c)
{
	if ((c >= 0x21) && (c <= 0x2f)) return 1;
	if ((c >= 0x3a) && (c <= 0x40)) return 1;
	if ((c >= 0x5b) && (c <= 0x60)) return 1;
	if ((c >= 0x7b) && (c <= 0x7e)) return 1;
	return 0;
}
int isspace(int c)
{
	if ((c >= 0x9) && (c <= 0xd)) return 1;
	if (c == ' ') return 1;
	return 0;
}
int isupper(int c)
{
	if ((c >= 'A') && (c <= 'Z')) return 1;
	return 0;
}
int isxdigit(int c)
{
	if ((c >= '0') && (c <= '9')) return 1;
	if ((c >= 'a') && (c <= 'f')) return 1;
	if ((c >= 'A') && (c <= 'F')) return 1;
	return 0;
}
int isblank(int c)
{
	if ((c == ' ') || (c == '\t')) return 1;
	return 0;
}
int isascii(int c)
{
	return (c < 128);
}

int tolower(int c)
{
	if (isupper(c))
	{
		return c + 'a' - 'A';
	}
	return c;
}
int toupper(int c)
{
	if (islower(c))
	{
		return c + 'A' - 'a';
	}
	return c;
}

