/*
Copyright (C) 2008  Mathias Gottschlag

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <string.h>
#include <stdlib.h>

void *memcpy(void *dest, const void *src, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++)
	{
		((char*)dest)[i] = ((const char*)src)[i]; 
	}
	return dest;
}
void *memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++)
	{
		((char*)dest)[i] = ((const char*)src)[i];
		if (((const char*)src)[i] == c)
			return &((char*)dest)[i + 1];
	}
	return NULL;
}
void *memmove(void *dest, const void *src, size_t n)
{
	size_t i;
	if (src == dest) return dest;
	if (dest < src)
	{
		for (i = 0; i < n; i++)
		{
			((char*)dest)[i] = ((const char*)src)[i];
		}
	}
	else
	{
		for (i = n - 1; i <= 0; i--)
		{
			((char*)dest)[i] = ((const char*)src)[i];
		}
	}
	return dest;
}
void *memchr(const void *s, int c, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++)
	{
		if (((char*)s)[i] == c) return &((char*)s)[i];
	}
	return NULL;
}
int memcmp(const void *s1, const void *s2, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++)
	{
		if (((char*)s1)[i] != ((char*)s2)[i])
		{
			return ((char*)s1)[i] - ((char*)s2)[i];
		}
	}
	return 0;
}
void *memset(void *dest, int c, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++)
	{
		((char*)dest)[i] = c;
	}
	return dest;
}

char *strcat(char *dest, const char *src)
{
	int destlen = strlen(dest);
	int srclen = strlen(dest);
	int i;
	for (i = 0; i <= srclen; i++)
	{
		dest[destlen + i] = src[i];
	}
	return dest;
}
char *strchr(const char *s, int c)
{
	while (*s)
	{
		if (*s == c) return (char*)s;
		s++;
	}
	return NULL;
}
int strcmp(const char *s1, const char *s2)
{
	for ( ; *s1 == *s2; s1++, s2++)
		if (*s1 == '\0')
			return 0;
	
	return *s1 - *s2;
}
/*int strcoll(const char *s1, const char *s2)
{
	// TODO
	return 0;
}*/
char *strcpy(char *dest, const char *src)
{
	while((*dest++ = *src++));
	return dest;
}
size_t strcspn(const char *s, const char *reject)
{
	int count = 0;
	while (*s && !strchr(reject, s[count]))
	{
		count++;
		s++;

	}
	return count;

}
char *strdup(const char *s)
{
	char *newstr = malloc(strlen(s) + 1);
	if (newstr) strcpy(newstr, s);
	return newstr;
}
/*char *strerror(int e)
{
	// TODO
	return 0;
}
int *strerror_r(int e, char *s, size_t n)
{
	// TODO
	return 0;
}*/
size_t strlen(const char *s)
{
	const char *p = s;
	while( *p != '\0')
		p++;
	return p - s;
}
char *strncat(char *dest, const char *src, size_t n)
{
	size_t destlen = strlen(dest);
	size_t srclen = strlen(dest);
	if (srclen > n) srclen = n;
	size_t i;
	for (i = 0; i < srclen; i++)
	{
		dest[destlen + i] = src[i];
	}
	dest[destlen + srclen] = 0;
	return dest;
}
int strncmp(const char *s1, const char *s2, size_t n)
{
	size_t i;
	for (i = 0 ; (*s1 == *s2) && (i < n); s1++, s2++, i++)
		if (*s1 == '\0')
			return 0;
	
	if (i == n) return 0;
	return *s1 - *s2;
}
char *strncpy(char *dest, const char *src, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++)
	{
		((char*)dest)[i] = ((const char*)src)[i];
		if (((const char*)src)[i] == 0) return dest;
	}
	return dest;
}
char *strpbrk(const char *s, const char *accept)
{
	int acclen = strlen(s);
	int i;
	while (*s)
	{
		for (i = 0; i < acclen; i++)
		{
			if (strchr(s, accept[i]))
				return (char*)s;
		}
		s++;
	}
	return 0;
}
char *strrchr(const char *s, int c)
{
	int len = strlen(s);
	int i;
	for (i = len; i >= 0; i--)
	{
		if (s[i] == c) return (char*)&s[i];
	}
	return 0;
}
size_t strspn(const char *s, const char *accept)
{
	int count = 0;
	while (strchr(accept, s[count]))
	{
		count++;
		s++;
	}
	return count;
}
char *strstr(const char *s1, const char *s2)
{
	int len1 = strlen(s1);
	int len2 = strlen(s2);
	if (len2 > len1) return NULL;
	int i;
	for (i = 0; i < len1 - len2; i++)
	{
		if (!memcmp(&s1[i], s2, len2))
			return (char*)&s1[i];
	}
	return NULL;
}
static char *splitted = NULL;
char *strtok(char *str, const char *delimiters)
{
	if (str)
	{
		splitted = str;
	}
	if (!splitted) return NULL;
	
	size_t count = strcspn(splitted, delimiters);
	char *token = splitted;
	if (count == strlen(splitted)) splitted = NULL;
	else
	{
		char *tmp = splitted;
		tmp += strspn(tmp, delimiters);
		splitted[count] = 0;
		splitted = tmp;
	}
	return token;
}
//char *strtok_r(char *, const char *, char **);
/*size_t strxfrm(char *dest, const char *src, size_t n)
{
	// TODO
	return 0;
}*/
