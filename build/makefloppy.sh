#!/bin/bash

dd if=/dev/zero of=build/floppy.img bs=1024 count=1440
# Create mtools config file
cat << EOF > build/mtools.conf
drive i:
	file="build/floppy.img" cylinders=80 heads=2 sectors=18 filter
EOF
export MTOOLSRC=build/mtools.conf
# Format disk
mformat i:
# Copy menu.lst
mmd -D s i:/boot
mmd -D s i:/boot/grub
mcopy -D o build/menu.lst i:/boot/grub/menu.lst
# Copy grub
mcopy -D o /boot/grub/stage? i:/boot/grub/
# Install grub
grub --batch --no-floppy <<EOF
	device (fd0) build/floppy.img
	root (fd0)
	setup (fd0)
EOF

# Copy files
mcopy -D o build/boot/* i:/boot/
mmd -D s i:/bin
mcopy -D o build/bin/* i:/bin/
mmd -D s i:/lib
mcopy -D o build/lib/* i:/lib/

rm -f build/mtools.conf

echo ""

