#!/bin/bash

mkdir build/iso
mkdir build/iso/bin
mkdir build/iso/lib
mkdir build/iso/boot
mkdir build/iso/boot/grub
cp /usr/lib/grub/x86_64-pc/stage2_eltorito build/iso/boot/grub/
cp build/menu.lst.cd build/iso/boot/grub/menu.lst
cp build/bin/* build/iso/bin/
cp build/lib/* build/iso/lib/
cp build/boot/* build/iso/boot/

mkisofs -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -boot-info-table -o build/cdrom.img build/iso

rm -r build/iso

